#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
using namespace std;
class Solution {
public:
    int tribonacci(int n) {
        vector<int> dp(n + 1);
        if (n == 0) return 0;
        if (n == 1 || n == 2) return 1;
        dp[0] = 0; dp[1] = 1, dp[2] = 1;
        for (int i = 3; i <= n; i++)
        {
            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
        }
        return dp[n];
    }
};
class Solution {
public:
    int tribonacci(int n) {
        if (n == 0) return 0;
        if (n == 1 || n == 2) return 1;
        int a = 0, b = 1, c = 1, d = 0;
        for (int i = 3; i <= n; i++)
        {
            d = a + b + c;
            a = b; b = c; c = d;
        }
        return d;
        // vector<int> dp(n+1);
        // if(n == 0) return 0;
        // if(n == 1 || n==2) return 1;
        // dp[0] = 0; dp[1] = 1,dp[2] = 1;
        // for(int i = 3;i <= n;i ++)
        // {
        //     dp[i]=dp[i-1]+dp[i-2]+dp[i-3];
        // } 
        // return dp[n];
    }
};

//爬台阶
class Solution {
public:
    int waysToStep(int n) {
        if (n == 1) return 1;
        if (n == 2) return 2;
        const int MOD = 1e9 + 7;
        vector<long int> dp(n + 1);
        dp[0] = 1; dp[1] = 1; dp[2] = 2;
        for (int i = 3; i <= n; i++)
        {
            dp[i] = (dp[i - 1] + dp[i - 2] + dp[i - 3]) % MOD;
        }
        return dp[n];
    }
};

//最小花费走楼梯
class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        int n = cost.size();
        vector<int> dp(n + 1);
        if (n == 0 || n == 1) return 0;
        dp[0] = 0; dp[1] = 0;
        for (int i = 2; i <= n; i++)
            dp[i] = min(dp[i - 2] + cost[i - 2], dp[i - 1] + cost[i - 1]);
        return dp[n];
    }
};
//最小花费走楼梯
class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        int n = cost.size();
        vector<int> dp(n);
        dp[n - 1] = cost[n - 1]; dp[n - 2] = cost[n - 2];
        for (int i = n - 3; i >= 0; i--)
            dp[i] = cost[i] + min(dp[i + 1], dp[i + 2]); //从i位置往前走1/2步走到目标的花费
        return min(dp[0], dp[1]);


        // int n=cost.size();
        // vector<int> dp(n+1);
        // if(n==0 || n==1) return 0;
        // dp[0]=0;dp[1]=0;
        // for(int i=2;i<=n;i++)
        //  dp[i]=min(dp[i-2]+cost[i-2],dp[i-1]+cost[i-1]);
        // return dp[n];
    }
};


//解码
class Solution {
public:
    int numDecodings(string s) {

        //优化版本
        int n = s.size();
        vector<int> dp(n + 1);
        dp[0] = 1;
        dp[1] = s[0] != '0';
        for (int i = 2; i <= n; i++)
        {
            if (s[i - 1] >= '1' && s[i - 1] <= '9') dp[i] += dp[i - 1];
            int tmp = 10 * (s[i - 2] - '0') + s[i - 1] - '0';
            if (tmp >= 10 && tmp <= 26) dp[i] += dp[i - 2];
        }
        return dp[n];


        // int n=s.size();
        // vector<int> dp(n);

        // dp[0]=s[0]!='0';
        // if(n==1) return dp[0];
        // if(s[1]>='1' && s[1]<='9') dp[1]+=dp[0];
        // int tmp=(s[0]-'0')*10+s[1]-'0';
        // if(tmp>=10 && tmp<=26) dp[1]+=1;
        // for(int i=2;i<n;i++)
        // {
        // if(s[i]>='1' && s[i]<='9') dp[i]+=dp[i-1];
        // int tmp=10*(s[i-1]-'0')+s[i]-'0';
        // if(tmp>=10 && tmp<=26) dp[i]+=dp[i-2];
        // }
        // return dp[n-1];
    }
};


class Solution {
public:
    int uniquePaths(int m, int n) {
        vector<vector<int>> dp(m, vector<int>(n, 0));
        for (int i = 0; i < n; i++)
        {
            dp[0][i] = 1;
        }
        for (int i = 0; i < m; i++)
            dp[i][0] = 1;
        for (int i = 1; i < m; i++)
        {
            for (int j = 1; j < n; j++)
            {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }

        }
        return dp[m - 1][n - 1];
    }
};

//不同路径I
class Solution {
public:
    int uniquePaths(int m, int n) {
        //优化版本
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        dp[0][1] = 1;
        for (int i = 1; i <= m; i++)
            for (int j = 1; j <= n; j++)
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
        return dp[m][n];
        // vector<vector<int>> dp(m,vector<int>(n,0));
        // for(int i=0;i<n;i++)
        //      dp[0][i]=1;
        // for(int i=0;i<m;i++)
        //      dp[i][0]=1;
        // for(int i=1;i<m;i++)
        //     for(int j=1;j<n;j++)
        //         dp[i][j]=dp[i-1][j]+dp[i][j-1];
        // return dp[m-1][n-1];
    }
};


//不同路径II
class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        int m = obstacleGrid.size(), n = obstacleGrid[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        dp[1][0] = 1;
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                if (obstacleGrid[i - 1][j - 1] == 0) //如果这个位置是障碍物直接就是0，不进行操作
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[m][n];
    }
};


//礼物的最大价值
class Solution {
public:
    int maxValue(vector<vector<int>>& grid) {
        int m = grid.size(), n = grid[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
        }
        return dp[m][n];
    }
};


//下降路径最小和
class Solution {
public:
    int minFallingPathSum(vector<vector<int>>& matrix) {
        int m = matrix.size(), n = matrix[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 2, INT_MAX));
        for (int i = 0; i <= n + 1; i++) //因为第一行没有选择只能是第一行的原本数据所以在上面一行设置为0
            dp[0][i] = 0;
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
                dp[i][j] = min(min(dp[i - 1][j - 1], dp[i - 1][j + 1]), dp[i - 1][j]) + matrix[i - 1][j - 1];
        }
        int ret = INT_MAX;
        for (int i = 0; i <= n + 1; i++)
            ret = min(ret, dp[m][i]);
        return ret;
    }
};

//最小路径和
class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        int m = grid.size(), n = grid[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
        dp[0][1] = dp[1][0] = 0;
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
        }
        return dp[m][n];
    }
};

//龙与地下城
class Solution {
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon) {
        int m = dungeon.size(), n = dungeon[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
        dp[m][n - 1] = dp[m - 1][n] = 1;
        for (int i = m - 1; i >= 0; i--)
        {
            for (int j = n - 1; j >= 0; j--)
            {
                //x+dp[i][j]>=dp[i][j+1] ——> x>=dp[i]][j+1]-dp[i][j]
                //同理 x>=dp[i+1][j]-dp[i][j];
                dp[i][j] = min(dp[i + 1][j], dp[i][j + 1]) - dungeon[i][j];
                dp[i][j] = max(1, dp[i][j]);
            }

        }
        return dp[0][0];
    }
};

//按摩师
class Solution {
public:
    int massage(vector<int>& nums) {
        int n = nums.size();
        if (n == 0) return 0;
        vector<int> f(n);
        vector<int> g(n);
        f[0] = nums[0]; g[0] = 0;
        for (int i = 1; i < n; i++)
        {
            f[i] = g[i - 1] + nums[i];
            g[i] = max(f[i - 1], g[i - 1]);
        }
        return max(f[n - 1], g[n - 1]);
    }
};

//打家劫舍II
class Solution {
public:
    int rob1(vector<int>& nums, int begin, int end)
    {
        if (begin > end) return 0;
        int n = nums.size();
        if (n == 0) return 0;
        vector<int> f(n);
        vector<int> g(n);
        f[begin] = nums[begin]; g[begin] = 0;
        for (int i = begin + 1; i <= end; i++)
        {
            f[i] = g[i - 1] + nums[i];
            g[i] = max(f[i - 1], g[i - 1]);
        }
        return max(f[end], g[end]);
    }
    int rob(vector<int>& nums) {
        int n = nums.size();
        return max(nums[0] + rob1(nums, 2, n - 2), rob1(nums, 1, n - 1));
    }
};

//删除点数
class Solution {
public:
    int deleteAndEarn(vector<int>& nums) {
        const int N = 1e4 + 1;
        int arr[N] = { 0 };
        for (auto e : nums) arr[e] += e;
        //在arr数组上打家劫舍
        vector<int> f(N);
        vector<int> g(N);
        f[0] = arr[0]; g[0] = 0;
        for (int i = 1; i < N; i++)
        {
            f[i] = g[i - 1] + arr[i];
            g[i] = max(f[i - 1], g[i - 1]);
        }
        return max(f[N - 1], g[N - 1]);
    }
};

//粉刷房子
class Solution {
public:
    int minCost(vector<vector<int>>& costs) {
        int n = costs.size();
        vector<vector<int>> dp(n + 1, vector<int>(3, 0));
        for (int i = 1; i <= n; i++)
        {
            dp[i][0] = min(dp[i - 1][1], dp[i - 1][2]) + costs[i - 1][0];
            dp[i][1] = min(dp[i - 1][0], dp[i - 1][2]) + costs[i - 1][1];
            dp[i][2] = min(dp[i - 1][0], dp[i - 1][1]) + costs[i - 1][2];

        }
        return min(min(dp[n][0], dp[n][1]), dp[n][2]);

        // int n=costs.size();
        // vector<vector<int>> dp(n+1,vector<int>(3,0));

        // for(int i=1;i<=n;i++)
        // {
        //     for(int j=0;j<3;j++)
        //     {
        //        if(j==0) dp[i][j]=min(dp[i-1][j+1],dp[i-1][j+2])+costs[i-1][j];
        //        else if(j==1) dp[i][j]=min(dp[i-1][j-1],dp[i-1][j+1])+costs[i-1][j];
        //        else dp[i][j]=min(dp[i-1][0],dp[i-1][1])+costs[i-1][j];
        //     }
        // }
        // return min(min(dp[n][0],dp[n][1]),dp[n][2]);
    }
};

//买股票的最佳时期
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int n = prices.size();
        vector<vector<int>> dp(n, vector<int>(3));
        dp[0][0] = -prices[0];
        //0 买 1 卖 2 冷冻
        for (int i = 1; i < n; i++)
        {
            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
            dp[i][1] = max(dp[i - 1][1], dp[i - 1][2]);
            dp[i][2] = dp[i - 1][0] + prices[i];
        }
        return max(dp[n - 1][1], dp[n - 1][2]);
    }
};

//买卖股票带手续费
class Solution {
public:
    int maxProfit(vector<int>& prices, int fee) {
        //或者用之前的买不买思想
        int n = prices.size();
        vector<int> f(n); //表示当天要买入
        vector<int> g(n); //卖出
        f[0] = -prices[0];
        for (int i = 1; i < n; i++)
        {
            f[i] = max(g[i - 1] - prices[i], f[i - 1]);
            g[i] = max(g[i - 1], f[i - 1] + prices[i] - fee);
        }
        return g[n - 1];

        //int n = prices.size();
        //vector<int> f(n); //表示当天要买入
        //vector<int> g(n); //卖出
        //f[0] = -prices[0] - fee;
        //for (int i = 1; i < n; i++)
        //{
        //    f[i] = max(g[i - 1] - prices[i] - fee, f[i - 1]);
        //    g[i] = max(g[i - 1], f[i - 1] + prices[i]);
        //}
        //return g[n - 1];

        // //0代表没股票，1代表有股票
        // int n=prices.size();
        // vector<vector<int>> dp(n,vector<int>(2));
        // dp[0][0]=0;dp[0][1]=-prices[0];
        // for(int i=1;i<n;i++)
        // {
        //     //今天结束处于买入状态,
        //     dp[i][0]=max(dp[i-1][0],dp[i-1][1]+prices[i]-fee);
        //     dp[i][1]=max(dp[i-1][1],dp[i-1][0]-prices[i]);
        // }
        // return dp[n-1][0];
    }
};


class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int n = prices.size();
        //买入——>卖出 算是一笔交易
        //初始化：选max时不能干涉结果
        //注意最小值的初始化
        vector<vector<int>> f(n, vector<int>(3, -0x3f3f3f)); //买入
        vector<vector<int>> g(n, vector<int>(3, -0x3f3f3f)); //卖出
        f[0][0] = -prices[0]; g[0][0] = 0;
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
                g[i][j] = g[i - 1][j];
                if (j >= 1)
                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
            }
        }
        //找到最后一天卖出状态的最大值
        int ans = 0;
        for (int i = 0; i < 3; i++)
            ans = max(g[n - 1][i], ans);
        return ans;
    }
};

//买卖股票的最佳时机 IV
class Solution {
public:
    int maxProfit(int k, vector<int>& prices) {
        const int MIN = -0x3f3f3f;
        int n = prices.size();
        k = min(n / 2, k); //交易次数不会超过天数的一半
        vector<vector<int>> f(n, vector<int>(k + 1, MIN));
        auto g = f;
        f[0][0] = -prices[0]; g[0][0] = 0;
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < k + 1; j++)
            {
                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
                g[i][j] = g[i - 1][j];
                if (j >= 1)
                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
            }
        }
        int ans = 0;
        for (int j = 0; j < k + 1; j++)
        {
            ans = max(ans, g[n - 1][j]);
        }
        return ans;
    }
};


//最大子数组和
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int n = nums.size();
        vector<int> dp(n);
        dp[0] = nums[0];
        int MAX = nums[0];
        for (int i = 1; i < n; i++)
        {
            dp[i] = max(dp[i - 1] + nums[i], nums[i]);
            if (dp[i] > MAX) MAX = dp[i];
        }
        //一定要注意返回值不一定是最后一个位置的最大连续子数组和
        return MAX;
    }

};


//环形子数组的最大和
class Solution {
public:
    int maxSubarraySumCircular(vector<int>& nums) {
        int n = nums.size();
        vector<int> f(n + 1), g(n + 1);
        int fmax = -0x3f3f3f3f, gmin = 0x3f3f3f3f, sum = 0;
        for (int i = 1; i < n + 1; i++)
        {
            int x = nums[i - 1];
            f[i] = max(x, x + f[i - 1]);
            if (f[i] > fmax) fmax = f[i];
            g[i] = min(x, x + g[i - 1]);
            if (gmin > g[i]) gmin = g[i];
            sum += x;
        }
        //所有元素都是负数返回fmax

        return sum == gmin ? fmax : max(fmax, sum - gmin);
    }
};


//乘积最大的子数组
class Solution {
public:
    int maxProduct(vector<int>& nums) {
        int n = nums.size();
        vector<int>f(n + 1), g(n + 1);
        f[0] = 1; g[0] = 1;
        int fmax = -0x3f3f3f3f, gmin = 0x3f3f3f3f;
        for (int i = 1; i <= n; i++)
        {
            if (nums[i - 1] > 0)
            {
                f[i] = max(nums[i - 1], f[i - 1] * nums[i - 1]);
                fmax = max(fmax, f[i]);
                g[i] = min(nums[i - 1], g[i - 1] * nums[i - 1]);
                gmin = min(g[i], gmin);
            }
            else
            {
                f[i] = max(nums[i - 1], g[i - 1] * nums[i - 1]);
                fmax = max(fmax, f[i]);
                g[i] = min(nums[i - 1], f[i - 1] * nums[i - 1]);
                gmin = min(g[i], gmin);
            }
        }
        return fmax;
    }
};

//乘积最小子数组
class Solution {
public:
    int maxProduct(vector<int>& nums) {
        int n = nums.size();
        vector<int> f(n + 1), g(n + 1);
        f[0] = g[0] = 1;
        int ans = -0x3f3f3f3f; //注意最后是要比max，所以选最小
        for (int i = 1; i <= n; i++)
        {
            int x = nums[i - 1], y = f[i - 1] * nums[i - 1], z = g[i - 1] * nums[i - 1];
            f[i] = max(x, max(z, y));
            g[i] = min(x, min(z, y));
            ans = max(ans, f[i]);
        }
        return ans;

        // int n=nums.size();
        // //只用一个dp是不行的，因为要区分乘的数字是大于还是小于0
        // vector<int>f(n+1),g(n+1);
        // f[0]=1;g[0]=1;
        // int fmax=-0x3f3f3f3f,gmin=0x3f3f3f3f;
        // for(int i=1;i<=n;i++)
        // {
        //     if(nums[i-1]>0)
        //     {
        //         f[i]=max(nums[i-1],f[i-1]*nums[i-1]);
        //         fmax=max(fmax,f[i]);
        //         g[i]=min(nums[i-1],g[i-1]*nums[i-1]);
        //         gmin=min(g[i],gmin);
        //     }
        //     else 
        //     {
        //         f[i]=max(nums[i-1],g[i-1]*nums[i-1]);
        //         fmax=max(fmax,f[i]);
        //         g[i]=min(nums[i-1],f[i-1]*nums[i-1]);
        //         gmin=min(g[i],gmin);
        //     }
        // }
        // return fmax;
    }
};


//乘积为正数的最长子数组长度
class Solution {
public:
    int getMaxLen(vector<int>& nums) {
        int n = nums.size();
        vector<int> f(n + 1), g(n + 1);
        f[0] = g[0] = 0; //f代表正，g代表负
        //注意这里的f和g表示长度而不是乘积
        int ans = -0x3f3f3f3f;
        for (int i = 1; i <= n; i++)
        {
            if (nums[i - 1] > 0)
            {
                f[i] = f[i - 1] + 1;
                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
            }
            else if (nums[i - 1] < 0)
            {
                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
                g[i] = f[i - 1] + 1;
            }
            ans = max(ans, f[i]);
        }
        return ans;
    }
};


//等差数列的划分
class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& nums) {
        int n = nums.size();
        vector<int> dp(n);
        int ans = 0;
        for (int i = 2; i < n; i++)
        {
            dp[i] = nums[i] - nums[i - 1] == nums[i - 1] - nums[i - 2] ? dp[i - 1] + 1 : 0;
            ans += dp[i];
        }

        return ans;
    }
};



//最长湍流子数组
class Solution {
public:
    int maxTurbulenceSize(vector<int>& arr) {
        //f:<   g:>
        int n = arr.size();
        vector<int> f(n, 1), g(n, 1);
        int ans = 1;
        for (int i = 1; i < n; i++)
        {
            if (arr[i - 1] < arr[i])
                f[i] = g[i - 1] + 1;
            else if (arr[i - 1] > arr[i])
                g[i] = f[i - 1] + 1;
            ans = max(max(f[i], g[i]), ans);
        }
        return ans;
    }
};

////单词拆分
//public:
//    bool wordBreak(string s, vector<string>& wordDict) {
//        //把整个的字符串划分成前面的字符串+后面的最后一个单词
//        int n = s.size();
//        unordered_set<string> hash(wordDict.begin(), wordDict.end());
//        vector<bool> dp(n + 1);
//        dp[0] = true;
//        s = ' ' + s;
//        for (int i = 1; i < n + 1; i++)
//        {
//            //j表示后面最后一个单词以j起始，前面字符串的结尾下标是j-1
//            for (int j = i; j >= 1; j--)
//            {
//                if (dp[j - 1] == true && hash.count(s.substr(j, i - j + 1)))
//                {
//                    dp[i] = true;
//                    break;
//                }
//            }
//        }
//        return dp[n];
//
//
//        // //把整个的字符串划分成前面的字符串+后面的最后一个单词
//        // int n=s.size();
//        // unordered_set<string> hash;
//        // for(auto& s:wordDict) hash.insert(s);
//        // vector<bool> dp(n+1);
//        // dp[0]=true;
//        // for(int i=1;i<n+1;i++)
//        // {
//        //     for(int j=0;j<i;j++)
//        //     {
//        //         //我感觉这样是最好理解的，j是从原字符串的下标映射遍历的
//        //         if(dp[j]==true && hash.count(s.substr(j,i-j)))
//        //        { 
//        //            dp[i]=true;
//        //            break;
//        //        }
//        //     }
//        // }
//        // return dp[n];
//    }
//};


//环绕字符串中唯一的子字符串
class Solution {
public:
    int findSubstringInWraproundString(string s) {
        //dp[i]表示以i结尾的子串有多少个在base出现
        //dp[i]=dp[i-1]+1
        int n = s.size();
        //注意这里一定要都设置成1，因为下面的条件如果不满足，那么永远要依赖前面的值，所以不能只依赖前一项
        vector<int> dp(n, 1);
        for (int i = 1; i < n; i++)
        {
            //确实是构成无限环绕才能算在dp中
            if (s[i - 1] + 1 == s[i] || (s[i - 1] == 'z' && s[i] == 'a'))
                dp[i] = dp[i - 1] + 1;
        }
        //但是一定要注意，dp中的值可能会有重复，比如说"aba"
        //因为dp是按照以s[i]结尾的判断，如果最后结尾是一样的就要去重，按照后出现的为准
        int hash[26] = { 0 };
        for (int i = 0; i < n; i++)
        {
            hash[s[i] - 'a'] = max(hash[s[i] - 'a'], dp[i]);
        }
        int sum = 0;
        for (auto e : hash) sum += e;
        return sum;
    }
};

//最长递增子序列
class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        //dp表示以i结束的最长递增子序列
        //dp[i]=max(dp[i],dp[j]+1)
        //由于每一个数字都能自己组成一个递增序列，所以所有数字都要初始化成1

        int n = nums.size();
        vector<int> dp(n, 1);
        int ans = 1; //记录dp中的最大值,一定是1开始的，因为一个数字就可以组成一个递增序列
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (nums[i] > nums[j])
                    dp[i] = max(dp[i], dp[j] + 1);
            }
            ans = max(ans, dp[i]);
        }
        return ans;
    }
};


//摆动序列
class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
        int n = nums.size();
        //f：最后一个是增的
        //g：最后一个减
        vector<int> f(n, 1), g(n, 1);
        int ans = 1;
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (nums[i] > nums[j])
                {
                    f[i] = max(f[i], g[j] + 1);
                }
                else if (nums[i] < nums[j])
                    g[i] = max(g[i], f[j] + 1);


            }
            ans = max(ans, max(f[i], g[i]));
        }
        return ans;

        //    int n =nums.size();
        //    vector<int> newnums;
        //    newnums.push_back(nums[0]);
        //     //去掉连续的重复的数字
        //     for(int i=1;i<n;i++)
        //     {
        //         if(nums[i]!=nums[i-1])
        //         newnums.push_back(nums[i]);
        //     }
        //     //一定要注意更新，不然去重白做了
        //     n=newnums.size();
        //    vector<int> dp(n,0);
        //    dp[0]=1;
        //    //可能最后去重结束都没有dp[1]
        //    if(n<=1) return n;
        //    dp[1]=2;

        //     for(int i=2;i<n;i++)
        //     {
        //         //此时相邻的值都是不一样的，所以直接比较相邻的是不是满足摆动即可
        //        if((newnums[i]-newnums[i-1])*(newnums[i-1]-newnums[i-2])<0)
        //        dp[i]=dp[i-1]+1;
        //        else dp[i]=dp[i-1];
        //     }
        //     return dp[n-1];
    };