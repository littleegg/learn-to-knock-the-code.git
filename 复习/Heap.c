#include "Heap.h"
void HeapCreat(Hp* hp, HeapDataType* a, int n)
{
	assert(hp);
	hp->a = (HeapDataType*)malloc(sizeof(HeapDataType) * n);
	if (hp->a == NULL)
	{
		perror("realloc fail");
		exit(-1);
	}
	memcpy(hp->a, a, sizeof(HeapDataType)*n);
	hp->capacity = hp->size=n;
	int child = n-1;
	int parent = (child - 1) /2;
	while(parent>=0)
	{
		AdjustDown(hp->a, hp->size, parent);
		child--;
		parent= (child - 1) * 2;
	}
	/*for (int i = (n - 1 - 1) / 2; i >=0; i--) //这个和我写的是一个思路
	{
		AdjustDown(hp->a,n, i);
	}*/
}
void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}
void AdjustUp(HeapDataType* a, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0) //如果孩子更大
	{
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}
void HeapPrint(Hp* hp)
{
	assert(hp);
	for (int i = 0; i < hp->size; i++)
	{
		printf("%d ", hp->a[i]);
	}
	printf("\n");
}
void HeapDestory(Hp* hp)
{
	assert(hp);
	free(hp->a);
	hp->a = NULL;
	hp->capacity = hp->capacity = 0;
}
void HeapInit(Hp* hp)
{
	assert(hp);
	hp->a = NULL;
	hp->capacity = hp->size = 0;
}
void Heappush(Hp* hp, HeapDataType x)
{
	assert(hp);
	if (hp->capacity == hp->size)
	{
		int newcapacity = hp->capacity == 0 ? 4 : hp->capacity * 2;
		HeapDataType* tmp = (HeapDataType*)realloc(hp->a, sizeof(HeapDataType) * newcapacity);
		if (tmp == NULL)
		{
			perror("realloc fail");
			exit(-1);
		}
		hp->a = tmp;
		hp->capacity = newcapacity;
	}
	hp->a[hp->size] = x;
	hp->size++;
	//默认这里是大堆结构，父亲都应该>=孩子

	//向上调整
	AdjustUp(hp->a, hp->size-1);

}
void AdjustDown(HeapDataType* a, int n, int parent)
{
	assert(a);
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (a[child + 1] < a[child] && child+1<n)
		{
			child = child + 1;
		}
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}
void HeapPop(Hp* hp)
{
	assert(hp);
	assert(hp->size > 0);
	Swap(&hp->a[0], &hp->a[hp->size - 1]);
	hp->size--;
	AdjustDown(hp->a, hp->size, 0);
}
HeapDataType HeapTop(Hp* hp)
{
	assert(hp);
	return hp->a[0];
}
int HeapSize(Hp* hp)
{
	assert(hp);
	return hp->size;
}
bool HeapEmpty(Hp* hp)
{
	assert(hp);
	return hp->capacity == hp->size==0;
}

//排序
//1.堆排
void HeapSort(Hp* hp,HeapDataType* a,int n)
{
	assert(hp); 
	/*for (int i =1; i <n; i++) //向上调整建堆 O(N*logN)
	{
		AdjustUp(a, i);
	}*/
	for (int i = (n - 1 - 1) / 2; i >= 0; i--) //向下调整O(N)
	{
		AdjustDown(a,n, i);
	}
	//建好(大堆)之后要求升序（建大堆）
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);//把最大的数字放到最后
		AdjustDown(a, end, 0);
		end--;
	}
}
 
//现在有一组很多个数据保存在文件里，想要找出前k个
void HeapTopk2(Hp* hp, int k)
{
	HeapDataType* Heapmin = (HeapDataType*)malloc(k * sizeof(HeapDataType));
	if (Heapmin == NULL)
	{
		perror("Heapmin malloc fail");
		exit(-1);
	}
	FILE* fout = fopen("Data.txt", "r");
	if (fout == NULL) {
		perror("fopen fail");
		exit(-1);
	}
	for (int i=0;i<k;i++)
	{
		fscanf(fout, "%d", &Heapmin[i]); //把数据都放到数组里面
	}
	//建小堆
	for (int i = (k - 1 - 1) / 2; i >= 0; i--) //向下调整O(N)
	{
		AdjustDown(Heapmin, k, i);
	}
	int val = 0;
	while (fscanf(fout, "%d", &val) != EOF)
	{
		if (val > Heapmin[0])
		{
			Heapmin[0] = val;
			AdjustDown(Heapmin, k, 0);

		}
	}
	for (int i = 0; i < k; i++)
	{
		printf("%d ", Heapmin[i]);
	}
	printf("\n");
	fclose(fout);
}

void Heapmake(void) //造数据
{
	int n = 10000;
	int k = 3;
	srand(time(0));
	FILE* fin = fopen("Data.txt", "w");
	if (fin == NULL)
	{
		perror("fopen fail");
		exit(-1);

	}
	for (int i = 0; i < n; i++)
	{
		int val = rand();
		fprintf(fin, "%d\n",val);
	}
	fclose(fin);
}