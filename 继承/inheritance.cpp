﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
using namespace std;
//class Person
//{
//protected:
//	string _name; // 姓名
//	string _sex;
//	int _age; // 年龄
//};
//class Student : public Person
//{
//public:
//	int _No; // 学号
//};
//void Test()
//{
//	Student s;
//	// 1.子类对象可以赋值给父类对象/指针/引用
//	Person p = s;
//	Person* pp = &s;
//	Person& rp = s;
//
//	// 3.基类的指针可以通过强制类型转换赋值给派生类的指针
//	pp = &s;
//	Student * ps1 = (Student*)pp; // 这种情况转换时可以的。
//	ps1->_No = 10;
//
//	pp = &p;
//	Student* ps2 = (Student*)pp; // 这种情况转换时虽然可以，但是会存在越界访问的问题
//	ps2->_No = 10;
//}
//class A
//{
//public:
//	void fun()
//	{
//		cout << "A" << endl;
//	}
//	A()
//	{
//		cout << "A()" << endl;
//
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//	int _age;
//};
//class B:public A 
//{
//public:
//	void fun(int i=0)
//	{
//		cout << "B" << endl;
//	}
//	B()
//	{
//		cout << "B()" << endl;
//	}
//	~B()
//	{
//		cout << "~B()" << endl;
//	}
//	int _num;
//};
//int main()
//{
//	B b;
//	return 0;
//}
//class Student;
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._stuNum << endl;
//}
//int main()
//{
//	Person p;
//	Student s;
//	Display(p, s);
//}


//class Person
//{
//public:
//	Person() { ++_count; }
//protected:
//	string _name; // 姓名
//public:
//	static int _count; // 统计人的个数。
//};
//int Person::_count = 0;
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//class Graduate : public Student
//{
//protected:
//string _seminarCourse; // 研究科目
//};
//void TestPerson()
//{
//	Student s1;
//	Student s2;
//	Student s3;
//	Graduate s4;
//	cout << " 人数 :" << Person::_count << endl;
//	Student::_count = 0;
//	cout << " 人数 :" << Person::_count << endl;
//}
//int main()
//{
//	TestPerson();
//}
// 
// 
// 
//class Person
//{
//public:
//	Person(const char* name)
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//
//		return *this;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//public:
//	Student(const char* name, int num)
//		:Person(name)
//		, _num(num)
//	{}
//
//	Student(const Student& s)
//		:Person(s)
//		, _num(s._num)
//	{}
//
//	Student& operator=(const Student& s)
//	{
//		if (this != &s)
//		{
//			Person::operator=(s);
//			_num = s._num;
//		}
//
//		return *this;
//	}
//
//protected:
//	int _num; //学号
//};
//
//int main()
//{
//	Student s1("张三", 18);
//	Student s2(s1);
//	Student s3("李四", 20);
//	s1 = s3;
//	return 0;
//}


//class Person
//{
//public:
//	string _name; // 姓名
//};
//
//class Student : virtual public Person
//{
//protected:
//	int _num; //学号
//};
//
//class Teacher : virtual public Person
//{
//protected:
//	int _id; // 职工编号
//};
//
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; // 主修课程
//};
//
//
//int main()
//{
//	Assistant a;
//	a._name = "小张";
//	a.Student::_name = "张三";
//	a.Teacher::_name = "张老师";
//
//	return 0;
//}


//class A
//{
//public:
//	int _a;
//};
//// class B : public A
//class B : virtual public A
//{
//public:
//	int _b;
//};
//// class C : public A
//class C : virtual public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	D d;
//	d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//	return 0;
//}

class A
{
	int _a;
};
class B :public A
{
	int _b;
};

class M
{
	int _m;
};
class N
{
	M _mm;
	int _n;
};