#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <mutex>
#include <memory>
using namespace std;

#include "SmartPtr.h"

// 使用RAII思想设计的SmartPtr类
//template<class T>
//class SmartPtr {
//public:
//	SmartPtr(T* ptr = nullptr)
//		: _ptr(ptr)
//	{}
//	~SmartPtr()
//	{
//		if (_ptr)
//			delete _ptr;
//	}
//
//private:
//	T* _ptr;
//};
//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//void Func()
//{
//	SmartPtr<int> sp1(new int);
//	SmartPtr<int> sp2(new int);
//	cout << div() << endl;
//}
//int main()
//{
//	try {
//		Func();
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}
// 
// template<class T>
//class SmartPtr {
//public:
//	SmartPtr(T* ptr = nullptr)
//		: _ptr(ptr)
//	{}
//	~SmartPtr()
//	{
//		if (_ptr)
//			delete _ptr;
//	}
//	T& operator*() { return *_ptr; }
//	T* operator->() { return _ptr; }
//private:
//	T* _ptr;
//};
//struct Date
//{
//	int _year=1;
//	int _month=1;
//	int _day=1;
//};
//int main()
//{
//	SmartPtr<int> sp1(new int);
//	*sp1 = 10;
//	cout << *sp1 << endl;
//	SmartPtr<Date> sparray(new Date);
//	// 需要注意的是这里应该是sparray.operator->()->_year = 2018;
//	// 本来应该是sparray->->_year这里语法上为了可读性，省略了一个->
//	sparray->_year = 2018;
//	sparray->_month = 1;
//	sparray->_day = 1;
//	return 0;
//}
int main()
{
	//wrt::test_unique_ptr();
	//wrt::test_shared_ptr();
	//wrt::test_auto_ptr();
	//wrt::test_shared_safe();
	wrt::test_shared_cycle();
	//wrt::test_shared_deletor();
	return 0;
}