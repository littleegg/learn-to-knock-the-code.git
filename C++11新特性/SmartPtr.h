#pragma once
#include <thread>
#include <functional>
#include <mutex>
#include<memory>
//对象是自定义类型
//加锁，线程安全问题
//namespace wrt
//{
//	template<class T>
//	class auto_ptr //管理权转移
//	{
//	public:
//		auto_ptr(T* ptr)
//			:_ptr(ptr)
//		{
//			cout << "	auto_ptr(T * ptr)" << endl;
//		}
//		T& operator*()
//		{
//			return *_ptr;
//		}
//		T* operator->()
//		{
//			return _ptr;
//		}
//		auto_ptr(auto_ptr<T>& ap)
//			:_ptr(ap._ptr)
//		{
//			ap._ptr = nullptr;
//		}
//		~auto_ptr()
//		{
//			if (_ptr)
//			{
//				cout << "~auto_ptr()" << endl;
//				delete _ptr;
//			}
//		}
//	private:
//		T* _ptr;
//	};
//	void test_auto_ptr()
//	{
//		auto_ptr<int> ap1(new int(1));
//		auto_ptr<int> ap2(ap1);
//		//*ap1 = 10; //此时ap1悬空导致不能访问
//		*ap2 = 20;
//		cout << *ap2 << endl;
//	}
//}


#include <mutex>
//namespace wrt
//{
//	template<class T>
//	class unique_ptr
//	{
//	public:
//		unique_ptr(T* ptr)
//			:_ptr(ptr) {}
//		T& operator*()
//		{
//			return *_ptr;
//		}
//		T& operator->()
//		{
//			return _ptr;
//		}
//		//简单粗暴，防止拷贝
//		//C++11
//		unique_ptr(const unique_ptr<T>& uq) = delete;
//		unique_ptr<T>& operator=(const unique_ptr<T>& uq) = delete;
//		//C++98
//	private:
//		unique_ptr(const unique_ptr<T>& uq);
//		~unique_ptr()
//		{
//			if (_ptr)
//				std::cout << "delete:" << _ptr << std::endl;
//
//			delete _ptr;
//		}
//	private:
//		T* _ptr;
//	};
//	void test_unique_ptr()
//	{
//		unique_ptr<int> uq(new int(1));
//
//	}
//}
namespace wrt
{
	template<class T>
	class shared_ptr
	{
	public:
		shared_ptr( T*ptr=nullptr, int* pcount=new int(1))
			:_ptr(ptr),_pcount(pcount),_pmtx(new mutex)
		{}

		//定时删除器 用function进行包装
		template<class D>
		shared_ptr(T* ptr , D del)
			:_ptr(ptr), _pcount( new int(1)), _pmtx(new mutex),_del(del)
		{}

		//因为后面多次涉及析构，所以封装成函数
		void Rease()
		{
			_pmtx->lock();
			bool daleteflage = false;
			if ((--(*_pcount)) == 0)
			{
				if (_ptr)
				{
					cout << "delete:" <<_ptr<< endl;
					//delete _ptr;
					_del(_ptr); //用包装器释放
				}
				delete _pcount;
				//如何解决 delete _pmtxx->用一个bool标志位
				daleteflage = true;
			}
			_pmtx->unlock();

			if (daleteflage)
				delete _pmtx;
			
		}
		~shared_ptr()
		{
			Rease();
		}
		void Addcount()
		{
			_pmtx->lock();
			(*_pcount)++;
			_pmtx->unlock();
		}
		//拷贝构造
		shared_ptr(const shared_ptr<T>& sp)
			:_ptr(sp._ptr)
			, _pcount(sp._pcount)
			,_pmtx(sp._pmtx)
		{
			Addcount();
		}

		shared_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			//相同的资源之间相互赋值不建议写
			//if(&sp != this) //因为可能本身就不同的对象但是管理着相同的资源，不用对象去判断，用资源判断

			if (sp._ptr != _ptr) //说明不是一个内存
			{
				{
					Rease();
				}
				_ptr = sp._ptr;
				_pcount = sp._pcount;
				_pmtx = sp._pmtx;
				Addcount();
			}
			return *this;
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		T* get_ptr() const 
		{
			return _ptr;
		}

		int use_count() const 
		{
			return *_pcount;
		}
	private:
		T* _ptr;
		int* _pcount;
		std::mutex* _pmtx;
		function<void(T*)>  _del = [](T* ptr) {
			cout << "lambda" << ptr << endl;
			delete ptr;
		};//包装器
	};
	class Date
	{
	public:
		Date() {}
		Date(int year, int month, int day)
			:_year(year), _month(month), _day(day) {}
		~Date()
		{}
		Date(const Date& d)
		{
			_month = d._month;
			_day = d._day;
			_year = d._year;
		}
	public:
		int _year = 0;
		int _month = 0;
		int _day = 0;
	};
	void SharePtrFunc(wrt::shared_ptr<Date>& sp, size_t n, mutex& mtx)
	{
		for (size_t i = 0; i < n; ++i)
		{
			// 这里智能指针拷贝会++计数，智能指针析构会--计数，这里是线程安全的。
			wrt::shared_ptr<Date> copy(sp);
			// 这里智能指针访问管理的资源，不是线程安全的。所以我们看看这些值两个线程++了2n
			//次，但是最终看到的结果，并一定是加了2n
			{
			mtx.lock();
			copy->_year++;
			copy->_month++;
			copy->_day++;
			mtx.unlock();
			}
		}
	}
	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr() :_ptr(nullptr),_pcount(new int(1))
		{}
		weak_ptr(const shared_ptr<T>& sp) :_ptr(sp.get_ptr()),_pcount(new int(sp.use_count())) {}
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}
		T* get_ptr() const
		{
			return _ptr;
		}

		int use_count()
		{
			return *_pcount;
		}
	private:
		T* _ptr;
		int* _pcount;
	};
	//void test_shared_safe()
	//{
	//	wrt::shared_ptr<Date> p(new Date);
	//	cout << p.get_ptr() << endl;
	//	const size_t n = 100000;
	//	mutex mtx;
	//	thread t1(shareptrfunc,ref(p), n, ref(mtx));
	//	thread t2(shareptrfunc, ref(p), n, ref(mtx));
	//	t1.join();
	//	t2.join();
	//	cout << p.use_count() << endl;

	//	cout << p->_year << endl;
	//	cout << p->_month << endl;
	//	cout << p->_day << endl;
	//}
	void test_shared_ptr()
	{
		/*shared_ptr<int> sq1(new int(1));
		shared_ptr<int> sq2(sq1);*/
		shared_ptr<Date> day1(new Date(2023, 7, 4));
		shared_ptr<Date> day2(day1);
		shared_ptr<Date> day3(new Date(2022,4,4));
		day3 = day1;
	}


	struct ListNode
	{/*
		ListNode* _next;
		ListNode* _prev;*/
		shared_ptr<ListNode> _next; //但是这样会报错说没有默认构造函数，是因为只能指针没有默认构造函数
		shared_ptr<ListNode> _prev;
		//wrt::weak_ptr<ListNode> _next;
		//wrt::weak_ptr<ListNode> _prev;

		int _val=10;
		~ListNode()
		{
			cout << "~ListNode" << endl;
		}
	};
	void test_shared_cycle()
	{
		//ListNode* n1 = new ListNode;
		//ListNode* n2 = new ListNode;
		//n1->_next = n2;  //但是可能这里会抛异常，这样就无法正常释放,所以考虑使用只能指针
		//n2->_prev = n1;
		//delete n1;
		//delete n2;
		//wrt::shared_ptr<ListNode> n1 = new ListNode;//其实这样写是不对的，相当于ListNode*指针隐式型转换,库里面在带参构造的函数前面加上了explicit 
		//wrt::shared_ptr<ListNode> n2 = new ListNode;
		//wrt::shared_ptr<ListNode> n1(new ListNode);
	   // wrt::shared_ptr<ListNode> n2(new ListNode); //此时有智能指针管就不用考虑释放
		//现在考虑指向的问题
		//n1->_next = n2;
		//n2->_prev = n1; //智能指针不能给给shared_ptr
		//只屏蔽一个指向会正常释放但是两个指向就什么都不会打印 是因为循环引用，本身shared_ptr设计导致极端场景的坑，分析一下场景（画图）
		//循环引用导致内存泄漏，但是我还想保持指向只是不想让next和prev参与管理
		//为了解决循环引用的问题，引入waek_ptr指针
		//他的特点是：
		//1.不符合RAII,不是常规智能指针
		//2.支持像指针一样，本质是专门设计出来解决shared_ptr循环应用的问题
		//3.weak_ptr本身是有引用计数的，但是他指向资源但不参与管理
	}

	//定制删除器
	template<class T>
	struct DeleteDate
	{
		void operator()(T* _ptr)
		{
			cout << "void operator()(T * _ptr)" << endl;
			delete[]_ptr;
		}
			T* _ptr;
	};
	void test_shared_deletor()
	{
		wrt::shared_ptr<Date> sp1(new Date[10],DeleteDate<Date>());
		wrt::shared_ptr<Date> sp2(new Date[10], [](Date* ptr) {
			cout << "lambda delete" << endl;
			delete[]ptr; });
		wrt::shared_ptr<FILE> spF3(fopen("Test.cpp","r"), [](FILE* ptr) {
			cout << "fclose()" << endl;
			fclose(ptr); });
	

	}
}