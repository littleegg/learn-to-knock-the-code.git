#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
#include <string>
using namespace std;



//struct Point
//{
//    int _x;
//    int _y;
//};
//int main()
//{
//    int array1[] = { 1, 2, 3, 4, 5 };
//    int array2[5] = { 0 };
//    Point p = { 1, 2 };
//    return 0;
//}
// 
// 
//struct Point
//{
//    int _x;
//    int _y;
//};
//int main()
//{
//    int x1 = 1;
//    int x2{ 2 };
//    int array1[]{ 1, 2, 3, 4, 5 }; //=可以省略
//    int array2[5]={ 0 };
//    Point p{ 1, 2 };
//    // C++11中列表初始化也可以适用于new表达式中
//    int* pa = new int[4]{ 0 };
//    return 0;
//}
//class Date
//{
//public:
//    Date(int year, int month, int day)
//        :_year(year)
//        , _month(month)
//        , _day(day)
//    {
//        cout << "Date(int year, int month, int day)" << endl;
//    }
//private:
//    int _year;
//    int _month;
//    int _day;
//};
//int main()
//{
//    Date d1(2022, 1, 1); // 老写法
//    // C++11支持的列表初始化，这里会调用构造函数初始化
//    Date d2{ 2022, 1, 2 };
//    Date d3 = { 2022, 1, 3 };
//    return 0;
//}

//#include <list>
//#include <map>
//int main()
//{
//    vector<int> v = { 1,2,3,4 };
//    list<int> lt = { 1,2 };
//    // 这里{"sort", "排序"}会先初始化构造一个pair对象
//    map<string, string> dict = { {"sort", "排序"}, {"insert", "插入"} };
//    // 使用大括号对容器赋值
//    v = { 10, 20, 30 };
//    return 0;
//}

////模拟实现vector支持initializer_list
//namespace wrt
//{
//	template<class T>
//	class vector {
//	public:
//		typedef T* iterator;
//		vector(initializer_list<T> l)
//		{
//			_start = new T[l.size()];
//			_finish = _start + l.size();
//			_endofstorage = _start + l.size();
//			iterator vit = _start;
//			typename initializer_list<T>::iterator lit = l.begin();
//			for (auto e : l)
//				(*vit)++ = e;
//		}
//		vector<T>& operator=(initializer_list<T> l) {
//			vector<T> tmp(l); //复用
//			std::swap(_start, tmp._start);
//			std::swap(_finish, tmp._finish);
//			std::swap(_endofstorage, tmp._endofstorage);
//			return *this;
//		}
//	private:
//		iterator _start;
//		iterator _finish;
//		iterator _endofstorage;
//	};
//}


//class A {
//public:
//    A() {
//        std::cout << "A construct..." << std::endl;
//        ptr_ = new int(100);
//    }
//
//    A(const A& a) {
//        std::cout << "A copy construct ..." << std::endl;
//        ptr_ = new int();
//        memcpy(ptr_, a.ptr_, sizeof(int));
//    }
//
//    ~A() {
//        std::cout << "A deconstruct ..." << std::endl;
//        if (ptr_) {
//            delete ptr_;
//        }
//    }
//
//    A& operator=(const A& a) {
//        std::cout << " A operator= ...." << std::endl;
//        return *this;
//    }
//
//    int* getVal() {
//        return ptr_;
//    }
//private:
//    int* ptr_;
//};
//
//int main(int argc, char* argv[]) {
//    std::vector<A> vec;
//    vec.push_back(A());
//}

//打印发现上面vector内部还会调用A的拷贝构造函数，为了不造成反复分配/释放堆的操作，采用移动构造
//移动构造就像名字一样，是对指针的移动，把一个对象的指针成员转移给另一个对象
//一般原对象的指针成员会被置为nullptr，防止被再度调用
//移动构造
//class A {
//public:
//    ...
//
//    A(A&& a)  //代表右值引用，接收的必须是右值
//    {
//        std::cout << "A move construct ..." << std::endl;
//        ptr_ = a.ptr_;
//        a.ptr_ = nullptr;
//    }
//    ...
//};
//int main()
//{
//    std::vector<A> vec;
//    vec.push_back(std::move(A())); //move()可以把左值转换成右值
//	return 0;
//}

//右值是C++从C继承来的概念，最初是指=号右边的值。但现在C++中的右值已经与它最初的概念完全不一样了。在C++中右值指的的临时值或常量，更准确的说法是保存在CPU寄存器中的值为右值，而保存在内存中的值为左值。
//可能有很多同学对计算机系统的底层不太了解，我们这里做一个简单的介绍。计算机是由CPU、内存、主板、总线、各种硬件等组成的，这个大家应该都清楚。而CPU又是由逻辑处理器，算术单元、寄存器等组成的。我们的程序运行时并不是直接从内存中取令运行的，因为内存相对于CPU来说太慢了。一般情况下都是先将一部分指令读到CPU的指令寄存器，CPU再从指令寄存器中取指令然后一条一条的执行。对于数据也是一样，先将数据从内存中读到数据寄存器，然后CPU从数据寄存器读数据。以Intel的CPU为例，它就包括了 EAX、EBX、ECX、EDX…多个通用寄存器，这样就可以让CPU更高效的工作。
//比如说一个常数5，我们在使用它时不会在内存中为其分配一个空间，而是直接把它放到寄存器中，所以它在C++中就是一个右值。再比如说我们定义了一个变量 a，它在内存中会分配空间，因此它在C++中就是左值。那么a + 5是左值还是右值呢？当然是右值对吧，因为a + 5的结果存放在寄存器中，它并没有在内存中分配新空间，所以它是右值。、
//int main(int argc, char* argv[]) {
//
//    int&& a = 5;  // 正确，5会被直接存放在寄存器中，所以它是右值
//    int b = 10;
//    int&& c = b;  // 错误，b在内存中有空间，所以是右值；右值不能赋值给左值
//    int&& d = b + 5; // 正确，虽然 b 在内存中，但 b+5 的结果放在寄存器中，它没有在内存中分配空间，因此是右值 、
//   //int&& e = a; 
//   //e虽然接收的必须是右值，但它本身是左值。换句话说e是一种特殊的变量，它是只能接收右值的变量。我们再从左值的本质来看，e也是占内存空间的，所以它肯定是左值。
//    //应该更改成
//    int&& e = move(a);
//}


//move的实现
//template <typename T>
//typename remove_reference<T>::type&& move(T&& t)
//{
//    return static_case<typename remove_reference<T>::type&&>(t);
//}
//
////通用引用
//template<typename T>
//void f(T&& param) {
//    std::cout << "the value is " << param << std::endl;
//}
//
//int main(int argc, char* argv[]) {
//
//    int a = 123;
//    auto&& b = 5;   //通用引用，可以接收右值
//
//    int&& c = a;    //错误，右值引用，不能接收左值
//
//    auto&& d = a;   //通用引用，可以接收左值
//
//    const auto&& e = a; //错误，加了const就不再是通用引用了
//
//    func(a);         //通用引用，可以接收左值
//    func(10);        //通用引用，可以接收右值
//}

//#include <iostream>
//using namespace std;
//class shape {
//public:
//    shape() { cout << "shape" << endl; }
//
//    virtual ~shape() {
//        cout << "~shape" << endl;
//    }
//};
//class circle : public shape {
//public:
//    circle() { cout << "circle" << endl; }
//
//
//    ~circle() {
//        cout << "~circle" << endl;
//    }
//};
//class triangle : public shape {
//public:
//    triangle() { cout << "triangle" << endl; }
//
//
//    ~triangle() {
//        cout << "~triangle" << endl;
//    }
//};
//class rectangle : public shape {
//public:
//    rectangle() { cout << "rectangle" << endl; }
//
//    ~rectangle() {
//        cout << "~rectangle" << endl;
//    }
//};
//class result {
//public:
//    result() { puts("result()"); }
//
//    ~result() { puts("~result()"); }
//};
//result process_shape(const shape& shape1, const shape& shape2) {
//    puts("process_shape()");
//    return result();
//}
//int main() {
//    result(move(process_shape(circle(), triangle())));
//}



//class MyClass
//{
//public:
//    MyClass(const std::string& s)
//        : str{ s }
//    {};
//    MyClass(const MyClass& m)
//    {
//        cout << "MyClass(const MyClass& m)" << endl;
//        str = m.str;
//    }
//
//private:
//    std::string str;
//};
//
//int main()
//{
//    MyClass A{ "hello" };
//    MyClass B = A;
//    return 0;
//}

//class MyClass
//{
//public:
//    MyClass(const std::string& s)
//        : str{ s }
//    {};
//
//private:
//    std::string str;
//};
//int main()
//{
//    std::vector<MyClass> myClasses;
//    MyClass tmp{ "hello" };
//    myClasses.push_back(tmp);
//    myClasses.push_back(tmp);
//    return 0;
//}

//namespace wrt
//{
//    class vector
//    {
//    public:
//        void push_back(const MyClass& value)  // const MyClass& 左值引用
//        {
//            // 执行拷贝操作
//        }
//
//        void push_back(MyClass&& value)  // MyClass&& 右值引用
//        {
//            // 执行移动操作
//        }
//    };
//    class MyClass
//    {
//    public:
//        MyClass(const std::string& s) //拷贝构造
//            : str{ s }
//        {};
//        MyClass(MyClass&& rvalue) //移动构造
//            :str{ std::move(rvalue.str) }
//        {}
//
//    private:
//        std::string str;
//    };
//}
//int main()
//{
//    std::vector<wrt::MyClass> myClasses;
//    wrt::MyClass tmp{ "hello" };
//    myClasses.push_back({ std::move(tmp) });
//    myClasses.push_back(tmp);  // 这里执行拷贝操作，将tmp中的数据拷贝给容器中的元素
//    myClasses.push_back(std::move(tmp));  // 这里执行移动操作，容器中的元素直接将tmp的数据转移给自己
//
//    return 0;
//}

//class MyClass
//{
//public:
//    MyClass()
//        : val{ 998 }
//    {
//        name = new char[] { "Peter" };
//    }
//
//    // 实现移动构造函数
//    MyClass(MyClass&& rValue) noexcept
//        : val{ std::move(rValue.val) }  // 转移数据
//    {
//        rValue.val = 0;  // 清除被转移对象的数据
//
//        name = rValue.name;  // 转移数据
//        rValue.name = nullptr;  // 清除被转移对象的数据
//    }
//
//    ~MyClass()
//    {
//        if (nullptr != name)
//        {
//            delete[] name;
//            name = nullptr;
//        }
//    }
//
//private:
//    int val;
//    char* name;
//};
//int main()
//{
//    MyClass A{};
//    MyClass B{ std::move(A) };  // 通过移动构造函数创建新对象B
//    return 0;
//}


//class MyClass
//{
//public:
//    MyClass()
//        : val{ 998 }
//    {
//        name = new char[] { "Peter" };
//    }
//
//    MyClass(MyClass&& rValue) noexcept
//        : val{ std::move(rValue.val) }
//    {
//        rValue.val = 0;
//
//        name = rValue.name;
//        rValue.name = nullptr;
//    }
//
//    // 移动赋值运算符
//    MyClass& operator=(MyClass&& myClass) noexcept
//    {
//        val = myClass.val;
//        myClass.val = 0;  //数据清空
//
//        name = myClass.name;
//        myClass.name = nullptr; //数据置空
//
//        return *this;
//    }
//
//    ~MyClass()
//    {
//        if (nullptr != name)
//        {
//            delete[] name;
//            name = nullptr;
//        }
//    }
//
//private:
//    int val;
//    char* name;
//};
//int main()
//{
//    MyClass A{};
//    MyClass B{};
//    B = std::move(A);  // 使用移动赋值运算符将对象A赋值给对象B
//    return 0;
//}


//class MyClass
//{
//public:
//    MyClass()
//    {}
//
//    // 我们定义了拷贝构造函数，这会禁止编译器自动生成移动构造函数和移动赋值运算符
//    MyClass(const MyClass& value)
//    {}
//};
//
//int main()
//{
//    MyClass A{};
//    MyClass B{ std::move(A) };  // 执行的是拷贝构造函数来创建对象B
//    return 0;
//}


//class MyBaseClass
//{
//public:
//    virtual ~MyBaseClass()
//    {}
//};
//
//class MyClass : MyBaseClass  // 子类没有实现自己的析构函数
//{};
//int main()
//{
//    MyClass A{};
//    MyClass B{ std::move(A) };  // 这里将执行编译器自动生成的移动构造函数
//    return 0;
//}

//
//class MyClass
//{
//public:
//    MyClass(int x)
//        :_x(x)
//    {}
//    MyClass(const MyClass& lvalue) //拷贝构造函数
//    {
//        cout << "MyClass(const MyClass& lvalue" << endl;
//        _x = lvalue._x;
//        throw runtime_error("copy exception");
//    }
//    // 我们定义了移动构造函数，这会禁止编译器自动生成移动赋值运算符，并且对移动赋值运算符的调用会产生编译错误
//    MyClass(MyClass&& rValue) noexcept
//        :_x(std::move(rValue._x))
//    {
//        rValue._x = 0;
//        cout << " MyClass(MyClass&& rValue) noexcept" << endl;
//        throw runtime_error("copy exception");
//
//    }
//    ~MyClass()
//    {
//        cout << "~MyClass()" << endl;
//    }
// private:
//     int _x;
//};
//int main()
//{
//    try
//    {
//        MyClass A{ 1 };
//        MyClass B{ std::move(A) };
//    }
//    catch(runtime_error e)
//    {
//        cout << "Catch!!!" <<e.what()<< endl;
//    }
//    return 0;
//}


// 模板中的&&不代表右值引用，而是万能引用，其既能接收左值又能接收右值。
// 模板的万能引用只是提供了能够接收同时接收左值引用和右值引用的能力，
// 但是引用类型的唯一作用就是限制了接收的类型，后续使用中都退化成了左值，
// 我们希望能够在传递过程中保持它的左值或者右值的属性, 就需要用我们下面学习的完美转发
//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const 左值引用" << endl; }
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const 右值引用" << endl; }
//
//template<typename T>
//void PerfectForward(T&& t)
//{
//	Fun(t);
//}
//int main()
//{
//	PerfectForward(10);  // 右值
//	int a;
//	PerfectForward(a);  // 左值
//	PerfectForward(std::move(a)); // 右值
//	const int b = 8;
//	PerfectForward(b);  //const 左值
//	PerfectForward(std::move(b)); // const 右值
//	return 0;
//}


//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const 左值引用" << endl; }
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const 右值引用" << endl; }
//// std::forward<T>(t)在传参的过程中保持了t的原生类型属性。
//template<typename T>
//void PerfectForward(T&& t)
//{
//	Fun(std::forward<T>(t));   //完美转发
//}
//int main()
//{
//		PerfectForward(10);  // 右值
//	int a;
//	PerfectForward(a);  // 左值
//	PerfectForward(std::move(a)); // 右值
//	const int b = 8;
//	PerfectForward(b);  //const 左值
//	PerfectForward(std::move(b)); // const 右值
//	return 0;
//}


//
//template<class T>
//struct ListNode
//{
//	ListNode* _next = nullptr;
//	ListNode* _prev = nullptr;
//	T _data;
//};
//template<class T>
//class List
//{
//	typedef ListNode<T> Node;
//public:
//	List()
//	{
//		_head = new Node;
//		_head->_next = _head;
//		_head->_prev = _head;
//	}
//	void PushBack(T&& x)
//	{
//		Insert(_head, x);
//		//Insert(_head, std::forward<T>(x));
//	}
//	void PushFront(T&& x)
//	{
//		//Insert(_head->_next, x);
//		Insert(_head->_next, std::forward<T>(x));
//	}
//	void Insert(Node* pos, T&& x)
//	{
//		Node* prev = pos->_prev;
//		Node* newnode = new Node;
//		newnode->_data=x; 
//
//		//newnode->_data = std::forward<T>(x); // 关键位置
//		// prev newnode pos
//		prev->_next = newnode;
//		newnode->_prev = prev;
//		newnode->_next = pos;
//		pos->_prev = newnode;
//	}
//	void Insert(Node* pos, const T& x)
//	{
//		Node* prev = pos->_prev;
//		Node* newnode = new Node;
//		newnode->_data = x; // 关键位置
//		// prev newnode pos
//		prev->_next = newnode;
//		newnode->_prev = prev;
//		newnode->_next = pos;
//		pos->_prev = newnode;
//	}
//private:
//	Node* _head;
//};
//int main()
//{
//	List<std::string> lt;
//	lt.PushBack("1111");
//	lt.PushFront("2222");
//	return 0;
//}

//#include <assert.h>
//namespace wrt
//{
//	class string
//	{
//	public:
//		typedef char* iterator;
//		iterator begin()
//		{
//			return _str;
//		}
//
//		iterator end()
//		{
//			return _str + _size;
//		}
//
//		string(const char* str = "")
//			:_size(strlen(str))
//			, _capacity(_size)
//		{
//			//cout << "string(char* str)" << endl;
//
//			_str = new char[_capacity + 1];
//			strcpy(_str, str);
//		}
//
//		// s1.swap(s2)
//		void swap(string& s)
//		{
//			::swap(_str, s._str);
//			::swap(_size, s._size);
//			::swap(_capacity, s._capacity);
//		}
//
//		// 拷贝构造
//		string(const string& s)
//			:_str(nullptr)
//		{
//			cout << "string(const string& s) -- 深拷贝" << endl;
//
//			string tmp(s._str);
//			swap(tmp);
//		}
//
//		// 移动构造
//		string(string&& s)
//			:_str(nullptr)
//		{
//			cout << "string(string&& s) -- 移动拷贝" << endl;
//			swap(s);
//		}
//
//		// 赋值重载
//		string& operator=(const string& s)
//		{
//			cout << "string& operator=(string s) -- 深拷贝" << endl;
//			string tmp(s);
//			swap(tmp);
//
//			return *this;
//		}
//
//		~string()
//		{
//			delete[] _str;
//			_str = nullptr;
//		}
//
//		char& operator[](size_t pos)
//		{
//			assert(pos < _size);
//			return _str[pos];
//		}
//
//		void reserve(size_t n)
//		{
//			if (n > _capacity)
//			{
//				char* tmp = new char[n + 1];
//				strcpy(tmp, _str);
//				delete[] _str;
//				_str = tmp;
//
//				_capacity = n;
//			}
//		}
//
//		void push_back(char ch)
//		{
//			if (_size >= _capacity)
//			{
//				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
//				reserve(newcapacity);
//			}
//
//			_str[_size] = ch;
//			++_size;
//			_str[_size] = '\0';
//		}
//
//		//string operator+=(char ch)
//		string& operator+=(char ch)
//		{
//			push_back(ch);
//			return *this;
//		}
//
//		string operator+(char ch)
//		{
//			string tmp(*this);
//			tmp += ch;
//			return tmp;
//		}
//
//		const char* c_str() const
//		{
//			return _str;
//		}
//	private:
//		char* _str;
//		size_t _size;
//		size_t _capacity; // 不包含最后做标识的\0
//	};
//
//	wrt::string to_string(int value)
//	{
//		bool flag = true;
//		if (value < 0)
//		{
//			flag = false;
//			value = 0 - value;
//		}
//
//		bit::string str;
//		while (value > 0)
//		{
//			int x = value % 10;
//			value /= 10;
//
//			str += ('0' + x);
//		}
//
//		if (flag == false)
//		{
//			str += '-';
//		}
//
//		std::reverse(str.begin(), str.end());
//		return str;
//	}
//}
//
//int main()
//{
//	wrt::string s1("hello world");
//
//	wrt::string ret1 = s1;
//	wrt::string ret2 = (s1+'!');
//
//	//wrt::string ret3 = move(s1);
//
//	return 0;
//}
//
//class A
//{
//public:
//    A() {}
//
//};
//
//void foo()
//{
//    throw new A;
//}
//
//int main()
//{
//    try {
//        foo();
//    }
//    catch(A a)
//    { }
//
//}
//class Solution {
//public:
//    int maxPower(string s) {
//        int n = s.size();
//        int l = 0, r = 0, ans = 0, tmp = 0;
//        while (l < n)
//        {
//            while (r < n && s[l] == s[r])
//            {
//                tmp++;
//                r++;
//            }
//            ans = max(ans, tmp - 1);
//            l = r;
//            tmp = 0;
//        }
//        return ans;
//    }
//};
//int main()
//{
//  /*  Solution s;
//    s.maxPower("letcoode");*/
//    int b = 10;
//    int* a = &b;
//    cout<<sizeof(a)<<endl;
//
//    return 0;
//}


//
//enum weekday
//{
//	sun,
//	mon = 3,
//	tue,
//	wed
//};
//int main()
//{
//	enum weekday workday;
//	enum weekday a;
//
//	workday = wed;
//	a = sun;
//	printf("%d\n", a);
//		return 0;
//}