#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)
#include<stdio.h>
#include<tchar.h>
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>
#include <WS2tcpip.h>
#include <string>
#pragma comment(lib, "ws2_32.lib")


#include <iostream>
#include <memory>
#include <cstdlib>
#include <cstring>

#include <stdio.h>

#include "err.hpp"

using namespace std;

static void Usage(string proc)
{
    cout << "Usage:\n\t" << proc << "port\n"
        << endl;
}

string severip = "43.143.220.147";
uint16_t severport = 8877;

int main()
{
    WSAData wsd;
    if (WSAStartup(MAKEWORD(2, 2), &wsd) != 0) {
        cout << "WSAStartup Error = " << WSAGetLastError() << endl;
        return 0;
    }
    SOCKET sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        std::cerr << "ctreate error" << strerror(errno) << std::endl;
        exit(CREATE_ERR);
    }
    struct sockaddr_in sever;
    memset(&sever, 0, sizeof(sever));
    sever.sin_port = htons(severport);
    sever.sin_addr.s_addr = inet_addr(severip.c_str());
    // client肯定要bind 但是不需要自己bind OS帮忙

    while (true)
    {
        // 用户输入
        cout << "Please Enter>>>";
        string message;
        getline(cin, message);
        //首次调用系统调用 发送数据的时候 OS在底层随机选择一个clienip+clientport bind&&构建发送的数据报文

        //发
        sendto(sock, message.c_str(), message.size(), 0, (struct sockaddr*)&sever, sizeof(sever));
        // 收
        char buffer[1024];
        struct sockaddr_in temp; //可能来自不同服务器的报文，最好还是把这个保留
        int len = sizeof(temp);
        int n = recvfrom(sock, buffer, sizeof(buffer) - 1, 0, (struct sockaddr*)&temp, &len);
        if (n > 0)
        {
            buffer[n] = '\0';
             cout<<"sever echo>>>"<<buffer<<endl;
        }
    }
    closesocket(sock);
    WSACleanup();

    return 0;
}