#include "queue.h"
typedef int type;

typedef struct Stack
{
	type* a;
	int top;// 初始化成0 表示栈顶位置下一个位置的下标
	int capacity;
}ST;
bool Empty(ST* p)//判断栈是不是空
{
	assert(p);
	return p->top == 0;
}
void InitST(ST* p)
{
	type* tmp = (type*)malloc(sizeof(type) * 4);
	if (tmp == NULL)
	{
		perror("InitST");
		exit(-1);
	}
	p->a = tmp;
	p->capacity = 4;
	p->top = 0;
}

void PushST(ST* p, type x)//在栈顶压数据
{
	if (p->capacity == p->top)//表示需要扩容
	{
		type* tmp = (type*)realloc(p->a, p->capacity * 2 * sizeof(type));
		if (tmp == NULL)
		{
			perror("realloc");
			exit(-1);
		}
		p->a = tmp;
		p->capacity *= 2;
	}
	p->a[p->top] = x;
	p->top++;
}

void PopST(ST* p)//从栈顶删除数据
{
	assert(p);
	assert(!Empty(p));
	p->top--;
}

void DestoryST(ST* p)//销毁栈
{
	assert(p);
	free(p->a);
	p->a = NULL;
	p->capacity = p->top = 0;
}

type StackTop(ST* p)//显示栈顶的数据
{
	assert(p);
	assert(!Empty(p));
	return p->a[p->top - 1];
}

bool isValid(char* s) {
	ST st;
	InitST(&st);
	while (*s)
	{
		if (*s == '(' || *s == '[' || *s == '{')
		{
			PushST(&st, *s);
			++s;
		}
		else
		{
			if (Empty(&st))
			{
				DestoryST(&st);
				return false;

			}
			char top = StackTop(&st);
			PopST(&st);
			if ((*s == ')' && top != '(') || (*s == ']' && top != '[') || (*s == '}' && top != '{'))
			{
				DestoryST(&st);
				return false;
			}
			else {
				s++;
			}

		}

	}
	bool ret = Empty(&st);
	DestoryST(&st);
	return ret;
}