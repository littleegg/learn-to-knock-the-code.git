#include "stack.h"
void InitST(ST* p)//初始化
{
	ST* tmp = (type*)malloc(sizeof(type)*4);
	if (tmp == NULL)
	{
		perror("InitST");
		exit(-1);
	}
	p->a = tmp;
	p->capacity = 4;
	p->top = 0;
}

void PushST(ST* p, type x)//在栈顶压数据
{
	if (p->capacity == p->top)//表示需要扩容
	{
		ST* tmp = (type*)realloc(p->a, p->capacity* 2);
		if (tmp == NULL)
		{
			perror("realloc");
			exit(-1);
		}
		p->a = tmp;
		p->capacity *= 2;
	}
	p->a[p->top] = x;
	p->top++;
}

void PopST(ST* p)//从栈顶删除数据
{
	assert(p);
	assert(!Empty(p));
	p->top--;
}

void DestoryST(ST* p)//销毁栈
{
	assert(p);
	free(p->a);
	p->a = NULL;
	p->capacity = p->top = 0;
}

bool Empty(ST* p)//判断栈是不是空
{
	assert(p);
	return p->top == 0;
}

type StackTop(ST* p)//显示栈顶的数据
{
	assert(p);
	assert(!Empty);
	return p->a[p->top - 1];
}

type SizeST(ST* p)//栈里面数据的个数
{
	assert(p);
	return p->top;
}