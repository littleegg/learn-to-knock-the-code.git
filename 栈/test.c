#include "queue.h"
//void test1(void)
//{
//	ST st;
//	InitST(&st);
//	PushST(&st, 1);
//	PushST(&st, 2);
//	PushST(&st, 3);
//	PushST(&st, 4);
//	PushST(&st, 5);
//
//	PopST(&st);
//	PopST(&st);
//	PopST(&st);
//
//	printf("size:%d", SizeST(&st));
//	DestoryST(&st);
//}

void test1(void)
{
	Q q;
	QInit(&q);
	QPush(&q, 1);
	QPush(&q, 2);
	QPush(&q, 3);
	QPush(&q, 4);
	Qprint(&q);

	QPop(&q);
	Qprint(&q);
	printf("队头元素：%d\n", QFront(&q));
	printf("队尾元素：%d\n", QBack(&q));
	printf("有效元素个数元素：%d\n", QSize(&q));

	QDestroy(&q);
}
int main()
{
	test1();
	return 0;
}