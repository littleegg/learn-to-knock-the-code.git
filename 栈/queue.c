#include "queue.h"
// 初始化队列 
void QInit(Q* q)
{
	q->front = NULL;
	q->rear = NULL;
}
// 队尾入队列 ,尾插
void QPush(Q* q, type data)
{

	node* newnode = (node*)malloc(sizeof(node));
	if (newnode == NULL)
	{
		perror("newnode");
		exit(-1);
	}
	newnode->data = data;
	newnode->next = NULL;

	if (QEmpty(q))
	{
      q->front = newnode;
      q->rear = newnode;
	}
	else
	{
    q->rear ->next= newnode;
	q->rear = q->rear->next;
	}
	
}
// 队头出队列 头删
void QPop(Q* q)
{
	assert(q);
	assert(!QEmpty(q));
	node* tmp = q->front->next;
	free(q->front);
	q->front = tmp;

}
// 获取队列头部元素 
type QFront(Q* q)
{
	assert(q);
	return q->front->data;
}
// 获取队列队尾元素 
type QBack(Q* q)
{
	assert(q);
	return q->rear->data;
}
// 获取队列中有效元素个数 
int QSize(Q* q)
{
	node* cur = q->front;
	int size = 0;
	while (cur)
	{
		size++;
		cur = cur->next;
	}
	return size;
}
// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
bool QEmpty(Q* q)
{
	assert(q);
	return q->front==NULL;
}
// 销毁队列 
void QDestroy(Q* q)
{
	node* cur = q->front;
	while (cur&&cur->next)
	{
		node* tmp = cur->next;
		free(cur);
		cur = tmp;
	}
}


//打印队列
void Qprint(Q* q)
{
	assert(q);
	node* cur = q->front;
	while (cur)
	{
		printf("%d", cur->data);
		cur = cur->next;
	}
	printf("\n");
}