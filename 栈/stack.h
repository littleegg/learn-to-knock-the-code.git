#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
typedef int type;
typedef struct Stack
{
	type *a;
	int top;// 初始化成0 表示栈顶位置下一个位置的下标
	int capacity;
}ST;

void InitST(ST* p);//初始化
void PushST(ST* p,type x);//在栈顶压数据
void PopST(ST* p);//从栈顶删除数据
void DestoryST(ST* p);//销毁栈
bool Empty(ST* p);//判断栈是不是空
type StackTop(ST* p);//显示栈顶的数据
type SizeST(ST* p);//栈里面数据的个数