#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

typedef int type;
typedef struct QueueNode
{
	struct QueueNode* next;
	type data;
}node;

typedef struct Queue
{
	node* front;
	node* rear;
}Q;

// 初始化队列 
void QInit(Q* q);
// 队尾入队列 
void QPush(Q* q, type data);
// 队头出队列 
void QPop(Q* q);
// 获取队列头部元素 
type QFront(Q* q);
// 获取队列队尾元素 
type QBack(Q* q);
// 获取队列中有效元素个数 
int QSize(Q* q);
// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
bool QEmpty(Q* q);
// 销毁队列 
void QDestroy(Q* q);
//打印队列
void Qprint(Q* q);