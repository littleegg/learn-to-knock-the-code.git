#define _CRT_SECURE_NO_WARNINGS
//#include <iostream>
//#include <unordered_map>
//#include <vector>
//using namespace std;
//int main()
//{
//	int n;
//	cin >> n;
//	string s;
//	s.resize(64);
//	unordered_map <string, int> m;
//	while (n--)
//	{
//		for (auto& e : s)
//			cin >> e;
//		if (m.count(s)) m[s]++;
//		else m[s] = 1;
//		cout << m[s]<< endl;
//	}
//	return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//int main()
//{
//	int n, d;
//	cin >> n >> d;
//	vector<vector<int>> q(n, vector<int>(d));
//	vector<vector<int>> k(n, vector<int>(d));
//	vector<vector<int>> v(n, vector<int>(d));
//	vector<int> w(n);
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < d; j++) cin >> q[i][j];
//	}
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < d; j++) cin >> k[i][j];
//	}
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < d; j++) cin >> v[i][j];
//	}
//	for (int i = 0; i < n; i++) cin >> w[i];
//	vector<vector<long long >> ans(n, vector<long long >(d));
//	vector<vector<long long >> tmp(d, vector<long long >(d));
//
//	for (int i = 0; i < d; i++)
//	{
//		for (int j = 0; j < d; j++)
//			for (int a = 0; a < n; a++) tmp[i][j] += k[a][i] * v[a][j];
//	}
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < d; j++)
//		{
//			for (int a = 0; a < d; a++)
//				ans[i][j] += q[i][a] * tmp[a][j];
//			ans[i][j] *= (long long)w[i];
//		}
//	}
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < d; j++)
//			cout << ans[i][j] << " ";
//		cout << endl;
//	}
//		
//	return 0;
//}


#include <iostream>
#include<string>
#include <vector>
#include <bitset>
//using namespace std;
//int idx = 0,p,n;
//const int N = 2e6 + 10;
//string ans;
//string readBytes(int num)
//{
//    //读入的一个字节是两个字符
//	string s;
//	s.resize(2 * num);
//	for (int i = 0; i < 2 * num; i++) cin >> s[i];
//	idx += num * 2;
//	return s;
//}
//void trackBack(int o, int l)
//{
//	int start = ans.size() - o * 2;
//	int len = o * 2;
//	string back = ans.substr(start, len);
//	int a = 0;
//	while (a < l * 2 - l * 2 % len)
//	{
//		ans += back;
//		a += len;
//	}
//	ans += back.substr(0, l * 2 % len);
//}
//int main()
//{
//	cin >> n;
//	string bts;
//	vector<int> c;
//	int v_c;
//	while ((bts = readBytes(1) ) >= "80")
//	{
//		v_c = stoi(bts, nullptr, 16);  //输入的字符串是16进制的格式
//		v_c -= 128;//只有低七位保留数据 所以要减去最高位，减去1000 0000=128
//		c.push_back(v_c);
//	}
//	v_c = stoi(bts, nullptr, 16); //最后一个字节高位是0  不需要减去
//	c.push_back(v_c);
//	int length = 0; //原始数据的长度
//	for (int i = 0; i < c.size(); i++) length += c[i] * pow(128, i); 
//    while (idx < n * 2)
//    {
//        // 接下来是数据域
//        // 读入一个字节
//        bts = readBytes(1);
//        string string_to_binary = bitset<8>(stoi(bts, nullptr, 16)).to_string();  //读到的字节转成二进制
//        string lowest_two_digits = string_to_binary.substr(6, 2);  //低两位
//        if (lowest_two_digits == "00")
//        {
//            //表示这是一个字面量，取其高六位
//            string high_six_digits = string_to_binary.substr(0, 6);
//            //变成二进制
//            int ll = stoi(high_six_digits, nullptr, 2);
//            // l <= 60，高六位 ll 表示 l - 1
//            if (ll <= 59)
//                ans += readBytes(ll + 1);  //题目中ll=59  读取[0x01,0x3B]  一共是60个  所以加一
//            else
//            {
//                // 第一个字节的高六位存储的值为 60、61、62 或 63 时，分别代表 l - 1 用 1、2、3 或 4 个字节表示
//                int literal_length = ll - 59;
//                // 按照小端序重组字符串 0x01 0x0A => 0x0A01
//                string string1 = readBytes(literal_length);
//                string string2;
//                // 字符串每两位反转
//                for (int i = string1.length() - 2; i >= 0; i -= 2)
//                    string2 += string1.substr(i, 2);
//                int l = 1 + stoi(string2, nullptr, 16); // 字面量长度
//                ans += readBytes(l);
//            }
//        }
//        else if (lowest_two_digits == "01")
//        {
//            // 第 2 ~ 4 位即 从下标 3 开始的三位 001 011 01
//            string two_to_four_digits = string_to_binary.substr(3, 3);
//            // l - 4 占 3 位，存储于首字节的 2 至 4 位中
//            int l = stoi(two_to_four_digits, nullptr, 2) + 4;
//            // o 占 11 位，其低 8 位存储于随后的字节中，高 3 位存储于首字节的高 3 位中
//            string high_three_digits = string_to_binary.substr(0, 3);
//            string next_byte_binary = bitset<8>(stoi(readBytes(1), nullptr, 16)).to_string();
//            int o = stoi(high_three_digits + next_byte_binary, nullptr, 2);
//            // 回溯引用
//            trackBack(o, l);
//        }
//        else if (lowest_two_digits == "10")
//        {
//            string high_six_digits = string_to_binary.substr(0, 6);
//            // l 占 6 位，存储于首字节的高 6 位中
//            int l = stoi(high_six_digits, nullptr, 2) + 1;
//            // o 占 16 位，以小端序存储于随后的两个字节中
//            string string1 = readBytes(2);
//            string string2;
//            // 字符串每两位反转
//            for (int i = string1.length() - 2; i >= 0; i -= 2)
//                string2 += string1.substr(i, 2);
//            int o = stoi(string2, nullptr, 16);
//            // 回溯引用
//            trackBack(o, l);
//        }
//    }
//    for (int i = 0; i < ans.length(); i++)
//    {
//        cout << ans[i];
//        // 输出，每16个字符加一个换行
//        if ((i + 1) % 16 == 0) cout << endl;
//    }
//    // 若最后一行不能凑8个，则补一个换行
//    if (ans.length() % 16) cout << endl;
//    return 0;
//}
//

#include <iostream>
#include <string>
using namespace std;
string ans;
void trackBack1(int o, int l)
{
	int start = ans.length() - o * 2;
	int len = o * 2;
	string back_track_string = ans.substr(start, len);
	int cnt = 0;
	while (cnt < l * 2 - l * 2 % len)
	{
		ans += back_track_string;
		cnt += len;
	}
	ans += back_track_string.substr(0, l * 2 % len);
	
}
void trackBack2(int o, int l)
{
	int p = ans.size();
	if (o >= l)
	{
		int begin = p - o;
		ans += ans.substr(begin, l);
	}
	else {
		int count = 0;
		while (l>count)
		{
			int begin = p - o;
			if(l-count < o)
			ans += ans.substr(begin, l-count);
			else 
			ans += ans.substr(begin,o);

			count += o;
		}
	}
}
int main()
{
	ans = "abcde";
	trackBack1(3, 2);
	trackBack2(2, 5);
}