#pragma once
#include <stdio.h>
#include <time.h>
#include  <string.h>
#include <stdlib.h>
void Insertsort(int* a, int n); //a是一个数组指针，n是数据元素个数
void Shellsort(int* a, int n);
void Printf(int* a, int n);
void Selectsort(int* a, int n);
void Toptest();
void bubblesort(int* a, int n);
void Quicksort_Hoare(int* a, int left, int right);
int Getmid(int* a, int begin, int end);
int Partsort2_Quicksort_Hole(int* a, int begin, int end);
void Mergesort(int* a, int n);
void _Mergesort(int* a, int begin, int end, int* tmp);
void MergesortNonR(int* a, int n);
void Quicksort_New(int* a, int begin, int end);

