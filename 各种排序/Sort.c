#define _CRT_SECURE_NO_WARNINGS
#include "Sort.h"

//插入排序
//时间复杂度是N^2，因为最坏的情况要挪动1,2,3...是一个等差数列，N^2的数量级，最好是O(N)（接近有序）
void Insertsort(int*a ,int n) //a是一个数组指针，n是数据元素个数
{
	for (int i = 0; i < n - 1; i++)
	{
		int end = i;
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (a[end] > tmp)
			{
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}

//希尔排序,如果是i++就叫gap并排
void Shellsort(int *a,int n)
{
	int gap = 3;
	for (int j = gap; j > 0; j--)
	{
		for (int i = 0; i < n - gap; i += gap)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}

//gap>1预排序，gap == 1直接排序
void Shellsort2(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap /= 2; //或者gap=gap/3+1;保证一定最后是1
		for (int j = gap; j > 0; j--)
		{
			for (int i = 0; i < n - gap; i += gap)
			{
				int end = i;
				int tmp = a[end + gap];
				while (end >= 0)
				{
					if (a[end] > tmp)
					{
						a[end + gap] = a[end];
						end -= gap;
					}
					else
					{
						break;
					}
				}
				a[end + gap] = tmp;
			}
		}
	}
}


void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

//这个选择排序是我写的
void Selectsort1(int* a, int n)
{
	int tmp;
	for (int i = 1; i < n; i++)
	{
		int tmp = a[i];
		for (int j = i+1; j < n; j++)
		{
			if (a[j] < a[i])
			{
				Swap(&a[j], &tmp);
			}
		}
		a[i] = tmp;
	}
}

void Selectsort(int* a, int n) //时间复杂度是N^2
{
	int begin = 0;
	int end = n-1;
	while(begin<end)
	{
		for (int i = begin; i < end; i++)
	   {
		if (a[i] < a[begin])
		{
			Swap(&a[i], &a[begin]);
		}
		if (a[i] > a[end])
		{
			Swap(&a[i], &a[end]);
		}
	   }
		begin++;
		end--;
	}
	
}

//三数取中,保证key是数据不是最值，排除有序对快排的影响
int Getmid(int* a, int begin, int end)
{
	int mid = (begin + end) / 2;
	if (a[begin] < a[mid])
	{
		if (a[mid] < a[end])
		{
			return mid;
		}
		else
		{
			if (a[begin] < a[end])
				return end;
			else
				return begin;
		}
	}
	else
	{
		if (a[mid] > a[end])
			return mid;
		else
		{
			if (a[begin] > a[end])
				return end;
			else
				return begin;
		}
	}
}

void Quicksort(int* a, int begin,int end)
{
	if (begin >= end)
	{
		return;
	}
	//减少递归调用的次数
	//可以进行区间优化，其实最后区间长度很小没必要用子问题，直接插入排序就行
	if ((end - begin + 1) < 5)
	{
		Insertsort(a, end - begin + 1);
	}
	else
	{
	int key = Part1Quicksort_Hoare(a, begin, end);
	Quicksort(a,begin,key-1);
	Quicksort(a ,key+1,end);
	}
}
int Part1Quicksort_Hoare(int*a,int begin,int  end)
{
	int mid = Getmid(a, begin, end);
	Swap(&a[begin], &a[mid]);
	int left = begin, right = end;
	int key = left;
	while (left < right)
	{
		while (a[right] > key && left < right)
		{
			right--;
		}
		while (a[left] < key && left < right)
		{
			left++;
		}
		Swap(&a[left], &a[right]);
	}
	Swap(&a[left], &a[key]);
	key = left;

	return key;
}
//快排有一个优化的版本，当数据很多都等于key的时候老版本就会性能下降
void Quicksort_New(int* a, int begin,int end)
{
	if (begin >= end)
	{
		return;
	}
	if ((end - begin + 1) < 5)
	{
		Insertsort(a, end - begin + 1);
	}
	else
	{
		int mid = Getmid(a, begin, end);
		Swap(&a[begin], &a[mid]);
		int left = begin;
		int right = end;
		int cur = begin + 1;
		int key = a[left];
		while (cur <= right)
		{
			if(a[cur]<a[left])
			{
				Swap(&a[cur++],&a[left++]);
			}
			if(a[cur]==a[left])
			{
				cur++;
			}
			if (a[cur] > a[right])
			{
				Swap(&a[cur], &a[right--]);
			}
		}
		//[begin,left-1] [left , right] [right+1,end]  其中，中间区域都是和key相等的
		Quicksort_New(a, begin, left - 1);
		Quicksort_New(a, right + 1, end);
	}
}
void bubblesort(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = i; j < n - i; j++)
		{
			if (a[j] > a[j + 1])
			{
				Swap(&a[j], &a[j + 1]);
			}
		}
	}
}

//挖坑法写快排
int Partsort2_Quicksort_Hole(int* a, int begin, int end)
{
	int mid = Getmid(a, begin, end);
	Swap(&a[begin], &a[mid]);
	int left = begin;
	int right = end;
	int hole = left;
	int key = a[hole];
	while (left < right)
	{
		while (a[right] > a[key] && left < right)
		{
			right--;
		}
		a[hole] = a[right];
		hole = right;
		while (a[left] < key && left < right)
		{
			left++;
		}
		a[hole] = a[left];
		hole = left;
	}
	a[hole] = a[key];
	return key;
}

//双指针方法写快排
int Partsort3_Quicksort_double(int* a, int begin,int end)
{
	if (begin == end)
		return 0;
	int* prev = &a[begin];
	int* cur = prev + 1;
	int key = begin;
	while (cur != &a[end])
	{
		while (*cur > a[key] &&cur!= &a[end])
		{
			cur++;
		}
		prev ++;
		Swap(cur,prev);
		cur++;
	}
	Swap(&a[key], prev);
	//这样就无法返回下标了
}

//这样写双指针
int Partsort3_Quicksort_double(int* a, int begin, int end)
{
	int key = begin;
	int prev = begin;
	int cur = prev++;
	while (cur <= end)
	{
		if (a[cur] < a[key])
		{
			Swap(&a[cur], &a[++prev]);
		}
		cur++;
	}
	Swap(&a[prev], &a[key]);
	key = prev;
	return key;
}

//快排还可以用非递归的方法写、
//注意我们是因为要带着区间范围这个参数才选择用子问题
//但是区间还可以用栈保存
void QuickNonR(int* a, int begin, int end)
{
	ST st; //需要拉进来栈的代码
	StackInit(&st);
	StactPush(&st, begin);
	StackPush(&st, end);
	while (!StackEmpty(&st))
	{
		int right = StackTop(&st);
		StackPop(&st);
		int left = StackTop(&st);
		StackPpop(&st);
		int key = Partsort2_Quicksort_Hole(a, left, right);
		如果按照前面的思路我还是想先排前面的区间就要先把后面的区间入栈
		【left,key-1】key【key+1,right】
		if (key + 1 < right)
		{
			StackPush(&st, key + 1);
			StackPush(&st, right);
		}
		if(left < key - 1)
		{
			StackPush(&st, left);
			StackPush(&st, key - 1);
		}

	}
	StackDestory(&st);
}

//归并排序，条件：两端有序区间，方法:取小的尾插
void _Mergesort(int* a, int begin, int end, int* tmp)
{
	if (begin >= end)
		return;
	//还是涉及分割区间，最后要变成两个比大小
	int mid = (begin + end) / 2;
	//[begin,mid][mid+1,end]
	//递归让子区间有序
	_Mergesort(a, begin, mid, tmp);
	_Mergesort(a, mid + 1, end, tmp);
	//此时两个大区间都有序了，可以进行归并
	//由于变量之间会相互影响，最好还是重新定义一下
	int begin1 = begin;
	int end1 = mid;
	int begin2 = mid + 1;
	int end2 = end;
	int i = begin;
	while (begin1<=end1 && begin2<=end2)
	{
		if (a[begin1] < a[begin2])
		{
			tmp[i] = a[begin1++];
		}
		else
		{
			tmp[i] = a[begin2++];
		}
		i++; 
	}
	while (begin1 <= end1)
	{
		tmp[i++] = a[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = a[begin2++];
	}
	memcpy(a + begin, tmp + begin, sizeof(int) * (end - begin + 1));

}

//也可以用非递归的方式
void MergesortNonR(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("Mergesort malloc fail");
		exit(-1);
	}
	 //每组有rangeN个数据要进行归并
	for (int rangeN = 1; rangeN < n; rangeN *= 2)
	{
		for (int i = 0; i < n; i += rangeN * 2)
		{
			int begin1 = i;
			int end1 = i+rangeN-1;
			int begin2 = rangeN + i;
			int end2 = i+2*rangeN-1;
			//end1 begin2 end2 都越界
			//可以修正也可以直接break
			//修正出一段不存在的区间然后所有数据相当于没动
			//如果在当前的i，break出去相当于跳出了这个i的循环，这一段没拷贝但是其他正常的地方还是要拷贝的所以不能把memcpy写在循环的外面（整体拷贝）
			if (end1 >= n) 
			{
				end2 = n-1;
				end1 = n - 1;
				begin2 = n;
			}
			//或者直接break；
			/*if (end1 >= n)
			{
				break;
			}*/
			else if (begin2 >= n)
			{
				begin2 = n;
				end2 = n - 1;
			}
			else if(end2>=n)
			{
				end2 = n-1;
			}
			int j = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] < a[begin2])
				{
					tmp[j++] = a[begin1++];
				}
				else
				{
					tmp[j++] = a[begin2++];
				}
			}
			while (begin1 <= end1)
			{
				tmp[j++] = a[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = a[begin2++];
			}
			memcpy(a + i, tmp + i, sizeof(int) * (end2-i+1));

		}
	}

	free(tmp);
	tmp = NULL;
}
void Mergesort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("Mergesort malloc fail");
		exit(-1);
	}
	//和上面的快排一样还是需要子函数
	_Mergesort(a, 0, n-1, tmp);

	free(tmp);
	tmp = NULL;
}
void Printf(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

//性能测试
void Toptest()
{
	srand(time(0));
	const int N = 10000;
	int* a1 = (int*)malloc(sizeof(int) * N);
	int* a2 = (int*)malloc(sizeof(int) * N);
	int* a3 = (int*)malloc(sizeof(int) * N);
	int* a4 = (int*)malloc(sizeof(int) * N);
	int* a5 = (int*)malloc(sizeof(int) * N);
	int* a6 = (int*)malloc(sizeof(int) * N);
	if (a1 == NULL)
	{
		perror("malloc a fail");
		exit(-1);
	}
	if (a2 == NULL)
	{
		perror("malloc a fail");
		exit(-1);
	}if (a3 == NULL)
	{
		perror("malloc a fail");
		exit(-1);
	}if (a4 == NULL)
	{
		perror("malloc a fail");
		exit(-1);
	}if (a5 == NULL)
	{
		perror("malloc a fail");
		exit(-1);
	}if (a6 == NULL)
	{
		perror("malloc a fail");
		exit(-1);
	}
	for (int i = 0; i < N; i++)
	{
		a1[i] = rand()+i;
		a2[i] = a1[i];
		a3[i] = a1[i];
		a4[i] = a1[i];
		a5[i] = a1[i];
		a6[i] = a1[i];
	}
	int begin1 = clock();
	Insertsort(a1, N);
	int end1 = clock();

	int begin2 = clock();
	Shellsort(a2, N);
	int end2 = clock();

	int begin3 = clock();
	Selectsort(a3, N);
	int end3 = clock();

	int begin4 = clock();
	//Insertsort(a4, N);
	int end4 = clock();

	int begin5 = clock();
	//Insertsort(a5, N);
	int end5 = clock();

	int begin6 = clock();
	//Insertsort(a6, N);
	int end6 = clock();

	printf("Insertsort:%d \n",end1-begin1);
	printf("Shellsort: %d \n", end2 - begin2);
	printf("Selectsort:%d \n", end3 - begin3);
	/*printf("Insertsort:%d %d \n", begin1, end1);
	printf("Insertsort:%d %d \n", begin1, end1);
	printf("Insertsort:%d %d \n", begin1, end1);*/

	free(a1);
	free(a2);
	free(a3);
	free(a4);
	free(a5);
	free(a6);
}
