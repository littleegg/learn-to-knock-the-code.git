#define _CRT_SECURE_NO_WARNINGS
#include "Sort.h"

void test1()
{
	int a[] = { 1,5,8,9,6,7 ,11,666,879};
	int n = sizeof(a) / sizeof(int);
	Insertsort(a,n );
	Printf(a, n);

	Shellsort(a, n);
	Printf(a, n);

	Selectsort(a, n);
	Printf(a, n);

	bubblesort(a, n);
	Printf(a, n);

	/*Quicksort_Hoare(a, 0,n-1);
	Printf(a, n);*/

	/*Quicksort_Hole(a, 0, n - 1);
	Printf(a, n);*/

	MergesortNonR(a, 0, n - 1);
	Printf(a, n);
}

int main()
{
	test1();
	//Toptest();
	return 0;
}