#define _CRT_SECURE_NO_WARNINGS
#include "Single List.h"

SL* Buy(type x) //开辟节点空间
{
	SL* newnode = (SL*)malloc(sizeof(SL));
	if (newnode == NULL)
	{
		perror("Buy");
		exit(0);
	}
	else
	{
		newnode->data = x;
		newnode->next = NULL;//先把指向下一个节点设为空

	}
	return newnode;
}

//需要把每个节点连起来
SL* Creat(type x)
{
	SL* phead = NULL;
	SL*ptail = NULL;
	for (type i = 0; i < x; ++i)
	{
		SL* newnode = Buy(i);

		if (phead == NULL)
		{
			ptail = phead = newnode;
		}
		else
		{
			ptail->next = newnode;
			ptail = newnode;
		}
	}
	return phead;
}
  
void SLPrint(SL* phead) //打印
{
	SL* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	if(cur==NULL)
	printf("NULL");

	printf("\n");
}


//尾插
void SLPushBack(SL** pphead,type x)
{
	SL* newnode = Buy(x);//新节点

	if (*pphead == NULL)
	{
		*pphead = newnode;

	}
	else
	{
		SL* tail = *pphead;

		while (tail->next)
		{
			tail = tail->next;
		}
		tail->next = newnode;
	}
}


//尾删
void PopBack(SL**pphead)
{
	SL* tail = *pphead;
	SL* prev = *pphead;
	if (tail == NULL)
	{
		printf("空\n");
	}
	else
	{
      while (tail->next) //第一种方法
	{
		  tail = tail->next;//tail后移
		  if (tail->next)
		  {
			  prev = prev->next; //prev后移
		  }
	}
	  free(tail);
	  prev->next = NULL;
		/*while (tail->next->next) //第二个方法
		{
			tail=tail->next;
	   }
		free(tail->next);
		tail->next = NULL;*/

	}
	
}

//头插
void PushFront(SL** pphead, type x)
{
	
	SL* newnode = Buy(x);
	newnode->next = *pphead;
	*pphead =newnode ;

}

//头删
void PopFront(SL** pphead)
{
	assert(pphead);
	
	SL* next = (*pphead)->next;
	free(*pphead);
	*pphead = next;
}
//查找
SL* SLFind(SL* pphead, type x)
{
	assert(pphead);
	SL* pos = pphead;
	while (pos->data != x)
	{
		pos=pos->next;
	}
	if (pos == NULL)
	{
		printf("无");
	}
	return pos;
}
//任意位置的插入
void SLInsertAfter(SL* pos, type x)
{
	assert(pos);
	SL* newnode = Buy(x);

	if (pos == NULL)
	{
		pos = newnode;
	}
	else
	{
		SL* next = pos->next;
		pos->next = newnode;
		newnode->next = next;
	}
}

//删除pos之后的所有
void SLEraseAfter(SL*pos)
{
	assert(pos);
	pos->next = NULL;
}

//销毁
void SLDestory(SL* plist)
{
	while (plist!= NULL)
	{

		SL* tmp = plist;
		plist=plist->next;
		free(tmp);
		tmp = NULL;
	}
}