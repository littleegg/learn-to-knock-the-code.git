#pragma once 
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef  int  type;
//创建一个节点的结构
typedef struct Single_List
{
	type data;
	struct Single_List* next;
}SL;

SL* Creat(type x);
SL* Buy(type x);//开辟节点空间
void SLPrint(SL* phead);
void SLPushBack(SL** pphead,type x);
void PopBack(SL** pphead);
void PushFront(SL** pphead, type x);
void PopFront(SL** pphead);
SL* SLFind(SL* pos, type x);
void SLInsertAfter(SL* pos, type x);
void SLEraseAfter(SL* pos);
void SLDestory(SL* plist);