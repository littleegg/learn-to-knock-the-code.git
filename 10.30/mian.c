#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int removeDuplicates(int* nums, int numsSize) {
    int* end = nums;
    int* head = nums;
    int count = 0;
    while (end)
    {
        if (*end != *head)
        {
            *(++head) = *end;
            count++;
        }
        end++;
    }
    return count + 1;
}
//int main()
//{
//    int nums[] = { 1,1,2 };
//    int numsSize = 3;
//    removeDuplicates(nums, numsSize);
//}