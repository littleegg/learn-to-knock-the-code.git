#define _CRT_SECURE_NO_WARNINGS
#include <iostream>

using std::endl;
using std::cout;
using namespace std;
//class A
//{
//public:
//	//explicit A(int a)
//    A(int a)
//		:age(a)
//		, num(a)
//	{
//		count++;
//	}
//	static int Get_count()
//	{
//		return count;
//	}
//private:
//	static int count;
//	int age;
//	int num;
//};
//
//int A::count = 0;
//
//int main()
//{
//	A aa1(1);
//	A aa2 = 1;
//	A aa3(4);
//	//cout << aa3.Get_count()<< endl;
//	cout << A::Get_count() << endl;
//}

//友元
//class Date
//{
//public:
//	/*Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//
//	}*/
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{}
//	void SetTime(int hour, int minate, int second)
//	{
//		_t._hour = hour;
//		_t._minate = minate;
//		_t._second = second;
//	}
//	friend ostream& operator<<(ostream& out, const Date& d); //友元函数的声明
//private:
//	int _year;
//	int _month;
//	int _day;
//	Time _t;
//};
//
//ostream& operator<<(ostream& out, const Date& d)  //友元函数 的定义
//{
//	out << d._year << "-" <<d. _month << "-" <<d. _day << endl;
//	return out;
//}

//友元类
//class Time
//{
//	friend class Date;  //Date 是 Time的朋友，可以访问Time 的非公有从成员
//public:	
//	Time(int hour=1 ,int minate=1,int second=1 )
//		:_hour(hour)
//		,_minate(minate)
//		,_second(second)
//	{}
// private:
//	 int _hour;
//	 int _minate;
//	 int _second;
//};
//class Date
//{
//public:
//	/*Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//
//	}*/
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{}
//	void SetTime(int hour, int minate, int second)
//	{
//		_t._hour = hour;
//		_t._minate = minate;
//		_t._second = second;
//	}
//	void Print()
//	{
//		cout << _year << "/" << _month << "/" << _day << "/" << _t._hour << "/" << _t._minate << "/" << _t._second << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//	Time _t;
//};
//int main()
//{
//	Date d1(2000,5,4);
//	d1.SetTime(4, 5, 7);
//	d1.Print();
//	return 0;
//}

//内部类
//Time是内部类 Date是外部类
//class Date
//{
//public:
//		Date(int year=1999, int month=1, int day=1)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	   {}
//		class Time {
//		public:
//				Time(int hour=1 ,int minate=1,int second=1 )
//		        :_hour(hour)
//		        ,_minate(minate)
//	 	         ,_second(second)
//	     {}
//				void func(const Date& a)
//				{
//					cout<<a._year<<"/"<<a._month<<"/"<<a._day << endl;
//				}
//		private:
//			int _hour;
//			int _minate;
//			int _second;
//		};
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	
//	Date::Time t;
//	t.func(Date());
//	return 0;
//
//}


//编译器的优化
//class A
//{
//public:
//	A(int a = 0) 
//		:_a(a)
//	{
//		cout << "A(int a)" << endl;
//	}
//
//	A(const A& aa)
//		:_a(aa._a)
//	{
//		cout << "A(const A& aa)" << endl;
//	}
//
//	A& operator=(const A& aa)
//	{
//		cout << "A& operator=(const A& aa)" << endl;
//
//		if (this != &aa)
//		{
//			_a = aa._a;
//		}
//
//		return *this;
//	}
//
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//};
//void func1(A a)
//{
//
//}
//void  func2(const A&a)
//{
//
//}
//
//A func3()
//{
//	A aa;
//	return  aa;
//}
//
//A func4()
//{
//	return A();
//}
//int main()
//{
	//A a1=1;
	//func1(1);
	/*A a1;
	func1(a1);*/
	//func1(A(3));
//	func2(2);
	//A a1;
	//func2(a1);
	//func2(A(3));
	/*func3();
	A a=func3();*/
	/*A a = func4();
	return 0;*/

//}

#include <climits>
#include <iostream>
	using namespace std;

	class Date
	{
	public:
		Date(int a, int b, int c)
			:_a(a)
			, _b(b)
			, _c(c)
			
		{}

		int Getday()
		{
			int n = 0;
			if ((_a % 4 == 0 && _a % 100 != 0) || (_a % 400 == 0))
			{
				n = 1;
			}
			return n + arr[_b - 1] + _c;
		}

	private:
		int _a;
		int _b;
		int _c;
		int arr[13];
	};
	int main()
	{
		int a, b, c;

		while (cin >> a >> b >> c) { // 注意 while 处理多个 case
			Date d(a, b, c);
			arr={}
			cout << d.Getday() << endl;
		}
	}