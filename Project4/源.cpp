#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include<vector>
#include <string>
#include<set>
#include <algorithm>
using namespace std;
//class A { public: A() {} A(int) = delete; };
//int main()
// {
//	 short c = 6; 
//	 short d{ 6 };
//	cout << c << " " << d <<endl;
//	 return 0;
//}


// 单字符重复子串的最大长度
//这个题目还是双指针，首先把所有字符串的字母个数统计一遍，然后从第一个字母开始，找到第一个不相等的字母，然后更新答案，此时是当前连续最长子串
//然后找第二个不相等的字母位置，更新答案
class Solution {
public:
    int maxRepOpt1(string text) {
        int ans = 1;
        vector<int> total(26, 0);
        for (auto e : text) total[e - 'a']++;
        int i = 0, j = 1, p = 0;
        while (i < text.size())
        {
            while (j < text.size() && text[i] == text[j]) j++;
            p = j; j += 1;
            ans = max(ans, p - i); //p本身就是第一个不相等字母的下一位
            while (j < text.size() && text[i] == text[j]) j++;
            ans = max(ans, min(j - i, total[text[i] - 'a'])); //j-i是连续的两段子串的长度，total[text[i] - 'a']是本身该字母有多少个，如果长度比本身有的数目还多，说明无法通过交换一次拼接两段子串，如果j-i比较小说明后面还有更长的拼接串

            i = p;
            j = i + 1;
        }
        return ans;
    }
};


//统计范围内的元音字符串数
class Solution {
public:
    // string s="aeiou";
    bool f(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }
    vector<int> vowelStrings(vector<string>& words, vector<vector<int>>& queries) {
        int sum[words.size() + 1]; //sum[2]表示0-1区间元音字符串个数
        sum[0] = 0;
        for (int i = 0; i < words.size(); i++)
            sum[i + 1] = sum[i] + (f(words[i][0]) && f(words[i][words[i].size() - 1]));
        vector<int> ans;
        for (auto& e : queries) ans.push_back(sum[e[1] + 1] - sum[e[0]]);
        return ans;
        // vector<int> change(words.size(),0);
        // vector<int> ans;
        // for(int i=0;i<words.size();i++)
        // {
        //     if(s.find(words[i][0])!=string::npos && s.find(words[i][words[i].size()-1])!=string::npos) change[i]=1;

        // }
        // for(int i=0;i<queries.size();i++)
        // {
        //     int l=queries[i][0],r=queries[i][1];
        //     int tmp=0;
        //     for(int j=l;j<=r;j++)
        //         tmp+=change[j];
        //     ans.push_back(tmp);
        // }

        // return ans;
    }
};