#pragma once
namespace K
{
	namespace wrt
	{
		template<class K>
		struct node {
			node<K>* _left;
			node<K>* _right;
			K _val;
			node(const K& val)
				:_val(val)
				, _left(nullptr)
				, _right(nullptr)
			{}
		};
		template <class K>

		class BST
		{
			typedef node<K> Node;
		public:
			BST()
				:_root(nullptr)
			{}
			BST(const BST<K>& t)
			{
				_root = Copy(t._root);
			}

			BST<K>& operator=(BST<K> t)
			{
				swap(_root, t._root);
				return *this;
			}

			~BST()
			{
				Destroy(_root);
				_root = nullptr;
			}
			bool insert(K val)
			{
				if (!_root) //空树，直接new然后返回true
				{
					_root = new Node(val);
					return true;
				}
				Node* parent = _root; //父节点
				Node* cur = _root;//遍历指针
				while (cur) //不走到空一直找
				{
					if (val > cur->_val) //要插入的更大，走右树
					{
						parent = cur; //先把孩子给给父亲
						cur = cur->_right; //孩子往后走
					}
					else if (val < cur->_val)//要插入的更小，走左树
					{
						parent = cur; 
						cur = cur->_left;
					}
					else
					{
						//说明存在这个节点，不可以插入
						return false;
					}
				}
				Node* newnode = new Node(val); //找到要插入的位置，new
				if (parent->_val > val) //判断往父节点的左/右插，节点val更大，说明要插入的更小，应该在左孩子
				{
					parent->_left = newnode;
				}
				else
				{
					parent->_right = newnode; //否则在右孩子
				}

				return true; //插入成功
			}
			void InOrder()
			{
				if (!_root) return;
				_InOrder(_root);//调用私有成员函数
				cout << endl; //打印之后换行
			}
			bool Erase(const K& val)
			{
				if (!_root) return false; //空树不删除
				Node* cur = _root; //遍历指针
				Node* parent = _root; //父节点

				while (cur)
				{
					//首先找到要删除的节点，找不到直接false
					if (val > cur->_val) //搜索逻辑
					{
						parent = cur;
						cur = cur->_right;
					}
					else if (val < cur->_val)
					{
						parent = cur;
						cur = cur->_left;
					}
					else
					{
						break;
					}
				}
				if (!cur) 
					return false;//如果循环条件结束是因为每找到节点，直接false
				//左为空
				//右为空
				//都不为空
				if (!cur->_left)
				{
					if (cur == _root)
					{
						_root = _root->_right;
					}
					else
					{
						if (parent->_left == cur)
						{
							parent->_left = cur->_right;
						}
						else
						{
							parent->_right = cur->_right;
						}
					}
					delete cur;
				}

				else if (!cur->_right)
				{
					if (cur == _root)
					{
						_root = _root->_left;
					}
					else
					{
						if (parent->_left == cur)
						{
							parent->_left = cur->_left;
						}
						else
						{
							parent->_right = cur->_left;
						}
					}
					delete cur;
				}
				else
				{
					Node* parent = cur; //保存父节点 
					Node* maxleft = cur->_left;
					while (maxleft->_right)
					{
						parent = maxleft;
						maxleft = maxleft->_right;
					}
					cur->_val = maxleft->_val;
					if (maxleft == parent->_left)
						parent->_left = maxleft->_left;
					else
						parent->_right = maxleft->_left;
					delete maxleft;
				}
					//Node* minRight = cur->_right; //找右树最小
					//while (minRight->_left) //找小往左树走
					//{
					//	//此时的父节点是要删除minRight的父节点
					//	parent = minRight; 
					//	minRight = minRight->_left;
					//}
					//cur->_val = minRight->_val; //找到右树最小，把他和要删除节点值交换
				
					//if (minRight == parent->_left) //如果最小在父亲的左侧
					//{
					//	parent->_left = minRight->_right; 
					//	//父亲左连minRight右，因为minRight的左一定是NULL（他是最小的）如果右节点还有一定要连上
					//}
					//else
					//{
					//	parent->_right = minRight->_right;//父亲右连minRight右
					//}
					//delete minRight;

				//}


				return true;
			}
			bool find(const K& val)
			{
				if (!_root) return false; //空树肯定找不到
				Node* cur = _root; //遍历指针
				while (cur)
				{
					if (cur->_val == val) return true; //找到啦
					else if (val < cur->_val) //搜索思想，不再赘述
					{
						cur = cur->_left;
					}
					else
					{
						cur = cur->_right;
					}
				}
				return false;
			}
			bool InsertR(const K& val)
			{
				return 	_InsertR(_root, val);
			}
			bool FindR(const K& val)
			{
				return _FindR(_root, val);
			}
			bool EraseR(const K& val)
			{
				return _EraseR(_root, val);
			}
			void Delete(const K& val)
			{
				_Delete(_root, val);
			}
			Node* Copy(Node* _root)
			{
				_Copy(_root);
			}
			void Destory(Node* _root)
			{
				if (!_root) return;
				Destory(_root->_left);
				Destory(_root->_right);
				delete  _root;
			}
		private:
			void _Delete(Node* root, const K& val)
			{
				if (!root)
					return;
				_Delete(root->_left, val);
				_Delete(root->_right, val);
				delete root;
			}
			Node* _Copy(Node* root)
			{
				if (!root) return nullptr;

				Node* newnode = new Node(root->_val);
				newnode->_left = _Copy(root->_left);
				newnode->_right = _Copy(root->_right);

				return newnode;
			}
			bool _InsertR(Node*& root, const K& val)
			{
				if (!root) //root是他父节点的某个孩子的别名
				{
					root = new Node(val);//把孩子new，自然和父亲有链接
					return true;
				}
				if (root->_val > val)
					_InsertR(root->_left, val);
				else if (root->_val < val)
					_InsertR(root->_right, val);
				else
					return false;
			}
			bool _EraseR(Node*& root, const K& val)
			{
				if (!root) return false;
				if (root->_val > val) 
				{
					//左
					_EraseR(root->_left, val);
				}
				else if (root->_val < val)
				{
					_EraseR(root->_right, val);
				}
				else //找到要删位置
				{
					Node* del = root; //del是要删的节点
					if (!root->_left) //左为空
						root = root->_right; 
					else if (!root->_right) //右为空
						root = root->_left;
					else //左右都不空
					{
						Node* minRight = root->_right; 
						while (minRight->_left)//向右找最小
						{
							minRight = minRight->_left;
						}
						swap(minRight->_val, root->_val); //交换val
						return _EraseR(root->_right, val); 
					}
	           delete del;
				return true;
				}
			
			}
			bool  _FindR(Node* root, const K& val)
			{
				if (!root) return false;
				if (root->_val > val)
				{
					//左
					_FindR(root->_left, val);
				}
				else if (root->_val < val)
				{
					_FindR(root->_right, val);
				}
				else
				{
					return true;
				}
			}
			void _InOrder(Node* root)
			{
				if (!root) return; //截断条件
				_InOrder(root->_left); //左
				cout << root->_val << " ";//根
				_InOrder(root->_right);//右
			}
			Node* _root;
		};
		void test()
		{
			int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
			BST<int> t;
			for (auto e : a)
			{
				t.insert(e);
			}
			//t.InOrder();

			t.Erase(3);
			t.InOrder();
			/*cout << t.FindR(5) << endl;
			t.InsertR(50);
			t.InOrder();
			t.EraseR(50);
			t.InOrder();*/

		}
	}
}
namespace KV
{
	template<class K,class V>
	struct BTNode
	{
		BTNode<K, V>* _left;
		BTNode<K, V>* _right;
		V _val;
		K _key;
		BTNode(const K& key, const V& val)
			:_key(key)
			,_val(val)
			,_right(nullptr)
			,_left(nullptr)
		{}
	};
	template<class K, class V>

	class Tree
	{
		typedef  BTNode<K, V> Node;
	public:
		bool Insert(const K& key, const V& value)
		{
			if (_root == nullptr)
			{
				_root = new Node(key, value);
				return true;
			}

			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return false;
				}
			}

			cur = new Node(key, value);
			if (parent->_key < key)
			{
				parent->_right = cur;
			}
			else
			{
				parent->_left = cur;
			}

			return true;
		}

		Node* Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					cur = cur->_left;
				}
				else
				{
					return cur;
				}
			}

			return nullptr;
		}

		void Inorder()
		{
			_Inorder(_root);
		}

		void _Inorder(Node* root)
		{
			if (root == nullptr)
				return;

			_Inorder(root->_left);
			cout << root->_key << ":" << root->_val << endl;
			_Inorder(root->_right);
		}
	private:
		Node* _root = nullptr;
	};
	void test()
	{
		string arr[] = { "am", "am", "is", "is", "am", "are", "are", "is", "is ","is", "is" };
		KV::Tree<string, int> countTree;
		for (auto e : arr)
		{
			auto ret = countTree.Find(e);
			if (!ret)
			{
				countTree.Insert(e, 1);
			}
			else
				ret->_val++;
	   }
		countTree.Inorder();
	}
}


