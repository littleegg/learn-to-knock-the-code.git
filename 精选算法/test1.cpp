//#define _CRT_SECURE_NO_WARNINGS
//#include "标头.h"
//
////旋转数组的最小数字
////这道题采用 二分查找的思想 一定要利用两个递增数组拼起来的性质 找到最小的数字
////同时注意对于细节还有边界条件的各种把握
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @return int整型
//     */
//    int minNumberInRotateArray(vector<int>& nums) {
//        if (nums.empty()) return 0;
//        // write code here
//        int mid = 0;
//        int l = 0, r = nums.size() - 1;
//        while (nums[l] >= nums[r]) {
//            if (r - l == 1) {
//                mid = r;
//                break;
//            }
//            mid = l + ((r - l) >> 1);
//            if (nums[mid] == nums[l] && nums[l] == nums[r]) {
//                //此时无法判断mid的具体位置 所以只能选择线性遍历
//                int ans = nums[l];
//                for (int i = l + 1; i < r; i++) {
//                    if (nums[i] < ans) ans = nums[i];
//                }
//                return ans;
//            }
//            if (nums[mid] >= nums[l]) l = mid;
//            else r = mid;
//        }
//        return nums[mid];
//    }
//};
//
//
////二维数组中的查找
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param target int整型
//     * @param array int整型vector<vector<>>
//     * @return bool布尔型
//     */
//    bool Find(int target, vector<vector<int> >& array) {
//        // write code here
//        int i = 0, j = array[0].size() - 1;
//        while (i < array.size() && j >= 0) {
//            if (target < array[i][j]) j--;
//            else if (target > array[i][j]) i++;
//            else return true;
//        }
//        return false;
//    }
//};
//
//
////替换空格
////相当于直接在原数组上覆盖改写 
////先找好要开的空间
//class Solution {
//public:
//    void replaceSpace(char* str, int length) {
//        int count = 0;
//        for (int i = 0; i < length; i++) if (str[i] == ' ') count++;
//        int l = length + count * 2;
//        for (int i = length - 1; i >= 0 && i != l; i--) {
//            if (str[i] == ' ')
//            {
//                str[--l] = '0';
//                str[--l] = '2';
//                str[--l] = '%';
//            }
//            else str[--l] = str[i];
//        }
//    }
//};
//
//
////数组中出现次数超过一半的数字
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param numbers int整型vector
//     * @return int整型
//     */
//    int MoreThanHalfNum_Solution(vector<int>& numbers) {
//        // write code here
//        //当然可以直接用unordered_map记录每个数字出现的次数
//        //但是这里采用打擂台的思路 
//        //打擂台：士兵分别穿上五颜六色的衣服 首先随便选取一个颜色上台 接着再来一个士兵打擂 如果相同颜色 那么擂主的计数器++ 否则-- 当计数器减到0 说明这个颜色的士兵没人了 那么换一个其他颜色的守擂
//        //最后留在擂台上的就是出现颜色最多的士兵
//        if (numbers.empty()) return 0;
//        int key = numbers[0];
//        int count = 0;
//        for (int i = 1; i < numbers.size(); i++) {
//            if (count == 0) { key = numbers[i]; count = 1; }
//            if (numbers[i] == key) count++;
//            else if (numbers[i] != key) count--;
//        }
//        int time = 0;
//        for (auto& e : numbers) {
//            if (key == e) time++;
//        }
//        return time >= numbers.size() / 2 ? key : 0;
//    }
//};
//
////从尾到头打印链表
///**
//*  struct ListNode {
//*        int val;
//*        struct ListNode *next;
//*        ListNode(int x) :
//*              val(x), next(NULL) {
//*        }
//*  };
//*/
//class Solution {
//public:
//    void dfs(ListNode* head, vector<int>& v) {
//        if (!head) return;
//        dfs(head->next, v);
//        v.push_back(head->val);
//    }
//    vector<int> printListFromTailToHead(ListNode* head) {
//        vector<int> v;
//        dfs(head, v);
//        return v;
//    }
//};
//
//
////重建二叉树
///**
// * struct TreeNode {
// *	int val;
// *	struct TreeNode *left;
// *	struct TreeNode *right;
// *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param preOrder int整型vector
//     * @param vinOrder int整型vector
//     * @return TreeNode类
//     */
//    TreeNode* build(vector<int>& pre, int prestart, int preend, vector<int>& vin, int vinstart, int vinend) {
//        if (prestart > preend || vinstart > vinend) return nullptr;;
//        TreeNode* root = new TreeNode(pre[prestart]);
//        for (int i = vinstart; i <= vinend; i++) {
//            if (vin[i] == root->val) {
//                //找到了
//                root->left = build(pre, prestart + 1, prestart + i - vinstart, vin, vinstart, i - 1);
//                root->right = build(pre, prestart + i - vinstart + 1, preend, vin, i + 1, vinend);
//                break;
//            }
//        }
//        return root;
//    }
//    TreeNode* reConstructBinaryTree(vector<int>& preOrder, vector<int>& vinOrder) {
//        // write code here
//        if (preOrder.empty() || vinOrder.empty() || preOrder.size() != vinOrder.size()) return nullptr;
//        return build(preOrder, 0, preOrder.size() - 1, vinOrder, 0, vinOrder.size() - 1);
//    }
//};
//
////斐波那契的unordered_map写法
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @return int整型
//     */
//    unordered_map<int, int> m;
//
//    int Fibonacci(int n) {
//        // write code here
//        int pre = 0;
//        int ppre = 0;
//        if (n == 1 || n == 0) {
//            return n;
//        }
//        if (n == 2) return 1;
//        if (m.find(n - 1) == m.end()) {
//            pre = Fibonacci(n - 1);
//            m.insert({ n - 1,pre });
//        }
//        else {
//            pre = m[n - 1];
//        }
//        if (m.find(n - 2) == m.end()) {
//            ppre = Fibonacci(n - 2);
//            m.insert({ n - 2,ppre });
//        }
//        else {
//            ppre = m[n - 2];
//        }
//        return pre + ppre;
//    }
//};
//
//
//
////矩形覆盖
//class Solution {
//public:
//    int rectCover(int number) {
//        vector<int> dp(number + 1);
//        dp[1] = 1; dp[2] = 2;
//        for (int i = 3; i < number + 1; i++) {
//            dp[i] = dp[i - 1] + dp[i - 2];
//        }
//        return dp[number];
//    }
//};
//
//
////计算二进制中1的个数
////不要再每一个位都&1 
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @return int整型
//     */
//    int NumberOf1(int n) {
//        // write code here
//        int ans = 0;
//        while (n)
//        {
//            n &= (n - 1);
//            ans++;
//        }
//        return ans;
//    }
//};
//
////调整数组顺序 奇数在前并且不改变原数组的奇偶顺序
//class Solution {
//public:
//    void reOrderArray(vector<int>& array) {
//        int k = 0;
//        int tmp = 0;
//        for (int i = 0; i < array.size(); i++) {
//            if (array[i] & 1) {
//                tmp = array[i];
//                int j = i;
//                while (j > k) {
//                    array[j] = array[j - 1];
//                    j--;
//                }
//                array[k++] = tmp;
//            }
//        }
//    }
//};
//
//
////链表无难题 返回链表的倒数第k和节点
///*
//struct ListNode {
//    int val;
//    struct ListNode *next;
//    ListNode(int x) :
//            val(x), next(NULL) {
//    }
//};*/
//class Solution {
//public:
//    ListNode* FindKthToTail(ListNode* pListHead, unsigned int k) {
//        if (pListHead == nullptr) return nullptr;
//        ListNode* f = pListHead;
//        ListNode* s = pListHead;
//        while (k > 0 && f) {
//            f = f->next;
//            k--;
//        }
//        while (f) {
//            s = s->next;
//            f = f->next;
//        }
//        return k > 0 ? nullptr : s;
//    }
//};
//
////反转链表——三指针
///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* ReverseList(ListNode* head) {
//        // write code here
//        ListNode* pre = nullptr;
//        ListNode* cur = head;
//        ListNode* tmp = nullptr;
//        while (cur) {
//            tmp = cur->next;
//            cur->next = pre;
//            pre = cur;
//            cur = tmp;
//        }
//        return pre;
//    }
//};
//
////反转链表——头插
///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* ReverseList(ListNode* head) {
//        // write code here
//        ListNode* tmp = nullptr;
//        ListNode* cur = head;
//        ListNode* tail = nullptr;
//        while (cur) {
//            tmp = cur->next;
//            cur->next = tail;
//            tail = cur;
//            cur = tmp;
//        }
//        return tail;
//    }
//};
//
////合并两个链表——普通方法
///*
//struct ListNode {
//    int val;
//    struct ListNode *next;
//    ListNode(int x) :
//            val(x), next(NULL) {
//    }
//};*/
//class Solution {
//public:
//    ListNode* Merge(ListNode* pHead1, ListNode* pHead2) {
//        ListNode* cur1 = pHead1;
//        ListNode* cur2 = pHead2;
//        ListNode* newhead = new ListNode(-1);
//        ListNode* cur = newhead;
//        while (cur1 && cur2)
//        {
//            ListNode* tmp1 = cur1->next;
//            ListNode* tmp2 = cur2->next;
//            if (cur1->val < cur2->val)
//            {
//                cur->next = cur1;
//                cur1 = tmp1;
//            }
//            else
//            {
//                cur->next = cur2;
//                cur2 = tmp2;
//            }
//            cur = cur->next;
//        }
//        while (cur1)
//        {
//            cur->next = cur1;
//            cur = cur->next;
//            cur1 = cur1->next;
//        }
//        while (cur2)
//        {
//            cur->next = cur2;
//            cur = cur->next;
//            cur2 = cur2->next;
//        }
//        return newhead->next;
//    }
//};
//
//
////合并两个链表——递归
///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param pHead1 ListNode类
//     * @param pHead2 ListNode类
//     * @return ListNode类
//     */
//    ListNode* Merge(ListNode* pHead1, ListNode* pHead2) {
//        // write code here
//        if (pHead1 == nullptr) {
//            return pHead2;
//        }
//        if (pHead2 == nullptr) {
//            return pHead1;
//        }
//        ListNode* newNode = nullptr;
//        if (pHead1->val < pHead2->val) {
//            newNode = pHead1;
//            pHead1 = pHead1->next;
//        }
//        else {
//            newNode = pHead2;
//            pHead2 = pHead2->next;
//        }
//        newNode->next = Merge(pHead1, pHead2);
//        return newNode;
//    }
//};
//
//
////数的子结构
///*
//struct TreeNode {
//    int val;
//    struct TreeNode *left;
//    struct TreeNode *right;
//    TreeNode(int x) :
//            val(x), left(NULL), right(NULL) {
//    }
//};*/
//class Solution {
//public:
//    bool issame(TreeNode* a, TreeNode* b) {
//        if (b == nullptr) return true;
//        if (a == nullptr) return false;
//        if (a->val != b->val) return false;
//        return issame(a->left, b->left) && issame(a->right, b->right);
//    }
//    bool HasSubtree(TreeNode* pRoot1, TreeNode* pRoot2) {
//        if (!pRoot1 || !pRoot2) return false;
//        bool ans = false;
//        if (pRoot1->val == pRoot2->val) {
//            ans = issame(pRoot1, pRoot2);
//        }
//        if (ans == false) {
//            ans = HasSubtree(pRoot1->left, pRoot2);
//        }
//        if (ans == false) {
//            ans = HasSubtree(pRoot1->right, pRoot2);
//        }
//        return ans;
//    }
//};
//
