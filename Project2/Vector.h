#pragma once

namespace wrt
{
	template <typename  T>

	class vector
	{
	public:
		typedef  T* iterator;
		typedef  const T* const_iterator;

		iterator begin()
		{
			return _start;
		}
		iterator end()
		{
			return _finish;
		}
		const_iterator begin() const
		{
			return _start;
		}

		const_iterator end() const
		{
			return _finish;
		}

		T& operator[](size_t pos)
		{
			assert(pos < size());
			return _start[pos];
		}
		const T& operator[](size_t pos) const
		{
			assert(pos < size());
			return _start[pos];
		}
		vector()
			:_start(nullptr)
			, _finish(nullptr)
			, _endstorage(nullptr)
		{}
		vector(int n, const T& val = T())
			:_start(nullptr)
			, _finish(nullptr)
			, _endstorage(nullptr)
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}
		void push_back(const  T& x)
		{
			if (_finish==_endstorage)
			{
				//需要扩容
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
			}
			*_finish = x;
			_finish++;
		}
		~vector()
		{
			delete[] _start;
			_start = nullptr;
			_finish = nullptr;
			_endstorage = nullptr;
		}
		void resize(size_t n, T& val = T())
		{
			if (n > capacity())
			{
				reserve(n);
			}
			if (n > size())
			{
				while ((n - size() + 1)--)
				{
					*_finish = val;
					_finish++;
				}
			}
			else
			{
				_finish = _start + n;
			}
		}
		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t oldsize = size();
				T* tmp = new T[n];
				if (_start)
				{
					//memcpy(tmp, _start, sizeof(int) * oldsize); //这个地方有问题，自定义类型的浅拷贝
					for (size_t  i = 0; i < oldsize; i++)
					{
						tmp[i]=_start[i];
					}
					delete[] _start;
				}
				_start = tmp;
				_finish = _start + oldsize;
				_endstorage = _start + n;
			}
			//如果n更小不需要扩容
		}
		size_t  capacity()
		{
			return _endstorage - _start;
		}
		bool empty()
		{
			return _finish == _start;
		}
		void pop_back()
		{
			assert(!empty());
				_finish--;
		}
		iterator  insert(iterator pos, const T& val)
		{
			assert(pos >= _start);
			assert(pos < _finish);

			if (_finish == _endstorage)
			{
				size_t len = pos - _start;
				size_t newCapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newCapacity);

				// 扩容会导致pos迭代器失效，需要更新处理一下
				pos = _start + len;
			}

			// 挪动数据
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				--end;
			}

			*pos = val;
			++_finish;

			return pos;
		}
		/*iterator erase(iterator pos)
		{
			assert(pos >= _start);
			assert(pos < _finish);


			iterator begin = pos + 1;
			while (begin < _finish)
			{
				*(begin - 1) = *(begin);
				++begin;
			}

			--_finish;

			return pos;
		}*/
		void erase(iterator pos)
		{
			assert(pos >= _start);
			assert(pos < _finish);


			iterator begin = pos + 1;
			while (begin < _finish)
			{
				*(begin - 1) = *(begin);
				++begin;
			}

			--_finish;
		}
		void clear()
		{
			_finish = _start;
		}
		
		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endstorage, v._endstorage);
		}
		size_t  size() const 
		{
			return _finish - _start;
		}
		//拷贝构造的几种写法
		//v1(v2)
		//vector(vector<T>& v)  //传统写法
		//{
		//	_start = new T[v.capacity()];  //按照v的大小开空间
		//	for (size_t i = 0; i < v.size(); i++)
		//	{
		//		_start[i] = v._start[i];  //一个一个拷贝
		//	}
		//	_finish = _start+v.size();
		//	_endstorage =_start+v.capacity();
		//}
		//另一种传统方法
		//vector(vector<T>& v)
		//	:_start(nullptr)
		//	, _finish(nullptr)
		//	, _endstorage(nullptr)
		//{
		//	reserve(v.capacity());
		//	for (auto& e : v)  //一定要加上&,不知道v里面元素类型，如果是自定义类型需要深拷贝
		//	{
		//		push_back(e);
		//	}
		//}
		//很新的方法
		template <typename Inputiterator>
		vector(Inputiterator first, Inputiterator last)
			:_start(nullptr)
			, _finish(nullptr)
			, _endstorage(nullptr)
		{
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}
		vector(const vector<T>& v)
			:_start(nullptr)
			, _finish(nullptr)
			, _endstorage(nullptr)
		{
			vector<T> tmp(v.begin(), v.end());
			swap(tmp); //this 和 tmp 交换
		}
		vector<T>& operator=(vector <T>v)
		{
			swap(v);
			return *this;
		}
	private:
		iterator _start;
		iterator _finish;
		iterator _endstorage;
	};
	void test()
	{
		/*vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);
		v1.push_back(6);
		v1.push_back(7);

		

		vector<int>::iterator it = find(v1.begin(), v1.end(),3);
		if (it != v1.end())
			v1.erase(it);
		*it++;*/
//	vector<int>::iterator it =v1.begin();

		//删除所有偶数
		/*while(it !=v1.end())
		{
			if (*it % 2 == 0)
				v1.erase(it);
			++it;
		}
		for (size_t  i = 0; i < v1.size(); i++)
		{
			cout << v1[i] <<" ";
		}
		cout << endl;*/

	//	vector<vector<int>> vv(5);
		//vector<int > v(5); 
		/*for (size_t i = 0; i < v.size(); i++)
		{
			cout << v[i] << " ";
		}
		cout << endl;*/
		/*vv.push_back(v);
		vv.push_back(v);
		vv.push_back(v);
		vv.push_back(v);
		vv.push_back(v);*/
	/*for (size_t  i = 0; i < vv.size(); i++)
		{
			for (size_t j = 0; j < vv[i].size(); j++)
			{
				cout << vv[i][j] << " ";
			}
			cout << endl;
	    }*/

		

		//for (size_t i = 0; i < v1.size(); i++)
		//{
		//	for(size_t j=0;j< v1[i].size();j++)
		//	cout << v1[i][j] << " ";
		//	cout << endl;
		//}
		//cout << endl;


	vector <int> v;
		v.push_back(1);
		v.push_back(1);
		v.push_back(1);
		v.push_back(1);
		cout << v.size() << endl;
		cout << v.capacity() << endl;
		vector <int> v2;
		v = v;
		for (size_t i = 0; i < v.size(); i++)
		{
			cout << v[i] << " ";
		}
		cout <<endl;
		cout << v.size() << endl;
		cout << v.capacity() << endl;

		cout << endl;
	}
}
