#pragma once
template<size_t N>
class bitset {
public:
	bitset(){
		_bits.resize(N / 8+1, 0);
	}
	void set(size_t x) {
		//设为1
		size_t i = x / 8;//计算x映射的位在第i个char数组的位置
		size_t j = x % 8; //计算x映射的位在这个char的第一个比特位
		//左移就是从低往高挪 表示的高低方向不是左右方向
		_bits[i] |= (1<<j);
	}
	void reset(size_t x) {
		//设为0
		size_t i = x / 8;//计算x映射的位在第i个char数组的位置
		size_t j = x % 8; //计算x映射的位在这个char的第一个比特位
		_bits[i] &= ~(1 << j);

	}
	bool test(size_t x) {
		size_t i = x / 8;
		size_t j = x % 8;
		return _bits[i] & (1 << j);
	}
private:
	vector<char> _bits;
};
void test_bitset() {
	bitset<100> bs;
	bs.set(10);
}


//设计算法找到只出现一次的整数
//可以设计两位位图 00：没出现过 01：只出现一次 10：出现一次以上

template<size_t N>
class twobitset {
public:
	void set(size_t x) {
		//00->01
		if (_bs1.test(x) == false && _bs2.test(x) == false)
			_bs2.set(x);
		//01->10
		else if (_bs1.test(x) == false && _bs2.test(x) == true)
		{
			_bs1.set(x);
			_bs2.reset(x);
		}
		//10 不变
	}

	//只要第二个位是1 就是只出现一次的
	void Print() {
		for (size_t  i = 0; i < N; i++)
		{
			if (_bs2.test(i)) cout << i << endl;
		}
	}
public:
	bitset < N > _bs1;
	bitset < N > _bs2;

};
//给两个文件 找到两个文件的交集
//可以复用第一个题目的思路 直接两个位图 然后&

//布隆过滤器
//想把其他类型的数据也转成整形这时候需要hashfunc
//多选择几个函数去映射 布隆是允许误判的

struct BKDRHash
{
	size_t operator()(const string& s)
	{
		size_t hash = 0;
		for (auto ch : s)
		{
			hash += ch;
			hash *= 31;
		}

		return hash;
	}
};

struct APHash
{
	size_t operator()(const string& s)
	{
		size_t hash = 0;
		for (long i = 0; i < s.size(); i++)
		{
			size_t ch = s[i];
			if ((i & 1) == 0)
			{
				hash ^= ((hash << 7) ^ ch ^ (hash >> 3));
			}
			else
			{
				hash ^= (~((hash << 11) ^ ch ^ (hash >> 5)));
			}
		}
		return hash;
	}
};
struct DJBHash
{
	size_t operator()(const string& s)
	{
		size_t hash = 5381;
		for (auto ch : s)
		{
			hash += (hash << 5) + ch;
		}
		return hash;
	}
};

// N最多会插入key数据的个数
template<size_t N,
	class K = string,
	class Hash1 = BKDRHash,
	class Hash2 = APHash,
	class Hash3 = DJBHash>
class BloomFilter
{
public:
	void set(const K& k) {
		size_t len = N * _X;
		size_t hash1 = Hash1()(k) % len;
		_bs.set(hash1);
		size_t hash2 = Hash2()(k) % len;
		_bs.set(hash2); 
		size_t hash3 = Hash3()(k) % len;
		_bs.set(hash3);
	}
	bool test(const K& k) {
		size_t len = N * _X;
		size_t hash1 = Hash1()(k) % len;
		size_t hash2 = Hash2()(k) % len;
		size_t hash3 = Hash3()(k) % len;
		if (_bs.test(hash1) && _bs.test(hash2) && _bs.test(hash3)) return true;
		//有一个不在 就是不在
		return false;

		//在-》可能误判 不准确的
		//不在是准确的————————重点！！！！！！！！！！！！！！！！！！！！！！！ 
	}
private:
	static const size_t _X = 4;
	bitset<N * _X> _bs;
};