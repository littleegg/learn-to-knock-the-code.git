#include <iostream>
#include <mutex>
#include <thread>
#include <condition_variable>
#include "semaphore.hpp"

using namespace std;

condition_variable cond1, cond2;
condition_variable cond;

mutex mut;
int g_nums = 1;
static const int num = 100;
semaphore smp1(0),smp2(0);


////双条件变量版本
//void thread1() {
//	while (true) {
//		unique_lock<mutex> locker(mut);  //会自动解锁
//		if (g_nums % 2 == 1 && g_nums <= num)
//		{
//			cout << "Thread1:" << g_nums << endl;
//			g_nums++;
//		}
//		cond2.notify_one();
//		cond1.wait(locker);
//		if (g_nums >= num+1)
//				break;
//	}
//	cout << "1 done"<< endl;
//	cond2.notify_one();
//}
// void thread2() {
//	while (true) {
//		unique_lock<mutex> locker(mut);
//		if (g_nums % 2 == 0 && g_nums <= num)
//		{
//			cout << "Thread2:" << g_nums << endl;
//			g_nums++;
//		}
//		cond1.notify_one();
//		cond2.wait(locker);
//		if (g_nums >= num + 1)
//			break;
//	}
//	cout << "2 done" << endl;
//	cond1.notify_one();
//}



////单条件变量版本
//void thread1()
//{
//	while (true)
//	{
//		unique_lock<mutex> lock(mut);
//		if (g_nums % 2 == 1 && g_nums <= num)
//		{
//			cout << "thread 1:" << g_nums ++<< endl;
//			
//		}
//	       cond.notify_one();  //唤醒另一个进程
//			cond.wait(lock); //把当前的进程锁住
//			if (g_nums >= num+1)
//				break;
//	}
//	cout << "1 done"<< endl;
//	cond.notify_one();
//}
//void thread2() {
//	while (1) {
//		unique_lock<mutex> locker(mut);
//		if (g_nums % 2 == 0 && g_nums <= num)
//		{
//			cout << "Thread2:" << g_nums << endl;
//			g_nums++;
//		}
//		cond.notify_one();
//		cond.wait(locker);
//      if (g_nums >= num+1)
//				break;
//	}
//	cout << "2 done" << endl;
//	cond.notify_one();
//}





 //信号量版本
void thread1()
{
	while (true)
	{
		if (g_nums % 2 == 1 && g_nums <= num)
		{
			cout << "thread 1:" << g_nums++ << endl;
			smp1.single();
		}
		smp2.wait();
		//cond.notify_one();  //唤醒另一个进程
		//cond.wait(lock); //把当前的进程锁住
		if (g_nums >= num + 1)
			break;
	}
	cout << "1 done" << endl;
	smp1.single();
}


void thread2() {
	while (true) {
		if (g_nums % 2 == 0 && g_nums <= num)
		{
			cout << "Thread2:" << g_nums ++<< endl;
			smp2.single();
		}
		smp1.wait();
			if (g_nums >= num+1)
				break;
	}
	cout << "2 done" << endl;
	smp2.single();
}


int main() 
{
	thread t1(thread1);
	thread t2(thread2);
	t1.join();
	t2.join();
	cout << "done" << endl;
	return 0;
}
