#pragma once

#include <iostream>
#include <mutex>
#include <condition_variable>

using namespace std;

class semaphore {
public:
	semaphore(int num=0)
		:count(num)
	{
	}
	~semaphore()
	{}
	void single() //V
	{
		unique_lock<mutex> lock(mtx);
		if (++count <= 0)
			cond.notify_one();
	}
	void wait() //p
	{
		unique_lock<mutex> lock(mtx);
		if (--count < 0)
			cond.wait(lock);
	}


private:
	int count;
	mutex mtx;
	condition_variable cond;

};