#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <random>
#include <stdlib.h>
using namespace std;


typedef struct Position {
    int row;
    int col;
}Pos; //位置参数由两个变量控制，定义一个结构体存储变量
typedef Pos STDataType;  //栈元素类型是位置参数
typedef struct Stack
{
    STDataType* a;
    int top;
    int capacity;
}ST;
ST Path; //定义一个全局变量好写

/// /////////////////////////////////////////////
/// 常规栈的操作 增删查改

void StackInit(ST* head)
{
    head->a = NULL;
    head->capacity = 0;
    head->top = 0;
}
bool StackEmpty(ST* head)
{
    return head->top == 0;
}
void StackPush(ST* head, STDataType x)
{
    if (head->top == head->capacity)
    {
        int newcapacity = head->top == 0 ? 4 : 2 * head->capacity;
        STDataType* tmp = (STDataType*)realloc(head->a, sizeof(STDataType) * newcapacity);
        head->a = tmp;
        head->capacity = newcapacity;
    }
    head->a[head->top] = x;
    head->top++;
}
void StackPop(ST* head)
{
    head->top--;
}
STDataType StackTop(ST* head)
{
    return (head->a)[head->top - 1];
}
void StackDestory(ST* head)
{
    free(head->a);
    head->top = 0; head->capacity = 0;
}
//栈基操结束
/// /////////////////////////////////////////////

//判断当前位置可不可以走
bool istruepass(int** maze, int N, int M, Pos cur)
{
    if (cur.col >= 0 && cur.col < M && cur.row >= 0 && cur.row < N && maze[cur.row][cur.col] == 0)
        return true;
    else
        return false;
}

//开始走路
bool Getmazepath(int** maze, int N, int M, Pos cur)
{
    StackPush(&Path, cur);  //当前位置入栈
    if (cur.row == N - 1 && cur.col == M - 1)  //结束递归的条件
        return true;
    maze[cur.row][cur.col] = 2; //走过的位置设置成另一个数字便于区分，不走回头路
    Pos next; //下一次该走的位置

    //上
    next = cur;
    next.row -= 1;
    if (istruepass(maze, N, M, next))
    {
        if (Getmazepath(maze, N, M, next))
            return true;
    }
    //下
    next = cur;
    next.row += 1;
    if (istruepass(maze, N, M, next))
    {
        if (Getmazepath(maze, N, M, next))
            return true;
    }
    //左
    next = cur;
    next.col -= 1;
    if (istruepass(maze, N, M, next))
    {
        if (Getmazepath(maze, N, M, next))
            return true;
    }
    //右
    next = cur;
    next.col += 1;
    if (istruepass(maze, N, M, next))
    {
        if (Getmazepath(maze, N, M, next))
            return true;
    }
    //走到这里就是上下左右都走不通
    StackPop(&Path);
    return false;
}
void PrintPath(ST* path)
{
    //最后的工作，把栈里位置全部打印，需要新栈，因为path栈是栈底入口，栈顶出口，只能先出栈顶的出口
    //建立新栈，把path站内元素取顶压栈，再不断打印新栈顶元素
    ST t;
    StackInit(&t);
    while (!StackEmpty(path))
    {
        StackPush(&t, StackTop(path));
        StackPop(path);
    }
    while (!StackEmpty(&t))
    {
        Pos pos = StackTop(&t);
        StackPop(&t);
        printf("(%d,%d)\n", pos.row, pos.col);
    }
    StackDestory(&t);
}
int main() {
    int N, M;
    while (cin >> N >> M) { // 注意 while 处理多个 case
        int** maze = (int**)malloc(sizeof(int*) * N);
        //二维数组动态开辟
        for (int i = 0; i < N; i++)
        {
            maze[i] = (int*)malloc(sizeof(int) * M);
        }
        //输入迷宫
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                cin >> maze[i][j];
            }
        }
        StackInit(&Path);
        Pos entry = { 0,0 };//入口
        if (Getmazepath(maze, N, M, entry))
            PrintPath(&Path);
        else printf("The maze Path is Not\n");
        StackDestory(&Path);
        //释放栈
        for (int i = 0; i < N; i++)
            free(maze[i]);
        free(maze);
        return 0;
    }
}