#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
#include <assert.h>
using namespace std;

#include"标头.h"

void test_vector1()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	for (size_t i = 0; i < v.size(); ++i)
	{
		cout << v[i] << " ";
	}
	cout << endl;

	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	cout << v.max_size() << endl;

	v.pop_back();
	v.pop_back();
	v.pop_back();
	v.pop_back();
	v.pop_back();

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector2()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	cout << v.capacity() << endl;

	v.reserve(10);
	cout << v.capacity() << endl;

	// 比当前容量小时，不缩容
	v.reserve(4);
	cout << v.capacity() << endl;

	v.resize(8);
	v.resize(15, 1);
	v.resize(3);
}

void TestVectorExpand()
{
	size_t sz;
	vector<int> v;
	v.reserve(100);
	sz = v.capacity();
	cout << "making v grow:\n";
	for (int i = 0; i < 100; ++i)
	{
		v.push_back(i);
		if (sz != v.capacity())
		{
			sz = v.capacity();
			cout << "capacity changed: " << sz << '\n';
		}
	}
}

void Func(const vector<int>& v)
{
	v.size();
	v[0];
	//v[0]++;
	v.at(0);
}

void test_vector3()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.size();
	v[0];
	v[0]++;
	v.at(0);

	Func(v);
}

void test_vector4()
{
	vector<int> v;
	//v.reserve(10);
	v.resize(10);
	for (size_t i = 0; i < 10; ++i)
	{
		v[i] = i;       // assert 
		//v.at(i) = i;  // 抛异常
		//v.push_back(i);
	}
}

void test_vector5()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	v.assign(10, 1);
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	vector<int> v1;
	v1.push_back(10);
	v1.push_back(20);
	v1.push_back(30);

	v.assign(v1.begin(), v1.end());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	string str("hello world");
	v.assign(str.begin(), str.end());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	v.assign(++str.begin(), --str.end());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector6()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);

	v.insert(v.begin(), 4);
	v.insert(v.begin() + 2, 4);

	// 没有find成员
	//vector<int>::iterator it = v.find(3);

	vector<int>::iterator it = find(v.begin(), v.end(), 3);
	if (it != v.end())
	{
		v.insert(it, 30);
	}

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	vector<int> v1;
	v1.push_back(10);
	v1.push_back(20);
	v1.push_back(30);

	v1.swap(v);
	swap(v1, v);
}

void test_vector7()
{
	vector<int> v;
	v.reserve(10);
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	cout << v.size() << endl;
	cout << v.capacity() << endl;

	// 设计理念：不动空间，不去缩容   空间换时间
	v.reserve(5);
	cout << v.size() << endl;
	cout << v.capacity() << endl;

	v.resize(3);
	cout << v.size() << endl;
	cout << v.capacity() << endl;

	v.clear();
	cout << v.size() << endl;
	cout << v.capacity() << endl;

	// 设计理念：时间换空间，一般缩容都是异地，代价不小，一般不要轻易使用
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.shrink_to_fit();
	cout << v.size() << endl;
	cout << v.capacity() << endl;
}

int main()
{
	try
	{
		bit::test_vector9();
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}

	//TestVectorExpand();

	return 0;
}