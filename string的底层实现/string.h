#pragma once
#include <assert.h>
#include <iostream>

namespace wrt
{
	typedef  char* iterator;
	typedef const char* const_iterator;
	
	class string
	{
	private:
		friend ostream& operator<<(ostream& _cout, const wrt::string& s);

		friend istream& operator>>(istream& _cin, wrt::string& s);
	public:
		iterator begin()
	{
		return _str;
	}
		iterator end()
		{
			return _str + _size;
		}
		const_iterator begin() const 
		{
			return _str;
		}
		const_iterator end() const
		{
			return _str + _size;
		}
		string(const char* str = "")
			:_size(strlen(str))
		{
			_capacity = _size==0?3:_size;
			_str = new char[1+_capacity];
			strcpy(_str, str);
		}
		~string()
		{
			if (_str != nullptr)
			{
				delete[] _str;
				_str = nullptr;
			}
			_size = 0;
			_capacity = 0;
		}
		size_t size(const string str)
		{
			return str._size;
		}
		void reserve(size_t n)
		{
			if (n <= _size)
				return;
			else
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}
		void resize(size_t n, char c)
		{
			if (n < _size)
			{
				char* tmp = new char[n + 1];
				strncpy(tmp, _str, n);
				tmp[n] = '\0';
				delete[] _str;
				_str = tmp;
			}
			else
			{
				if (_capacity < n)
				{
					reserve(n);
				}
				memset(_str, c, n - _size);
			}
		}
		const char* c_str()
		{
			return _str;
		}
		char& operator[](size_t pos)
		{
			return _str[pos];
		}

		bool operator<(const string&str ) const
		{
			return strcmp(_str, str._str)<0;

		}
		bool operator=(const string& str) const
		{
			return strcmp(_str, str._str) == 0;
		}
		bool operator>(const string& str) const
		{
			return strcmp(_str, str._str) >0;

		}
		bool operator<=(const string& str) const
		{
			return !(*this >str);
		}
		void push_back(char c)
		{
			if (_size + 1 > _capacity)
			{
				//扩容
				size_t new_capacity = _capacity == 0 ? 2 : _capacity * 2;
				reserve(new_capacity);
			}
			_str[_size] = c;
			_size++;
			_str[_size] = '\0';
		}
		void append(const char* s)
		{
			size_t len = strlen(s);
			if (_size + len > _capacity)
			{
				reserve(_size+len);
			}
			strcpy(_str + _size, s);
			_size += len;
		}

		string& operator+=(const char * s)
		{
			append(s);
			return *this;
		}

		string& operator+=(char c)
		{
			push_back(c);
			return *this;
		}

		void push_front(char c )
		{
			if (_size + 1 > _capacity)
			{
				size_t new_capacity = _capacity == 0 ?  2 : _capacity * 2;
				reserve(new_capacity);
			}
			_size += 1;
			_str[_size] = '\0';
			for (int i=_size-2;i>=0;i--)
			{
				_str[i+1] = _str[i];
			}
			_str[0] = c;
		
		}
		void insert(size_t pos, char ch)
		{
			assert(pos<0 || pos>_size);

			if (_size + 1 > _capacity)
			{
				size_t new_capacity = _capacity == 0 ? 2 : _capacity * 2;
				reserve(new_capacity);
			}
			if (pos == 0)
			{
				push_front(ch);
			}
			else if (pos == _size)
			{
				push_back(ch);
			}
			else
			{
            size_t end = _size;
			while (end > pos)
			{
				_str[end - 1] = _str[end];
				end--;
			}
			_str[pos] = ch;
			_size++;
			}
			
		}
		void front_append(const char* s)
		{
			size_t len = strlen(s);
			if (_size + len > _capacity)
			{
				size_t new_capacity = _capacity == 0 ? len + 1 : _capacity * 2;
				reserve(new_capacity);
			}
			size_t tmp = _size + len;
			while (tmp )
			{
				_str[tmp] = _str[tmp - len];
				tmp--;
			}
			int i = 0;
			int ret = len;
			while (len--)
			{
				_str[i++] = s[i];
			}
			_size = _size + ret ;
			_str[_size+1] = '\0';
		}
		void back_append(const char* s)
		{
			size_t len = strlen(s);
			if (_size + len > _capacity)
			{
				size_t new_capacity = _capacity == 0 ? len + 1 : _capacity * 2;
				reserve(new_capacity);
			}
			strcpy(_str + _size, s);
			_size += len;
			_str[_size] = '\0';
		}
		void insert(size_t pos, const char* s)
		{
			assert(pos>=0 &&  pos<=_size);

			size_t len = strlen(s);
			if (_size + len > _capacity)
			{
				size_t new_capacity = _capacity == 0 ? len + 1 : _capacity * 2;
				reserve(new_capacity);
			}
			if (pos == 0)
				front_append(s);
			else if (pos == _size)
				back_append(s);
			else
			{
				size_t tmp = _size+len;
				while (tmp > pos)
				{
					_str[tmp] = _str[tmp - len];
					tmp--;
				}
				strcpy(_str + pos, s);
				_size += len;
				_str[_size] = '\0';
			}
		}
		void erase(size_t pos, size_t len =-1) //从pos开始的n个字符
		{
			if (pos >= 0 && pos < _size)
			{
				if (len > _size - pos)
					len = _size - pos;
				for (size_t i = pos + len; i < _size; i++)
				{
					_str[i - len] = _str[i];
				}
				_size -= len;
				_str[_size] = '\0';
			}
		}
		void clear()
		{
			delete[] _str;
			_size = _capacity = 0;
		}
		size_t size()
		{
			return _size;
		}
		size_t capacity()
		{
			return _capacity;
		}
	
		char* _str;
		size_t _size;
		size_t _capacity;
	};

	istream& wrt::operator>>(istream& _cin, wrt::string& s)

	{

		//预分配100个空间

		char* str = (char*)malloc(sizeof(char) * 100);

		char* buf = str;

		int i = 1;

		//预处理：跳过流里面的所有空格和回车

		while ((*buf = getchar()) == ' ' || (*buf == '\n'));



		for (; ; ++i)

		{

			if (*buf == '\n') //回车跳出

			{

				*buf = '\0';

				break;

			}

			else if (*buf == ' ') //空格跳出

			{

				*buf = '\0';

				break;

			}

			else if (i % 100 == 0) //空间不足

			{

				i += 100; //追加100个空间

				str = (char*)realloc(str, i);

			}

			else  //每次getchar()一个值

			{

				buf = (str + i);//为了避免realloc返回首地址改变，不使用++buf，而是用str加上偏移.

				//每次读取一个字符

				*buf = getchar();

			}

		}

		//输入完成,更新s

		s._str = str;

		s._capacity = s._size = i;



		return _cin;

	}
void test1()
	{
		string s1("hello");
		string s2;
		
		cout <<"s1:"<< s1.c_str() << endl;
		cout <<"s2:"<< s2.c_str() << endl;

		s1.push_back(' ');
		s1.push_back('x');
		s2.back_append("hi");
		cout << "s1.push_back(x):" << s1.c_str() << endl;
		cout << "s2.back_append(hi):" << s2.c_str() << endl;

		s1.push_front('o');
		s2.push_front('o');
		cout << "s1.push_front('o'):" << s1.c_str() << endl;
		cout << "s2.push_front('o'):" << s2.c_str() << endl;

		s1.erase(0, 1);
		s2.erase(1, 20);
		cout << "s1.erase(0, 1):" << s1.c_str() << endl;
		cout << "s2.erase(3,20):" <<s2.c_str() <<endl;

		cout << s1.size() << endl;
		cout << s1.capacity() << endl;
		cout << s2.size() << endl;
		cout << s2.capacity() << endl;
		s1.insert(0, "hi");
		s2.insert(1, "o");
		cout << "s1.insert(0, hi):" << s1.c_str() << endl;
		cout << "s2.insert(1,'o'):" << s2.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl;
		cout << s2.size() << endl;
		cout << s2.capacity() << endl;

		s1.append("bb");
		s2 += "ll";
		cout << "s1.append(bb):" << s1.c_str() << endl;
		cout << "s2+=ll :" << s2.c_str() <<endl;

		cout << s1.c_str() << endl;
		cout << s2.c_str() <<endl;


		s2.erase(1, 20);
		cout << s2.c_str() << endl;

	}
	
}

