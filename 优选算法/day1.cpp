//#define _CRT_SECURE_NO_WARNINGS
//
//
////移动0
//class Solution {
//public:
//    void moveZeroes(vector<int>& nums) {
//        int dest = -1, cur = 0;
//        int n = nums.size();
//        while (cur < n)
//        {
//            if (nums[cur] == 0)
//            {
//                cur++;
//            }
//            else
//            {
//                dest++;
//                swap(nums[dest], nums[cur++]);
//            }
//        }
//    }
//};
//
////复写0
//class Solution {
//public:
//    void duplicateZeros(vector<int>& arr) {
//        int n = arr.size();
//        //首先找到最后一个复写位置 然后倒序覆盖式复写
//
//        //这样的方法肯定不够好 无法处理数组前半段很多0
//        // int targ=0,tmp=0;
//        // while(targ < n)
//        // {
//        //     if(arr[targ] != 0) tmp++;
//        //     else tmp+=2;
//        //     if(tmp == n )
//        //      break;
//        //     targ++;
//        // }
//
//        int cur = 0, dest = -1;
//        while (cur < n)
//        {
//            dest += (arr[cur] == 0 ? 2 : 1);
//            if (dest >= n - 1) break;
//            cur++;
//        }
//        if (dest == n)
//        {
//            arr[n - 1] = 0;
//            cur--;
//            dest -= 2;
//        }
//        for (; cur >= 0;)
//        {
//            if (arr[cur] != 0)
//            {
//                arr[dest--] = arr[cur--];
//            }
//            else
//            {
//                arr[dest--] = arr[cur];
//                arr[dest--] = arr[cur--];
//            }
//        }
//
//    }
//};