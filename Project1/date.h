#pragma once
#include <stdio.h>
#include <iostream>
#include <assert.h>
using namespace std;

class Date
{
public:
	Date(int year = 2002, int month = 11, int day = 26);
	
	void Print();
	int Getday(int year, int month);
	bool operator==(const Date& d);
	bool operator!=(const Date& d);
	bool operator<(const Date& d);
	bool operator<=(const Date& d);
	bool operator>(const Date& d);
	bool operator>=(const Date& d);
	// d1 - 100
	Date operator-(int day);

	// d1 - d2;
	int operator-(const Date& d);

	Date& operator+=(int day);
	Date operator+(int day);
	// ++d1
	Date& operator++();

	// d1++
	// int参数 仅仅是为了占位，跟前置重载区分
	Date operator++(int);
private:
	int _year;
	int _month;
	int _day;
};

class Stack
{
	~Stack()
	{
		free(a);
		int size = 0;
		int capacity = 0;
	}
private:
	int size;
	int* a;
	int capacity;
};

//深拷贝
//inline
//String::String(const String& str)
//{
//	m_data = new char[strlen(str.m_data) + 1];
//	strcpy(m_data, str.m_data);
//}


