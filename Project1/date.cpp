#define _CRT_SECURE_NO_WARNINGS
#include "date.h"
Date::Date(int year , int month, int day)
{
	_day = day;
	_month = month;
	_year = year;
}

void Date::Print()
{
	cout << (this->_year) << " " << this->_month << " " << this->_day<<endl;
	cout << endl;
}
int Date::Getday(int year, int month)
{
	assert(month >= 1 && month <= 12);
	int day[] = { 31,29,31,30,31,30,31,31,30,31,30,31 };
	if (month == 2 && (year % 4 == 0 && year % 100 != 0) || (year % 400 != 0))
	{
		day[1] = 28;
	}
	return day[month - 1];
}
bool Date::operator==(const Date& d)
{
	return (d._day == _day) && (d._month == _month) && (d._year == _year);
}
bool Date::operator!=(const Date& d)
{
	return !(*this ==d);
}
bool Date::operator<(const Date& d)
{
	if (_year >= d._year) return false;
	else if (_month >= d._month) return false;
	else if (_day >= d._day) return false;
	return true;
}
bool Date::operator>(const Date& d)
{
	if (_year <= d._year) return false;
	else if (_month <= d._month) return false;
	else if (_day <= d._day) return false;
	return true;
}
bool Date::operator<=(const Date& d)
{
	return !(*this>d);
}

bool Date::operator>=(const Date& d)
{
	return !(*this<d);
}

// d1 - 100
Date Date::operator-(int day)
{
	_day -= day;
	while (_day<1 )
	{
		_month--;
		if (_month < 1)
		{
			_year--;
			_month = 12;
		}
		
	 _day += Getday(_year, _month);
		
	}
	//_day = Getday(_year, _month) - _day;
		return *this;
}

// d1 - d2;
int Date::operator-(const Date& d)
{
	int big_year= max(d._year, _year);
	int small_year = min(d._year, _year);
	int gap_year = (big_year-small_year)*365;
	small_year++;
    int big=max(d._month, _month);
	int small = min(d._month, _month);
	int gap_month = 0;
	while (big!=small)
	{
		gap_month += Getday(_year, small);
		small++;
		
	}
	int gap_day = abs(d._day-_day);
	
	return  gap_year -gap_day -gap_month;
}

Date& Date::operator+=(int day)
{
	_day += day;
	while (_day > Getday(_year, _month))
	{
		_day -= Getday(_year, _month);
		_month++;
		if (_month == 13)
		{
			++_year;
			_month = 1;
		}
	}

	return *this;
}

// d1 + 100
Date Date::operator+(int day)
{
	Date tmp(*this);

	tmp += day;

	return tmp;
}

Date& Date::operator++()
{
	//默认是前置++，先把this++，然后返回结果
	*this+=1;
	return *this;
}

//后置++
Date Date::operator++(int)
{
	Date tmp(*this);

	*this += 1;

	return tmp;
}

//class Student
//{
//	public:
//		Student()
//		{
//			age = 20;
//		}
//		Student(const Student& s)
//		{
//			age = s.age;
//		}
//private:
//	int age;
//	char gender[5];
//};
//int main()
//{
//	Student s1 ;
//	Student s2 = s1;
//}

