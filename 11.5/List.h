#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
typedef int type;
typedef struct SeqList
{
	struct SeqList* prev;
	struct SeqList* next;
	type data;
}SL;

SL* Buy(type x);
void 