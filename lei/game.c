#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "game.h"
void Init(char arr[ROWS][COLS], int rows, int cols,char set)
{
	int i = 0;
	int j = 0;
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			arr[i][j] = set;
		}
	}
}
void Printf(char arr[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	for (j = 0; j <= col; j++)
	{
		printf("%d ", j);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
			printf("%d ", i);
		for (j = 1; j <= row; j++)
		{
			printf("%c ", arr[i][j]);
		}printf("\n");
	}
}
void Set(char arr[ROWS][COLS], int row, int col)
{
	int count = COUNT;
	while (count)
	{
		int x = rand() % row + 1;
		int y = rand() % col + 1;
		if (arr[x][y] == '0')
			arr[x][y] = '1';
		count--;
	}
}
void Play(char arr1[ROWS][COLS], char arr2[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int win = 0;
	while (1)
	{
		printf("请输入坐标\n");
		scanf_s("%d%d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			 if (arr2[x][y] != '*')
				printf("该坐标已经被排查\n");
			else
			{
				 if (arr1[x][y] == '1')
				 {
					 printf("恭喜你被炸死了\n");
					 break;
				 }
				 else
				 {
					 If(arr1,arr2, ROW, COL, x, y);
					 int win = Iswin(arr2, row, col);
					 if (win == 1)
					 {
						 printf("恭喜扫雷成功撒花花！！！\n");
						 Printf(arr2, ROW, COL);
						 break;
					 }
					 Printf(arr2, ROW, COL);
					 Sign(arr2, row, col);
					 Printf(arr2, ROW, COL);
				 }
			}
		}
		else
			printf("能不能看好了再输入\n");
	}
}
static int Num(char arr1[ROWS][COLS], int x,int y)
{
	return arr1[x - 1][y -1] +
		arr1[x - 1][y] +
		arr1[x - 1][y + 1] +
		arr1[x][y - 1] +
		arr1[x][y + 1] +
		arr1[x + 1][y - 1] +
		arr1[x + 1][y] +
		arr1[x + 1][y + 1] - 8 * '0';
}
static void If(char arr1[ROWS][COLS], char arr2[ROWS][COLS], int row, int col, int x, int y)
{
	int c = Num(arr1, x, y);
	if (c == 0)
	{
		arr2[x][y] = ' ';
		if (arr2[x - 1][y - 1] == '*' && x - 1 > 0 && y - 1 >0)
			If(arr1, arr2, row, col, x - 1, y - 1);
		if (arr2[x - 1][y] == '*' && x - 1 >0)
			If(arr1, arr2, row, col, x - 1, y);
		if (arr2[x - 1][y + 1] == '*' && x - 1 > 0 && y + 1 <= col)
			If(arr1, arr2, row, col, x - 1, y + 1);
		if (arr2[x][y - 1] == '*' && y - 1 > 0)
			If(arr1, arr2, row, col, x, y - 1);
		if (arr2[x][y + 1] == '*' && y + 1 <= col)
			If(arr1, arr2, row, col, x, y + 1);
		if (arr2[x + 1][y + 1] == '*' && x + 1 <= col && y + 1 <= col)
			If(arr1, arr2, row, col, x + 1, y + 1);
		if (arr2[x + 1][y] == '*' && x + 1 <= col)
			If(arr1, arr2, row, col, x + 1, y);
		if (arr2[x + 1][y + 1] == '*' && x + 1 <= col && y + 1 <= col)
			If(arr1, arr2, row, col, x + 1, y + 1);
	}
	else
		arr2[x][y] = c+'0';
}
 void Sign(char arr2[ROWS][COLS], int row, int col)
{
	 int input = 0;
	 do 
	 {
	flag:	 printf("请选择是否要进行标记\n");
		 scanf_s("%d", &input);
		 switch (input)
		 {
		 case 1:
		 {
			 int x = 0;
			 int y = 0;
			 printf("请输入要标记的坐标\n");
			 scanf_s("%d%d", &x, &y);
			 if (x >= 1 && x <= row && y >= 1 && y <= col)
			 {
				 if (arr2[x][y] == '*')
				 {
					 arr2[x][y] = '!';
					 Printf(arr2, ROW, COL);

				 }
				 else if (arr2[x][y] == '!')
				 {
					 arr2[x][y] = '*';
					 Printf(arr2, ROW, COL);
				 }
				 else
				 {
					 printf("该坐标已经被标记\n");
					 goto flag;
				 }
			 }
			 else
			 {
				 printf("能不能看清楚再输入真无语\n");
				 goto flag;
			 }
		 }
		 break;
		 case 0:
			 break;
		 default:
			 break;
		 }
	 } while (input);
}
 static int Iswin(char arr2[ROWS][COLS], int row, int col)
 {
	 int count = 0;
	 int i= 0;
	 int j = 0;
	 for (i = 1; i <= row; i++)
	 {
		 for (j = 1; j <= col; j++)
		 {
			 if (arr2[i][j] != '*' && arr2[i][j] != '!')
				 count++;
		 }
	 }
	 if (count == row * col - COUNT)
		 return 1;
	 else
		 return 0;
 }