#pragma once
#define ROW 9
#define COL 9
#define ROWS ROW+2
#define COLS COL+2
#define COUNT 10
#include<time.h>
#include<stdlib.h>
void Init(char arr[ROWS][COLS], int rows, int cols,char set);
void Printf(char arr[ROWS][COLS], int row, int col);
void Set(char arr[ROWS][COLS], int row, int col);
void Play(char arr1[ROWS][COLS], char arr2[ROWS][COLS], int row,int col);
static int Num(char arr1[ROWS][COLS],  int x, int y);
static void If(char arr1[ROWS][COLS], char arr2[ROWS][COLS], int row, int col, int x, int y);
static void Sign(char arr2[ROWS][COLS], int row, int col);
static int Iswin(char arr2[ROWS][COLS], int row, int col);


