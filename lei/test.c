#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "game.h"
void menu()
{
	printf("——————————————————————\n");
	printf("—————————1.play———————————\n");
	printf("—————————0.exit———————————\n");
	printf("——————————————————————\n");
}
void game()
{
	char arr1[ROWS][COLS] = { 0 };
	char arr2[ROWS][COLS] = { 0 };
	Init(arr1, ROWS, COLS,'0');
	Init(arr2, ROWS, COLS,'*');
	Printf(arr2, ROW, COL); 
	Set(arr1, ROW, COL);    
	//Printf(arr1, ROW, COL); //实际没有这步骤
	Play(arr1, arr2, ROW, COL);
}
int main()
{
	menu();
	srand((unsigned int)time(NULL));
	int n = 0;
	flag : printf("请输入数字\n");
	scanf_s("%d", &n);
	switch (n)
	{
	case 1:
	{
		printf("游戏开始喽！！！\n");
		game();
		break;
	}
	case 0:
	{
		printf("退出游戏\n");
		break;
	}
	default:
	{
		printf("真无语能不能好好看看再输入\n");
		goto flag;
	}
	}
	return 0;
}