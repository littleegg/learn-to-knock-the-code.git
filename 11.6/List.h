#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef  int  type;
typedef  struct  SeqList
{
	struct SeqList* prev;
	struct SeqList* next;
	type data;
}SL;

SL* Buy(type x);
SL* Init(void);// 初始化形成一个头
void PushBack(SL*phead,type x);//尾插
void Print(SL* phead);
void PopBack(SL* phead);//尾删
