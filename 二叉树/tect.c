#include "BinaryTree.h"

int main()
{
	BTNode* n1 = BuyNode(1);
	BTNode* n2 = BuyNode(2);
	BTNode* n3 = BuyNode(3);
	BTNode* n4 = BuyNode(4);
	BTNode* n5 = BuyNode(5);
	n1->left = n2;
	n1->right = n4;
	n2->left=n3;
	n4->right = n5;
	Q q;
	QuenceInit(&q);
	
	printf("TreeSize:%d \n", BinaryTreeSize(n1));
	printf("TreeLeafSize:%d \n", BinaryTreeLeafSize(n1));
	printf("第2层的节点个数：%d \n", BinaryTreeLevelKSize(n1, 2));
	BTNode* ret=BinaryTreeFind(n1, 1);
	BinaryTreeLevelOrder(n1, &q);
	//打印队列
	for (int i = 0; i < (&q)->size; i++)
	{
		printf("%d ", (&q)->head->data);
		(&q)->head = (&q)->head->next;
	}
	printf("\n");

	BinaryTreeDestory(&n1);
	BinaryTreeDestory(&n2);
	BinaryTreeDestory(&n3);
	BinaryTreeDestory(&n4);
	BinaryTreeDestory(&n5);

}