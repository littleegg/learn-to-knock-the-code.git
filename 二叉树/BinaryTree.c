#include "BinaryTree.h"

BTNode* BuyNode(BTDataType x)
{
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	if (node == NULL)
	{
		perror("malloc faile");
		exit(-1);
	}
	node->data = x;
	node->left = NULL;
	node->right = NULL;
}


// 二叉树销毁
void BinaryTreeDestory(BTNode** root)
{
	assert(*root);
	(*root)->left = NULL;
	(*root)->right = NULL;
	(*root)->data =0;
	free(*root);
}
// 二叉树节点个数
int BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	int left = BinaryTreeSize(root->left);

	int right = BinaryTreeSize(root->right);
	return 1 + left + right;
}
// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}
	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}
// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}
	return BinaryTreeLevelKSize(root->left, k - 1) + BinaryTreeLevelKSize(root->right, k - 1);
}
// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root->data == x)
	{
		printf("找到啦\n");
		return root;
		
	}
	if (root->left == NULL && root->right == NULL)
	{
		printf("没找到\n");
		return NULL;
	}
	BinaryTreeFind(root->left,x);
	BinaryTreeFind(root->right,x);

}
// 二叉树前序遍历 
void BinaryTreePrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL");
		return;
	}
	printf("%d ", root->data);
	BinaryTreePrevOrder(root->left);
	BinaryTreePrevOrder(root->right);

}
// 二叉树中序遍历
void BinaryTreeInOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL");
		return;
	}
	BinaryTreeInOrder(root->left);
	printf("%d ", root->data);
	
	BinaryTreeInOrder(root->right);

}
// 二叉树后序遍历
void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL");
		return;
	}
	
	BinaryTreePostOrder(root->left);
	 BinaryTreePostOrder(root->right);
    printf("%d ", root->data);
}

//队列初始化
void QuenceInit(Q*q)
{
		assert(q);
	q->head = NULL;
	q->rear = NULL;
	q->size = 0;
}

//入队列
void EnQuence(Q* q,BTNode* root)
{
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("malloc faile");
		exit(-1);
	}
	newnode->data = root->data;
	newnode->next = NULL;
	if (q->head ==NULL && q->rear == NULL)
	{
		q->head = q->rear = newnode;
	}
	else
	{
    q->rear->next = newnode;
	q->rear = newnode;
	}
	q->size++;
}

bool QEmpty(Q* q)//是否为空
{
	assert(q);
	return q->head== NULL  && q->rear==NULL;
 }//空就是1


//出队列
QNode* DeQuence(Q* q)
{
	assert(q);
	assert(!QEmpty(q));
	if (q->head->next == NULL)
	{
		free(q->head);
		q->head = q->rear = NULL;
	}
	else
	{
		QNode* cur = q->head;
		QNode* next =cur->next;
		q->head = next;
		return cur;
	}
	q->size--;
}

// 层序遍历
void BinaryTreeLevelOrder(BTNode* root,Q*q )
{
	assert(root);
	
	//这个数组里面存储的就是层序之后的二叉树
	/*BTDataType* newarray = (BTDataType*)malloc(sizeof(BTDataType)*((&q)->size));
	if (newarray == NULL)
	{
		perror("newarray fail");
		exit(-1);
	}
	BTDataType* cur = newarray;*/
	EnQuence(q, root);
	
	//BuyNode(root->data);

	/**cur = root->data;
	cur = cur+1;*/
	//DeQuence(&q);
	if (root->left == NULL && root->right==NULL)
	{
		return ;
	}
	BinaryTreeLevelOrder(root->left,q);
	BinaryTreeLevelOrder(root->right,q);
}



bool  EmptyTree(BTNode* root)
{
	return root == NULL;//如果是空，返回TRUE
}
