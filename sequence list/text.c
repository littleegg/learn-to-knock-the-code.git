#define _CRT_SECURE_NO_WARNINGS
#include "list.h"

void Test1(void)
{
	SL* n1 = Buy(1);
	SL* n2 = Buy(1);
	SL* n3 = Buy(1);
	SL* n4 = Buy(1);
	n1->next = n2;
	n2->next = n3;
	n3->next = n4;
	Print(n1);

	PopFront(&n1);
	Print(n1);
}

void Test2(void)
{
	SL* plist;
	PushFront(&plist, 1);
	PushFront(&plist, 2);
	PushFront(&plist, 3);
	PushFront(&plist, 4);
	Print(plist);

	PopFront(&plist);
	Print(plist);


}
int main()
{
//	Test1();
	Test2();
	return 0;
}