#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
typedef int type;
typedef struct Sequence_List
{
	type data;
	struct Sequence_List* next;
}SL;

void PopFront(SL** pphead);
void Print(SL* phead);
SL* Creat(type x);
SL* Buy(type x);
void PushFront(SL** pphead, type x);


