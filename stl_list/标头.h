#pragma once
namespace wrt
{
	template <typename T>
		struct _list_node
	{
			_list_node<T>* _prev;
			_list_node<T>* _next;
			T data;
			_list_node(const T& x)  //用x初始化节点
				:_prev(nullptr)
				,_next(nullptr)
				,data(x)
			{
			}
	};
		template <typename T, typename Ref,class Ptr>
			struct __list_iterator
		{
			typedef _list_node<T> node;
			typedef __list_iterator<T, Ref,Ptr> Self;
			node* _pnode;
			__list_iterator(node* p)
				:_pnode(p)
			{}
			Ref operator*()
			{
				return _pnode->data;
			}
			Ptr operator->()
			{
				return &_pnode->data;
			}
			//在同一个类里面实现就是不行，因为const——iterator只能遍历，不能++
		/*	const T& operator*() const 
			{
				return _pnode->data;
			}*/
			//前置
			Self& operator++()
			{
				_pnode = _pnode->_next;
				return *this;
			}
			//后置
			Self& operator++(int)
			{
				Self tmp(*this);
				_pnode = _pnode->_next;
				return tmp;
			}
			//前置
			Self& operator--()
			{
				_pnode = _pnode->_prev;
				return *this;
			}
			//后置
			Self& operator--(int )
			{
				Self tmp(*this);
				_pnode = _pnode->_prev;
				return tmp;
			}
			bool operator!=(const Self& it)
			{
				return _pnode != it._pnode;
			}
		};
		/*	template <class T>
			struct __list_const_iterator
			{
				typedef _list_node<T> node;
				node* _pnode;
				__list_const_iterator(node* p)
					:_pnode(p)
				{}
				const T& operator*() const 
				{
					return _pnode->data;
				}
				__list_const_iterator<T>& operator++()
				{
					_pnode = _pnode->_next;
					return *this;
				}
				bool operator!=(const __list_const_iterator<T>& it)
				{
					return _pnode != it._pnode;
				}
			};*/
			template <typename T>

		class list
		{
			typedef _list_node<T> node;
		public:
			//typedef __list_iterator<T> iterator;
			//typedef __list_const_iterator<T>  const_iterator;
			typedef __list_iterator<T,T&,T*> iterator;
			typedef __list_iterator<T,const T&,T*>  const_iterator;
			size_t _size()
			{
				return size;
			}
			bool rmpty()
			{
				//return head->next==head?
				return size == 0 ;
			}
			iterator begin()
			{
				return iterator(head->_next);
			}
			iterator end()
			{
				return iterator(head);
			}
			const_iterator begin() const 
			{
				return iterator(head->_next);
			}
			const_iterator end() const 
			{
				return iterator(head);
			}
			
			void push_back(const T& x)
			{
				//node* newnode  = new node(x);
				//node* tail = head->_prev;
				////head  tail newnode
				//tail->_next = newnode;
				//newnode->_next = head;
				//newnode->_prev = tail;
				//head->_prev = newnode;
				insert(end(), x);
			}
			void push_front(const T& x)
			{
				insert(begin(), x);
			}
			~list()
			{
				clear();
				//此时需要把头节点也删除
				delete head;
				head = nullptr;
				size = 0;
			}
			
			//拷贝构造
			//l2=l1
			/*list<T>& operator=(const list<T>& l)
			{
				if (*this != l)
				{
					clear();
					for (const auto&e :l)
					{
						push_back(e);
					}
				}
				return *this;
			}*/
			void empty_initialize()
			{
				head = new node(T());
				head->_next = head;
				head->_prev = head;
				size = 0;
			}
			list()
			{
				empty_initialize();
			}
			//l2(l1)
		/*	list(const list <T>& l)
			{
				empty_initialize();
				for (const auto& e : l)
				{
					push_back(e);
				}
			}*/
			//拷贝构造的现代写法
			template <class InputIterator>
			list(InputIterator first, InputIterator last)
				{
					empty_initialize();
			    while (first != last)
			    {
				push_back(*first);
				++first;
			    }
				}
				void swap(const list<T>& l)
				{
					std::swap(head, l.head); //两个链表交换只需交换头结点
				}
				//l2(l1)
				list( list<T>& l)
				{
					empty_initialize();
					list<T> tmp(l.begin(), l.end());
					swap(tmp); //tmp出作用域销毁
				}
				//l2=l1  这是对于一个已经存在的对象l1，无需构造头结点
				list <T>& operator=(const list<T>& l)
				{
					swap(l);
					return *this;
				}
				void clear()
			{
				iterator it = begin();
				while(it!=end())
				{
					it=erase(it);
				}
				//头节点不能删除
				size = 0;
			}
			void pop_back()
			{
				erase(--end());
			}
			void pop_front()
			{
				erase(begin());
			}

			iterator insert(iterator  pos, const T& x)
			{
				node* newnode = new node(x);
				node* cur = pos._pnode;
				node* prev = cur->_prev;
				//prev newnode cur
				prev->_next = newnode;
				newnode->_next = cur;
				newnode->_prev = prev;
				cur->_prev = newnode;
				++size;
				return iterator(newnode);
			}
			iterator erase(iterator pos)
			{
				assert(pos != end());
				node* cur = pos._pnode;
				node* prev = cur->_prev;
				node* next = cur->_next;
				//prev cur next
				prev->_next = next;
				next->_prev = prev;
				delete cur;
				--size;
				return  iterator(next);
			}

		private :
			node* head;
			size_t size;
		};
		struct Pos
		{
			size_t _row;
			size_t _col;
			Pos(size_t row=0,size_t col=0)  //一定要时刻记着写一个默认构造！！！！！！
				:_row(row)
				,_col(col)
			{}
		};
		void test()
		{
			list<Pos> lt;
			Pos p1(1, 1);
			lt.push_back(p1);
			lt.push_back(p1);
			lt.push_back(p1);
			lt.push_back(Pos(2, 2)); //匿名函数
			lt.push_back(Pos(3, 3));
			list<Pos>::iterator it = lt.begin();
			while (it != lt.end())
			{
			//	cout << *it << " ";
				//cout << it.operator->()->_row << ":" << it->_col << endl;
				cout << it->_row << ":" << it->_col << endl;
				it++;
			}
		}
		/*void test()
		{
			list<int>  lt;
			lt.push_back(1);
			lt.push_back(3);
			lt.push_back(4);
			lt.push_back(5);
			lt.push_back(6);
			lt.push_back(7);
			lt.insert(lt.begin(), 5);
			lt.erase(lt.begin());
			lt.push_back(40);
			list<int>::iterator it = lt.begin();
			while (it != lt.end())
			{
				cout << *it <<" ";
			++it;
			}
			cout <<endl;
			cout << lt._size() << endl;
		}*/
}
