#define _CRT_SECURE_NO_WARNINGS

//int lowestCommonAncestor(TreeNode* root, int p, int q) {
//    // write code here、
//    if (!root) return -1;
//    if (p < root->val && q < root->val)
//        return lowestCommonAncestor(root->left, p, q); //在左子树里的子问题
//    else if (p > root->val && q > root->val) //在右子树的子问题
//        return lowestCommonAncestor(root->right, p, q);
//    //在两个子树
//    else return root->val;
//}

//bool Isintree(TreeNode* root, int x) { //判断值为x的节点是不是在以root为根的树里
//    if (!root) return false;
//    return   root->val == x || Isintree(root->left, x) ||
//        Isintree(root->right, x);
//
//}
//int lowestCommonAncestor(TreeNode* root, int o1, int o2) {
//    if (!root) return -1; //空树直接没有
//    if (root->val == o1 || root->val == o2) return root->val; //根节点就是共同祖先
//    bool Iso1inleft = Isintree(root->left, o1); //记录o1在不在左树
//    bool Iso1inright = !Iso1inleft; //o1在不在右树
//    bool Iso2inleft = Isintree(root->left, o2); //o2在不在左树
//    bool Iso2inright = !Iso2inleft; //o2在不在右树
//    if ((Iso1inleft && Iso2inright) || (Iso1inright && Iso2inleft)) //o1 o2在不同侧的子树
//        return root->val;
//    if (Iso1inleft && Iso2inleft) //都在左树，递归到左子树
//        return lowestCommonAncestor(root->left, o1, o2);
//    else //都在右树，递归到右子树
//        return lowestCommonAncestor(root->right, o1, o2);
//}


//
//bool FindPath(TreeNode* root, stack<int>& s, int o) //找到这个节点的所有祖先
//{
//    if (!root) return false; //遇到空节点肯定不是祖先
//    s.push(root->val); //先把这个节点入栈
//    if (root->val == o) //找到了目标节点，直接返回
//        return true;
//    if (FindPath(root->left, s, o))  return true; //在左子树里找到他，返回true
//
//    if (FindPath(root->right, s, o)) return true; //右子树中找到他，true
//    s.pop(); //走到这说明左/右子树没找到这个节点 ，这个root就不是目标节点的祖先，pop
//    return false; //返回在这个路径没找到
//}
//int lowestCommonAncestor(TreeNode* root, int o1, int o2) {
//    // 两个栈记录他们的祖先列表
//    stack<int> s1;
//    stack<int> s2;
//    FindPath(root, s1, o1);
//    FindPath(root, s2, o2);
//    while (s1.size() < s2.size()) //长度不一样
//        s2.pop(); //更长的pop
//    while (s1.size() > s2.size())
//        s1.pop();
//    while (s1.top() != s2.top()) //当前不是公共祖先 
//    {
//        //都把这个节点pop
//        s1.pop();
//        s2.pop();
//    }
//    return s1.top(); //或者s2.top() 都一样的值
//}

//
//void dfs_Serialize(TreeNode* root, string& s)
//{
//    if (!root)//如果是空节点，直接写一个#，空节点的子节点被省略，所以直接返回
//    {
//        s += '#';
//        return;
//    }
//    s += to_string(root->val) + ','; //把非空节点的val变成字符，写入到字符串
//    dfs_Serialize(root->left, s); //左子树
//    dfs_Serialize(root->right, s);//右子树
//}
//char* Serialize(TreeNode* root) { //用二叉树写字符串数组
//    string s; //首先写成字符串
//    dfs_Serialize(root, s); //深度优先遍历（前序）
//    char* ans = new char[s.size() + 1]; //最后应该输出数组形式，先开数组（加上1写'\0')
//    memcpy(ans, s.c_str(), s.size()); //把字符串拷贝给数组
//    ans[s.size()] = '\0';//终止符
//    return ans;
//}
//TreeNode* dfs_Deserialize(char*& s) //把字符串变二叉树，注意这个字符串在递归的时候不是每次从头开始，所以&
//{
//    if (*s == '#')//是空节点的标志，直接返回nullptr
//    {
//        s++; //s向后走一个，因为这个字符对应的节点已经被表示完了
//        return nullptr;
//    }
//    int val = 0; //字符串对应的节点val
//    while (*s != ',') //看样例，字符串以，分割
//    {
//        val = val * 10 + (*s - '0'); //万一不是个位数，需要这样算好每一位
//        s++;//s往后走
//    }
//    s++; //把逗号跳过
//    TreeNode* root = new TreeNode(val); //用val创建节点
//    root->left = dfs_Deserialize(s); //左节点的创建
//    root->right = dfs_Deserialize(s);//右节点
//    return root; //返回根
//}
//TreeNode* Deserialize(char* str) {
//    return dfs_Deserialize(str);
//}



//char* Serialize(TreeNode* root) { //用层序来写字符串
//    if (!root) return nullptr; //如果是空节点，不需要记录他的孩子
//    string str; 
//    queue<TreeNode*> que;
//    que.push(root); //先把根入队
//    while (!que.empty())
//    {
//        auto node = que.front(); //取队头元素
//        que.pop();
//        if (node) //取出的节点不是空
//        {
//            str += to_string(node->val) + ","; //字符串保存这个节点val对应的字符，并且在字符串里面写入分割符号
//            que.push(node->left);//带入左孩子
//            que.push(node->right);
//        }
//        else {
//            str += "#";//是空，写成#
//        }
//    }
//    auto res = new char[str.size() + 1]; //和上一方法这里的用处一样
//    strcpy(res, str.c_str());
//    res[str.size()] = '\0';
//    return res;
//}
//int Getinteger(char* str, int& k)  //把字符转化成数字
//{
//    int val = 0;
//    while (str[k] != ',' && str[k] != '\0' && str[k] != '#')
//    {
//        val = val * 10 + str[k] - '0';
//        ++k;
//    }
//    if (str[k] == '\0') return -1;
//    if (str[k] == '#') val = -1;
//    k++;
//    return val;
//}
//TreeNode* Deserialize(char* str) {
//    if (!str) return nullptr;
//    int k = 0; //记录字符串的下标
//    queue<TreeNode* > que;
//    auto head = new TreeNode(Getinteger(str, k)); //一定要把字符串下标也传，要不然找不到位置了
//    que.push(head); //把头结点入队
//    while (!que.empty())
//    {
//        auto node = que.front();//队头
//        que.pop();
//        //这里的反序列化是根据前序用字符串构建二叉树
//        int leftval = Getinteger(str, k); //把字符串转换成左节点的val
//        if (leftval != -1)//如果左孩子的val是-1，代表是空节点
//        {
//            //不是空
//            auto nodeleft = new TreeNode(leftval) ;//创建节点
//            node->left = nodeleft; //node和左孩子链接
//            que.push(nodeleft);//链接好的节点入队
//        }
//        //右节点同样的方法
//        int rightval = Getinteger(str, k);
//        if (rightval != -1)
//        {
//            auto noderight = new TreeNode(rightval);
//            node->right = noderight;
//            que.push(noderight);
//        }
//    }
//    return head; //返回头结点
//}


//
//TreeNode* _reConstructBinaryTree(vector<int>& pre, vector<int>& vin, int& k, //k一定要给&，在每个递归里是看得见改变的
//    int begin, int end) {
//    if (begin > end) return nullptr;  //区间不成立，说明为空
//    int rooti = begin; //rooti记录中序节点里面根的下标
//    while (rooti <= end) { //根的下标肯定在合法区间里
//        if (vin[rooti] == pre[k]) //在中序数组里找到根，k记录每个根在前序里的下标
//            break;
//        ++rooti;
//    }
//    //rooti代表根节点的下标
//    //[begin,rooti-1] rooti [rooti+1,end]
//    TreeNode* root = new TreeNode(pre[k++]); //创建节点
//    root->left =
//        _reConstructBinaryTree(pre, vin, k, begin, rooti - 1); //左孩子在左区间找（因为中序是左根右）
//    root->right =
//        _reConstructBinaryTree(pre, vin, k, rooti + 1, end);
//    return root;
//}
//TreeNode* reConstructBinaryTree(vector<int> pre, vector<int> vin) {
//    int k = 0;
//    return _reConstructBinaryTree(pre, vin, k, 0, pre.size() - 1);
//}


//TreeNode* buildtree(vector<int>& xianxu, vector<int>& zhongxu, int& k, //建树（和前面重建二叉树是一样的）
//    int begin, int end) {
//    if (begin > end) return nullptr;
//    int rooti = begin;
//    while (rooti <= end) {
//        if (xianxu[k] == zhongxu[rooti])
//            break;
//        ++rooti;
//    }
//    TreeNode* root = new TreeNode(xianxu[k++]);
//    root->left = buildtree(xianxu, zhongxu, k, begin, rooti - 1);
//    root->right = buildtree(xianxu, zhongxu, k, rooti + 1, end);
//    return root;
//}
//void bfs(TreeNode* root, vector<int>& ans) { //层序+找最右节点
//    if (!root) return;
//    queue<TreeNode*> q;
//    q.push(root);
//    while (!q.empty()) {
//        int size = q.size(); //当前层数的size
//        while (size--) {
//            TreeNode* node = q.front();
//            q.pop();
//            if (size == 0) ans.push_back(node->val);  //直到size==0，找到最右节点
//            if (node->left) q.push(node->left);
//            if (node->right) q.push(node->right);
//        }
//    }
//}
//vector<int> solve(vector<int>& xianxu, vector<int>& zhongxu) {
//    // write code here
//    vector<int> ans;
//    int k = 0;
//    TreeNode* root = buildtree(xianxu, zhongxu, k, 0, xianxu.size() - 1);
//    bfs(root, ans);
//    return ans;
//}


//
//void _solve(vector<int> xianxu, vector<int> zhongxu, vector<int>& ans,
//    int level) {
//    if (xianxu.empty()) return;   //如果前序是空的，证明根是空
//    if (ans.size() < level) ans.push_back(xianxu[0]); //ans里面的元素个数一定是等于层数，如果小于，直接把当前的根入
//    else
//        ans[level - 1] = xianxu[0]; //level应该也是ans的个数，最后一个元素下标就是level-1
//    int headpos = 0; //还是找中序里的根
//    while (zhongxu[headpos] != xianxu[0])
//        headpos++;
//    _solve(vector<int>(xianxu.begin() + 1, xianxu.begin() + headpos + 1), 
//        vector<int>(zhongxu.begin(), zhongxu.begin() + headpos), ans, level + 1);
//    _solve(vector<int>(xianxu.begin() + headpos + 1, xianxu.end()),
//        vector<int>(zhongxu.begin() + headpos + 1, zhongxu.end()), ans, level + 1);
//}
//vector<int> solve(vector<int>& xianxu, vector<int>& zhongxu) {
//    vector<int> ans;
//    _solve(xianxu, zhongxu, ans, 1);
//    return ans;
//}

//
//TreeNode* _reConstructBinaryTree(vector<int> pre, vector<int>vin) {
//    if (pre.empty()) return nullptr;
//    int rooti = 0;
//    while (vin[rooti] != pre[0]) {
//        ++rooti;
//    }
//    TreeNode* root = new TreeNode(pre[0]);
//    root->left =
//        _reConstructBinaryTree(vector<int>(pre.begin() + 1, pre.begin() + rooti + 1),
//            vector<int>(vin.begin(), vin.begin() + rooti));
//    root->right =
//        _reConstructBinaryTree(vector<int>(pre.begin() + 1 + rooti, pre.end()),
//            vector<int>(vin.begin() + rooti + 1, vin.end()));
//    return root;
//}
//TreeNode* reConstructBinaryTree(vector<int> pre, vector<int> vin) {
//    return _reConstructBinaryTree(pre, vin);
//}



//#include<vector>
//#include<iostream>
//#include<algorithm>
//using namespace std;
//int main()
//{
//    int n;
//    while (cin >> n)
//    {
//    vector<int> v;
//    v.resize(3 * n);
//    for (int i = 0; i < v.size(); i++)
//    {
//        cin >> v[i];
//   }
//    sort(v.begin(), v.end());
//    long long sum = 0;
//    for (int i = n;i < v.size(); i += 2)
//    {
//        sum += v[i];
//    }
//    cout << sum<<endl;
//    }
//    return 0;
//}

//vector<int> preorderTraversal(TreeNode* root) {
//    // write code here
//    vector <int > v;
//    stack<TreeNode*> s;
//    TreeNode* cur = root; 
//    while (cur || !s.empty()) //s保存的都是左子树，cur也要遍历右子树，所以都为空才结束
//    {
//        while (cur) //一路向左，前序就是左 根 右
//        {
//            v.push_back(cur->val);//因为前序，所以一路的节点都是要保留，保留左孩子的同时也保存了根
//            s.push(cur);  
//            cur = cur->left;
//        }
//        TreeNode* top = s.top(); //左走到空，取栈顶节点，一定是根节点，因为前一个进去的cur就是现在cur（左孩子）的父节点
//        s.pop(); //删
//        cur = top->right; //走右子树
//    }
//    return v;
//}


//vector<int> inorderTraversal(TreeNode* root) {
//    vector <int > v;
//    stack<TreeNode* > s;
//    TreeNode* cur = root;
//    while (cur || !s.empty())
//    {
//        //中序是左 根 右，所以一路向左的时候不能同时把节点保存在v，因为向左都是先访问根，然后走他的左，你连根都不想要为什么全部加到v？
//        while (cur)
//        {
//            s.push(cur);    //保存在栈里，因为这里有根节点，可以帮助找到右子树
//            cur = cur->left;
//        }
//        TreeNode* top = s.top(); //栈顶，同理还是父节点
//        s.pop();
//        v.push_back(top->val);  //此时的父节点就需要加到v
//        cur = top->right; //走右
//    }
//    return v;
//}
//
//vector<int> postorderTraversal(TreeNode* root) {
//    vector <int > v;
//    if (!root) return v;
//    stack<TreeNode* > s1, s2;
//    s1.push(root);
//    while (!s1.empty())
//    {
//        TreeNode* top1 = s1.top();
//        s1.pop();
//        if (top1->left) s1.push(top1->left);
//        if (top1->right) s1.push(top1->right);
//        s2.push(top1);
//    }
//    while (!s2.empty())
//    {
//        v.push_back(s2.top()->val);
//        s2.pop();
//    }
//    return v;
//}

//
//
//vector<int> postorderTraversal(TreeNode* root) {
//    vector <int > v;
//    if (!root) return v;
//    stack<TreeNode* > s1;
//    //s1.push(root);
//    TreeNode* cur = root;
//    TreeNode* prev = nullptr;
//    while (!s1.empty() || cur)
//    {
//        while (cur)
//        {
//            s1.push(cur);
//            cur = cur->left;
//        }
//        TreeNode* top = s1.top();
//        if (!top->right || top->right == prev)
//        {
//            s1.pop();
//            v.push_back(top->val);
//            prev = top;
//        }
//        else
//            cur = top->right;
//
//    }
//    return v;
//
//}
