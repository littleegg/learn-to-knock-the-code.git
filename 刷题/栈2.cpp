#define _CRT_SECURE_NO_WARNINGS
//class Solution {
//public:
//    priority_queue<int> left; //左区间的堆（对应大堆），优先级队列默认大堆
//
//    priority_queue<int, vector<int>, greater<int>> right;//右区间，对应小堆
//    void Insert(int num) {
//        //规定左区间的数目比右区间少1或者相等（或者规定右区间少一也可，只是方便理解）
//        if ((left.size() + right.size()) & 1) { //左区间和右区间总个数是奇数的意思
//            if (!left.empty() && num < left.top()) //这个值就是左区间的值（因为他比大根堆顶小），但是由于左右区间的总个数是奇数，也就是左区间已经和右区间不相等（根据规定，左区间比右区间少一/相等，但是这个数字还必须是左区间的必须插入左区间内），此时不能改变这种平衡，所以要把左区间最大的数挤到右区间
//            {
//                left.push(num);//把这个数字压进去，找到里面最大的 这个最大的应该被挤到右区间
//                right.push(left.top());//这个数字应该插入右区间（保证左<右）
//                left.pop();
//            }
//            else {
//                right.push(num); //左是空（根据规定，左比右少1/相等，如果此时入了左，破坏结构）或者num比左边top（）还大（应到右区间）
//            }
//        }
//        else //此时左右区间总个数是偶数，证明两个区间个数相等，由于左比右小1，所以优先考虑把数字插入右
//        {
//            if (!right.empty() && num > right.top()) //右不是空，并且这个数字应该在右
//            {
//                right.push(num); //和上面一样的做法
//                left.push(right.top());
//                right.pop();
//            }
//            else {
//                left.push(num);
//            }
//        }
//    }
//
//    double GetMedian() {
//        if (left.size() == right.size()) //左右相等个数，那么加上最中间的元素，整个数组是奇数个，中位数是平均值
//            return (left.top() + right.top()) / 2.0;
//        else if (left.size() > right.size()) //左边区间个数更多，中位数在左
//            return left.top();
//        else
//            return right.top();//否则在右
//    }
//
//};


//int MoreThanHalfNum_Solution(vector<int> numbers) {
//    //   int a[10001]={0};
//    //    for(int i=0;i<numbers.size();i++)
//    //    {
//    //     a[numbers[i]]++;
//    //     if(a[numbers[i]]>(numbers.size()/2))
//    //     return numbers[i];
//    //    } 
//    int mid = numbers.size() / 2;
//    int count;
//    sort(numbers.begin(), numbers.end());
//    for (int i = 0; i < numbers.size(); i++)
//    {
//        if (numbers[i] == numbers[mid])
//            count++;
//    }
//    if (count > mid)
//        return numbers[mid];
//    return 0;
//}


//
//#include<iostream>
//#include<string>
//using namespace std;
//int main()
//{
//    string str, res, cur;
//    cin >> str;
//    for (int i = 0; i <= str.length(); i++) {
//        if (str[i] >= '0' && str[i] <= '9')cur += str[i];
//        else {
//            if (res.length() < cur.length())res = cur;
//            cur = "";
//        }
//    }
//    cout << res;
//    return 0;
//}