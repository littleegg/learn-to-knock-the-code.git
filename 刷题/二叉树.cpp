#define _CRT_SECURE_NO_WARNINGS

//class Solution {
//public:
//    void level(vector<vector<int>>& vv, TreeNode* root, int depth)  
//    {
//        if (!root) return;
//        if (vv.size() > depth)
//            vv[depth].push_back(root->val);
//        else
//        {
//            vector<int> v;
//            v.push_back(root->val);
//            vv.push_back(v);
//        }
//        level(vv, root->left, depth + 1);
//        level(vv, root->right, depth + 1);
//
//    }
//    vector<vector<int> > Print(TreeNode* pRoot) {
//        vector<vector<int>> vv; //创建二维数组
//        level(vv, pRoot, 0);  //层序
//        for (int i = 0; i < vv.size(); i++)
//        {
//            if (i % 2) //奇数层逆置
//                reverse(vv[i].begin(), vv[i].end());
//        }
//        return vv;
//    }
//};

//int maxDepth(TreeNode* root) {
//    // write code here
//    if (!root) return 0;
//    int a1 = maxDepth(root->left);
//    int a2 = maxDepth(root->right);
//
//    return max(a1, a2) + 1;
//}


//bool hasPathSum(TreeNode* root, int sum) {
//    // write code here
//    if (!root) return false;
//    if (!root->left && !root->right) return root->val == sum;
//    return  hasPathSum(root->left, sum - root->val) || hasPathSum(root->right, sum - root->val);
//}

//TreeNode* Convert(TreeNode* pRootOfTree)
//{
//	if (!pRootOfTree) return nullptr; //空树，直接返回
//	stack<TreeNode*> s; //栈
//	TreeNode* head = nullptr, * pre = nullptr; //head是双向链表的头，pre是链接位置的前一个
//	bool First = true; //判断是不是链表第一个节点
//	while (pRootOfTree || !s.empty()) //栈不为空说明这个路径还没有走完
//	{
//		while (pRootOfTree) //一直到最左侧的节点（找最小)
//		{
//			s.push(pRootOfTree); //把经过的每一个节点 入栈
//			pRootOfTree = pRootOfTree->left; //往左走
//		}
//		pRootOfTree = s.top(); //由于刚才结束条件是空，所以栈顶元素就是最小节点
//		s.pop(); //删除栈顶，这样下次找到次小的
//		if (First) //判断是不是链表头
//		{
//			head = pRootOfTree;//当前整个树最小节点就是链表头
//			pre = pRootOfTree;
//			First = false;
//		}
//		else {
//			pre->right = pRootOfTree;//不是链表头就正常连接
//			//pre  pRootOfTree 
//			pRootOfTree->left = pre;
//			pre = pRootOfTree; //pre后移
//		}
//		pRootOfTree = pRootOfTree->right; //右走
//	}
//	return head;
//}


//TreeNode* Convert(TreeNode* pRootOfTree) {
//
//	if (!pRootOfTree) return nullptr;
//	Convert(pRootOfTree->left); 
//	if (!pre)
//	{
//		head = pRootOfTree;
//		pre = pRootOfTree;
//	}
//	else {
//		pre->right = pRootOfTree;
//		pRootOfTree->left = pre;
//		pre = pRootOfTree;
//
//	}
//	Convert(pRootOfTree->right);
//	return head;
//}


//bool _isSymmetrical(TreeNode* p1, TreeNode* p2) {
//    if (!p1 && !p2) return true; //两个节点都是空，true
//    if ((p1 && !p2) || (!p1 && p2) || (p1->val != p2->val)) return false;//一个空另一个不空或者两个节点值不一样都false
//
//    return _isSymmetrical(p1->left, p2->right) && //看镜像而不是看对称
//        _isSymmetrical(p1->right, p2->left);
//
//}
//bool isSymmetrical(TreeNode* pRoot) {
//    if (!pRoot) return true; //空树自然镜像
//    return _isSymmetrical(pRoot->left, pRoot->right);
//}