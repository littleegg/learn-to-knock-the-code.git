#define _CRT_SECURE_NO_WARNINGS

//static bool comp(const Interval& a, const Interval& b)
//{
//    return a.start < b.start;
//}
//vector<Interval> merge(vector<Interval>& intervals) {
//    sort(intervals.begin(), intervals.end(), comp);
//    vector<Interval> ans;
//    if (intervals.empty()) return ans;
//    Interval tmp = intervals[0];
//    for (int i = 1; i < intervals.size(); i++)
//    {
//        Interval a = intervals[i];
//        if (tmp.end >= a.start)
//            //需要合并
//            tmp.end = max(tmp.end, a.end);
//        else
//        {
//            ans.push_back(tmp);
//            tmp = a;
//        }
//    }
//    ans.push_back(tmp);
//    return ans;
//}


//void merge(int A[], int m, int B[], int n) {
//    int newsize = m + n - 1;
//    while (m && n) {
//        if (A[m - 1] > B[n - 1]) {
//            A[newsize] = A[m - 1];
//            m--;
//            newsize--;
//        }
//        else {
//            A[newsize] = B[n - 1];
//            n--;
//            newsize--;
//        }
//    }
//    while (m) {
//        //说明只有A还有元素
//        A[newsize] = A[m - 1];
//        m--;
//        newsize--;
//    }
//    while (n) {
//
//        A[newsize] = B[n - 1];
//        n--;
//        newsize--;
//    }
//}


//bool judge(string str) {
//    // write code here
//    if (str.empty()) return true;
//    int begin = 0, end = str.size() - 1;
//    while (begin <= end)
//    {
//        if (str[begin] != str[end])
//            return false;
//        begin++; end--;
//    }
//    return true;
//}


//string minWindow(string s, string t) {
//    unordered_map<char, int> hs, ht;
//    for (auto c : t) ht[c] ++;
//    string res;
//    int cnt = 0;
//    //区间[i,j]
//    for (int i = 0, j = 0; i < s.size(); i++) {
//        hs[s[i]] ++; //(当前字符在窗口中的次数++）
//     
//        if (hs[s[i]] <= ht[s[i]]) cnt++;   // 如果当前字符是有效字符，那么窗口内有效字符个数 + 1
//
//        
//        while (hs[s[j]] > ht[s[j]]) hs[s[j++]] --; //扩大窗口，找到一个j，使得窗口中恰好有T
//      
//        if (cnt == t.size()) {  // 如果当前有效窗口个数等于 t 中的字符个数，尝试更新答案
//            if (res.empty() || i - j + 1 < res.size()) //如果答案是空或者[i,j]区间的长度<答案的长度（说明找到更小的子串）
//                res = s.substr(j, i - j + 1); //把这个
//        }
//    }
//    return res;
//}


//string solve(string str) {
//    // write code here
//    for (int i = 0; i < str.size() / 2; i++)
//        swap(str[i], str[str.size() - i - 1]);
//
//    return str;
//}