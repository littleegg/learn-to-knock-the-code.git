#define _CRT_SECURE_NO_WARNINGS
//TreeNode* mergeTrees(TreeNode* t1, TreeNode* t2) {
//    // write code here
//    if (t1 && t2) //如果两个节点都不空
//    {
//        t1->val += t2->val;//以t1为基准，把t2的val加到他身上
//        t1->left = mergeTrees(t1->left, t2->left); //左
//        t1->right = mergeTrees(t1->right, t2->right); //右
//        return t1; //返回t1（基准树）
//    }
//    //t1，t1至少一个是空
//    else if (t1) return t1;  //基准树的节点不为空（t2==nullptr），直接返回
//    else
//        return t2; //t1==nullptr，返回t2
//}


//TreeNode* Mirror(TreeNode* pRoot) {
//    // write code here
//    if (!pRoot) return nullptr; //碰到空节点直接返回
//    TreeNode* p; 
//    p = pRoot->left;
//    pRoot->left = pRoot->right;
//    pRoot->right = p; 
//    Mirror(pRoot->left);  //左
//    Mirror(pRoot->right); //右
//    return pRoot;
//}

//bool _isValidBST(TreeNode* root, TreeNode* min, TreeNode* max)
//{
//    if (!root) return true; //空节点一定符合要求
//    if (min && min->val >= root->val) return false; //右孩子val>根val>左孩子val
//    if (max && max->val <= root->val) return false;
//    return _isValidBST(root->left, min, root) && _isValidBST(root->right, root, max);
//    //比左孩子大的节点是他的父节点，比左孩子小的节点没有限制
//    //右孩子同理，只有比他小的节点是父节点，比他大的没有限制
//}
//bool isValidBST(TreeNode* root) {
//    // write code here
////保存最小节点和最大节点，根没有束缚直接给空
//    return _isValidBST(root, nullptr, nullptr); 
//}

//bool isCompleteTree(TreeNode* root)
//{
//    if (root == nullptr)
//        return true;
//    queue<TreeNode*> q;
//    q.push(root);
//    while (!q.empty())
//    {
//        TreeNode* node = q.front();
//        q.pop();
//        //如果为空，则判断后续队列中有无非空节点，若有非空节点，则返回false。反之返回true。
//        if (node == nullptr) //一直到空跳出
//        {
//            break;
//        }
//        q.push(node->left);//空也压入栈中
//        q.push(node->right);//空也压入栈中
//    }
//    //到这里都是已经遇到空的
//    while (!q.empty())
//    {
//        TreeNode* node = q.front();
//        q.pop();
//        if (node)//还有空直接判错
//        {
//            return false;
//        }
//    }
//    return true;
//}


//bool isCompleteTree(TreeNode* root)
//{
//    if (root == nullptr) //空树直接对
//        return true;
//    queue<TreeNode*> q; //队列
//    q.push(root);
//    bool saturate = true; //判断是不是饱和
//    while (!q.empty()) //树没有遍历完
//    {
//        TreeNode* p = q.front(); //层序思想
//        q.pop();
//        if (p->left) //只要左不是空
//        {
//            if (!saturate) return false; //在这之前已经不饱和，那么直接错
//            q.push(p->left); //如果是饱和的，把不为空的左 入队
//        }
//        else saturate = false; //左为空，设置为不饱和
//        if (p->right)// 同上
//        {
//            if (!saturate) return false;
//            q.push(p->right);
//        }
//        else saturate = false;
//    }
//    return true;
//}
//
//
//int height(TreeNode* root)
//{
//    if (!root) return 0;
//    return 1 + max(height(root->left), height(root->right));
//}
//bool IsBalanced_Solution(TreeNode* pRoot) {
//    if (!pRoot) return true;
//    return abs(height(pRoot->left) - height(pRoot->right)) <= 1 && IsBalanced_Solution(pRoot->left) && IsBalanced_Solution(pRoot->right);
//}