#define _CRT_SECURE_NO_WARNINGS
//#define _CRT_SECURE_NO_WARNINGS
//#include <iostream>
//#include <stdlib.h>
//using namespace std;
//char* multiply(char* num1, char* num2) {
//    int m = strlen(num1), n = strlen(num2);
//    char* ans = (char*)malloc(sizeof(char) * (m + n + 1));
//    memset(ans, 0, sizeof(char) * (m + n + 1));
//    if ((m == 1 && num1[0] == '0') || (n == 1 && num2[0] == '0')) {
//        ans[0] = '0';
//        return ans;
//    }//如果字符串有0，直接返回‘0’即可
//    int* ansArr = (int*)malloc(sizeof(int) * (m + n));//先创建一个int类型的数组把每一位的乘积都储存，然后再进行判断
//    memset(ansArr, 0, sizeof(int) * (m + n ));
//    for (int i = m - 1; i >= 0; i--) {
//        int x = num1[i] - '0';
//        for (int j = n - 1; j >= 0; j--) {
//            int y = num2[j] - '0';
//            ansArr[i + j + 1] += x * y;
//        }
//    }
//    //举一个例子 "12" * "8" 
//    //如果是int数组 结果就是 8 16
//    //思考一下和在最后结果“96”的差距 从最后一位开始是int%10，前一位+=int/10
//    for (int i = m + n - 1; i > 0; i--) {
//        ansArr[i - 1] += ansArr[i] / 10;
//        ansArr[i] %= 10;
//    }
//    //判断一下第一位是不是空的，如果是就从第二位开始遍历，防止096,0的出现
//    int index = ansArr[0] == 0 ? 1 : 0;
//    int ansLen = 0;
//    while (index < m + n) { //原封不动拷贝即可
//        ans[ansLen++] = ansArr[index];
//        index++;
//    }
//    //每个数字都要变成字符
//    for (int i = 0; i < ansLen; i++) ans[i] += '0';
//    return ans;
//}
//
//int  main()
//{
//    char a[] = "2";
//    char b[] = "3";
//    multiply(a, b);
//    return 0;
//}
//#include <iostream>
//#include <vector>
//using namespace std;
//void fac(int x, vector<int>& v) { //找因子
//    for (int i = 2; i <= x / i; i++) { //区间折半
//        if (x % i == 0) { //i本身就是x的因子
//            v.push_back(i);
//            if (x / i != i) //此时还要判断背被折去的区间还有没有因子，如果x/i=i，说明x=i*i，此时被折去区间的因子和i相同，不需要压入
//                v.push_back(x / i);
//        }
//    }
//}
//int step(int n, int m) {
//    vector<int> v(m + 1, -1); //数组用来记录到下标位置所需要的最小步数，一定是可以取到下标为m，所以初始化成m+1个
//    //初始化给值一定是越不可能越好，所以直接给-1，到时候如果不能跳到，直接返回更方便
//    v[n] = 0; //从下标为n出发，此时到达n需要0步
//    for (int i = n; i < m; i++) {
//        if (v[i] == -1) continue; //代表这个节点不可以走到
//        vector<int> factor; //记录i的因子
//        fac(i, factor); //获取因子到一个数组factor
//        for (int j = 0; j < factor.size(); j++) { //遍历因子
//            if (i + factor[j] <= m && v[i + factor[j]] != -1) //如果从i走因子步合法&&下标对应的格子可以取到
//                v[i + factor[j]] = v[i] + 1 < v[i + factor[j]] ? v[i] + 1 : v[i + factor[j]]; //选更小的作为走因子步的最小步数
//            else if (i + factor[j] <= m) //如果下标对应的格子不能取到
//                v[i + factor[j]] = v[i] + 1; //直接是前一步+1
//        }
//    }
//    return v[m]; //如果下标m走不到（=-1），直接返回-1没问题
//}
//int main() {
//    int n, m;
//    cin >> n >> m;
//    int ans = step(n, m);
//    cout << ans << endl;
//    return 0;
//}

//#include <iostream>
//#include <vector>
//#include <string>
//using namespace std;
//int main() {
//    string tmp = ""; //一个临时的字符串，用来存储每次分割的字符串
//    string s; //题给字符串
//    vector<string> v;
//    getline(cin, s); //一定要用getline，cin会在空格处停止写入
//    bool is = false; //判断是不是双引号
//    for (int i = 0; i < s.size(); i++) { 
//        if (s[i] == ' " ') //如果是双引号
//            is = !is; //把is变成true
//        else if (s[i] == ' ' && !is) { //如果遇到空格，并且不是双引号
//            v.push_back(tmp); //把分割的子串压入
//            tmp = ""; //tmp清空
//        }
//        else
//            tmp += s[i]; //正常情况，直接+=
//    }
//    v.push_back(tmp); //最后一个子串不是以空格结尾，需要手动添加
//    cout << v.size() << endl; //按照题给要求输出
//    for (auto e : v)
//        cout << e << endl;
//}
//#include <iostream>
//#include <vector>
//using namespace std;
//void fac(int x, vector<int>& v)
//{
//    for (int i = 2; i < x; i++)
//    {
//        if (x % i == 0)
//        {
//            v.push_back(i);
//        }
//    }
//}
//int main()
//{
//    vector<int> v;
//    int a=10;
//    fac(a, v);
//    return 0;
//}