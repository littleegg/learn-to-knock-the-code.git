#define _CRT_SECURE_NO_WARNINGS
//// non-recursive
//int BitAdd(int num1, int num2) {
//    int sum = 0, carry = 0;
//    while (num2 != 0) {
//        sum = num1 ^ num2;
//        carry = (num1 & num2) << 1;
//        num1 = sum;
//        num2 = carry;
//    }
//    return num1;
//}
//
//// recursive
//int BitAdd_1(int& num1, int& num2) {
//    if (num2 == 0)
//        return num1;
//    if (num1 == 0)
//        return num2;
//    int sum = num1 ^ num2;
//    int carry = (num1 & num2) << 1;
//    return BitAdd(sum, carry);
//}
//
//int main()
//{
//    int num1 = 99;
//    int num2 = 99;
//    cout << BitAdd(num1, num2) << endl;
//    cout << BitAdd_1(num1, num2) << endl;
//    return 0;
//}



//int dfs(int x, int y) {
//    if (x == 0 || y == 0 || x == n + 1 || y == m + 1) return 0;
//    if (x == 1 || y == 1) return 1;
//    return dfs(x - 1, y) + dfs(x, y - 1);
//
//}
//int main()
//{
//    int n, m;
//    scanf("%d%d", &n, &m);
//    ++n, ++m;
//    printf("%d\n", dfs(n, m));
//    return 0;
//}
