#define _CRT_SECURE_NO_WARNINGS

//vector<int> FindNumsAppearOnce(vector<int>& array) {
//    // write code here
//    map<int, int> m; //定义一个map遍历
//    for (auto e : array)
//        m[e]++; //把每个数字出现的次数统计一下
//    vector<int> ans; //存放最后唯一出现的两个元素
//    for (auto e : m)
//        if (e.second == 1) ans.push_back(e.first);
//    if (ans[0] > ans[1]) //题目要求返回必须升序
//        swap(ans[0], ans[1]);
//    return ans;
//}

//int minNumberDisappeared(vector<int>& nums) {
//    // write code here
//    int tmp = 1; //注意题目说的是最小的正整数，不要从0开始定义
//    sort(nums.begin(), nums.end()); //排序
//    for (auto e : nums) //遍历
//    {
//        if (e <= 0) continue; //不符合正整数规则
//        if (e == tmp) tmp++; //遇到相等元素，那下一个就应该是tmp++
//        else return tmp; //如果不是tmp说明tmp缺失
//    }
//    return tmp; //最后走到结束，缺失的就是mtp
//}
#include <iostream> 
#include <vector>
#include <map>
#include <utility>
#include <algorithm>
using namespace std;
//vector<vector<int> > threeSum(vector<int>& num) {
//    sort(num.begin(), num.end()); //首先排序
//    vector<vector<int>>ans; //ans记录答案三元组数组
//    map<int, vector<int> > m;//记录找到的两个数，用这两个数去寻找第三个数
//    map<pair<int, int>, int> mp; //为了去重
//    for (int i = 0; i < num.size(); i++)
//    {
//        m[num[i]].push_back(i); //首先把num记录下来，然后把对应的下标也记录
//    }
//    for (int i = 0; i < num.size(); i++)
//    {
//        for (int j = i + 1; j < num.size(); j++)
//        {
//            int sum = num[i] + num[j]; //sum为前两个数之和
//            if (m.count(-sum)) //-sum是第三个数,如果这个数存在
//            {
//                vector<int> tmp; //临时三元组，需要判断他是不是符合要求
//                tmp.push_back(num[i]);
//                tmp.push_back(num[j]);
//                tmp.push_back(-sum);
//                int a = (num[i] == -sum) + (num[j] == -sum); 
//                //a记录了第三个数和前两个数中的几个相等，a=0说明第三个数和前两个都不等，a=1和其中一个等，a=2三数相等
//                if (m[-sum].size() <= a) continue; //如果第三个数的下标的数目之和<=a，小于说明数组里没有第三个数
//                //比如a=1，假设第三个数=第一个数=10，m[-sum].size()就是记录10的个数，若m[-sum].size()<1说明数组里连10这个数都没有
//                //m[-sum].size()=1，说明数组里10这个数只有一个，就是第一个数，所以第三个数还是不存在
//                sort(tmp.begin(), tmp.end()); //此时第三个数存在，给tmp排成非降序
//                if (mp.count({ tmp[0],tmp[1] }) != 0) continue; //如果mp里面存在这样的三元组，说明重复
//                ans.push_back(tmp); //此时满足一个三元组的所有要求，加入到答案里
//                mp[{tmp[0], tmp[1]}] = tmp[2]; //用第三个数标记这个三元组
//            }
//        }
//    }
//    return ans;
//}

//
//int main()
//{
//    int b[] = { 0, 0, 0 };
//    vector<int> a;
//    for (auto e : b)
//    {
//        a.push_back(e);
//    }
//    threeSum(a);
//    return 0;
//}

//
//vector<vector<int> > permute(vector<int>& num) {
//    vector<vector<int>> ans;
//    vector<int> v;
//    recursion(num, v, ans);
//    return ans;
//}
//void recursion(vector<int> num, vector<int>& v, vector<vector<int>>& ans)
//{
//    if (num.size() == 1) //回溯
//    {
//        v.push_back(num[0]);
//        ans.push_back(v);
//        v.pop_back();
//        return;
//    }
//    for (auto e : num)
//    {
//        v.push_back(e);
//        vector<int> t;
//        for (auto i : num)
//            if (i != e) t.push_back(i);
//        recursion(t, v, ans);
//        v.pop_back();
//    }
//}

//
//vector<vector<int> > permuteUnique(vector<int>& num) {
//
//    set<vector<int>> ans;
//    vector<int> v;
//    recursion(num, v, ans);
//
//    return vector<vector<int>>(ans.begin(), ans.end());
//}
//void recursion(vector<int> num, vector<int>& v, set<vector<int>>& ans)
//{
//    if (num.size() == 1) //回溯
//    {
//        v.push_back(num[0]);
//        ans.insert(v);
//        v.pop_back();
//        return;
//    }
//    for (int i = 0; i < num.size(); i++)
//    {
//        v.push_back(num[i]);
//        vector<int> t;
//        for (int j = 0; j < num.size(); j++)
//            if (i != j)  t.push_back(num[j]);
//        recursion(t, v, ans);
//        v.pop_back();
//    }
//}

//#include <iostream>
//#include <vector>
//using namespace std;
//int is(vector<int> v, int n, int begin, int sum, int mul) //再次回来我还是这个sum和mul所以不需要引用
//{
//    int count = 0, i = begin;
//    for (; i < n; i++)
//    {
//        sum += v[i]; mul *= v[i];
//        if (i == 0 && sum == v[0]) is(v, n, i + 1, sum, mul);
//        else
//        {
//            if (sum > mul)
//                count += 1 + is(v, n, i + 1, sum, mul);
//
//            else break;
//            sum -= v[i]; //把现在的数字去掉，看看后面有没可能
//            mul /= v[i];
//            while (i < n - 1 && v[i] == v[i + 1]) //相同数字没必要纠结
//                i++;
//        }
//    }
//    return count;
//}
//int main()
//{
//    int n = 0; cin >> n;
//    vector<int> v;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> v[i];
//    }
//    cout << is(v, n, 0, 0, 1) << endl;
//    return 0;
//}

//#include <iostream>
//using namespace std;
//int main()
//{
//    int day[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//    int y, m, d;
//    cin >> y >> m >> d;
//    if (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))
//        day[2] = 29;
//    int ans = 0;
//    int i = m;
//    while (--i)
//        ans += day[i];
//    ans += d;
//    cout << ans << endl;
//    return 0;
//}