//#define _CRT_SECURE_NO_WARNINGS
//#include <iostream>
//#include <string> 
//#include <stdlib.h>
//#include <algorithm>
//#include <vector>
//using namespace std;
//int main() {
//    string s1, s2; cin >> s1 >> s2;
//
//    //      NULL  a b c d e
//   //  NULL       1 2 3 4 5
//   //  a     1
//   //  b     2
//   //  c     3
//   //  d     4
//    vector<vector<int>> dp(s1.size() + 1, vector<int>(s2.size() + 1));
//    for (int i = 0; i <= s1.size(); i++)
//        dp[i][0] = i;
//    for (int i = 0; i <= s2.size(); i++)
//        dp[0][i] = i;
//    for (int i = 1; i <= s1.size(); i++)
//    {
//        for (int j = 1; j <= s2.size(); j++)
//        {
//            if (s1[i - 1] == s2[j - 1]) dp[i][j] = dp[i - 1][j - 1];
//            else dp[i][j] = min({ dp[i - 1][j] + 1,dp[i][j - 1] + 1,dp[i - 1][j - 1] + 1 });
//        }
//    }
//    cout << dp[s1.size()][s2.size()] << endl;
//    return 0;
//}
//
//class Gift {
//public:
//    int getValue(vector<int> gifts, int n) {
//        // write code here
//        sort(gifts.begin(), gifts.end());
//        int ans = gifts[n / 2];
//        int num = 0;
//        for (auto e : gifts)
//        {
//            if (e == ans) num++;
//        }
//        return num <= n / 2 ? 0 : ans;
//    }
//};