#define _CRT_SECURE_NO_WARNINGS

//int Fibonacci(int n) {
//    int F[n + 1];
//    F[0] = 1;
//    F[1] = 1;
//    for (int i = 2; i < n; i++)
//    {
//        F[i] = F[i - 1] + F[i - 2];
//    }
//    return F[n - 1];
//}

//int jumpFloor(int number) {
//    int dp[number];
//    dp[0] = 1;
//    dp[1] = 2;
//    for (int i = 2; i < number; i++)
//    {
//        dp[i] = dp[i - 1] + dp[i - 2];
//    }
//    return dp[number - 1];
//}


//int minCostClimbingStairs(vector<int>& cost) {
//    // write code here
////状态：dp[i]:走到第i个台阶总共需要支付的费用
////状态方程：因为可以选择向上走一个，dp[i]=dp[i-1]+cost[i-1];
/////或者两个台阶:dp[i]=dp[i-2]+cost[i-2];
////最小花费，就是两种情况的最小值
//    vector<int> dp(cost.size() + 1);
//    dp[0] = 0; dp[1] = 0;
//    for (int i = 2; i <= cost.size(); i++)
//    {
//        dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
//    }
//    return dp[cost.size()];
//}

//int StrToInt(string str) {
//    int flag = 1;
//    int i = 0;
//    if (str[i] == '-' || str[0] == '+')
//    {
//        if (str[0] == '-')
//            flag = -1;
//        i++;
//    }
//
//    int ans = 0;
//    for (; i < str.size(); i++)
//    {
//
//        if (str[i] < '0' || str[i]>'9')
//            return 0;
//        ans = ans * 10 + str[i] - '0';
//    }
//    return flag * ans;
//}


//string LCS(string s1, string s2) {
//    // write code here
//
//    int n = s1.size();
//    int m = s2.size();
//    string ans;
//    int dp[n + 1][m + 1];
//    for (int j = 0; j <= m; j++)
//    {
//        dp[0][j] = 0;
//    }
//    for (int i = 0; i <= n; i++)
//    {
//        dp[i][0] = 0;
//    }
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            if (s1[i - 1] == s2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//            else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//        }
//    }
//    if (dp[n][m] == 0) return "-1";
//    int x = n;
//    int y = m;
//    while (x != 0 && y != 0) {
//        if (dp[x][y] == dp[x - 1][y]) {
//            x--;
//        }
//        else if (dp[x][y] == dp[x][y - 1]) {
//            y--;
//        }
//        else {
//            ans += s1[x - 1];
//            x--;
//            y--;
//        }
//    }
//    reverse(ans.begin(), ans.end());
//    return ans;
//
//}