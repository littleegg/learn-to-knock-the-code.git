#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
using namespace std;
//class Date
//{
//public:
//	Date()
//	{
//		cout << "Date()" << endl;
//		
//	}
//	~Date()
//	{
//		cout << "~Date()" << endl;
//	}
//};
//void Test()
//{
//	//Date d1(2022, 1, 13);
//	//d1.Print();
//	//const Date d2(2022, 1, 13);
//	////d2.Changeyear(2000);
//	//d2.Print();
//	Date* pclassa = new Date[5];
//
//	delete[] pclassa;
//}
//int main()
//{
//	Test();
//	return 0;
//}



//int globalVar = 1;
//
//static int staticGlobalVar = 1;
//
//int main()
//
//{
//
//	static int staticVar = 1;
//
//	int localVar = 1;
//
//	int num1[10] = { 1, 2, 3, 4 };
//
//	char char2[] = "abcd";
//
//	char* pChar3 = (const char *)"abcd";
//
//	int* ptr1 = (int*)malloc(sizeof(int) * 4);
//
//	int* ptr2 = (int*)calloc(4, sizeof(int));
//
//	int* ptr3 = (int*)realloc(ptr2, sizeof(int) * 4);
//
//	free(ptr1);
//
//	free(ptr3);
//
//}
//1. 选择题：
//选项 : A.栈 B.堆 C.数据段(静态区) D.代码段(常量区)
//globalVar在哪里？__c__  staticGlobalVar在哪里？__c__
//staticVar在哪里？__c__  localVar在哪里？__a__
//num1 在哪里？__a__
//char2在哪里？_a___ * char2在哪里？_a__
//pChar3在哪里？__a__ * pChar3在哪里？__d__
//ptr1在哪里？__a__ * ptr1在哪里？__b__
//
//2. 填空题：
//sizeof(num1) = _40___;
//sizeof(char2) = __5__;   strlen(char2) = _4___;
//sizeof(pChar3) = __4/8__;   strlen(pChar3) = _4___;
//sizeof(ptr1) = _4/8___;


//class A
//{
//public:
//	A(int a = 0)
//		: _a(a)
//	{
//		cout << "A():" << this << endl;
//	}
//	~A()
//	{
//		cout << "~A():" << this << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	// new/delete 和 malloc/free最大区别是 new/delete对于【自定义类型】除了开空间
//	//还会调用构造函数和析构函数
//	A* p1 = (A*)malloc(sizeof(A));
//	A* p2 = new A(1);
//	free(p1);
//	delete p2;
//	// 内置类型是几乎是一样的
//	int* p3 = (int*)malloc(sizeof(int)); // C
//	int* p4 = new int;
//	free(p3);
//	delete p4;
//	A* p5 = (A*)malloc(sizeof(A) * 10);
//	A* p6 = new A[10];
//	free(p5);
//	delete[] p6;
//	return 0;
//}

//#include <new>
//int main()
//{
//	const int N = 4;
//	char buffer[500];
//
//	int* p1 = new int[N] ; //正常的new
//	int* p2 = new(buffer) int[N]; //定位new，从buffer指针开始开辟内存
//	cout <<"buffer的地址：" << &buffer << endl;
//	for (int i = 0; i < N; i++)
//	{
//		cout << "p1[i]的地址：" << &p1[i] << endl;
//
//		cout << "p2[i]的地址：" << &p2[i] << endl;
//	}
//	delete[] p1;
//	cout << "====================" << endl;
//	int* p3 = new int[N]; //正常的new
//	int* p4 = new(buffer) int[N]; //定位new
//	cout << "buffer的地址：" << &buffer << endl;
//	for (int i = 0; i < N; i++)
//	{
//		cout << "p3[i]的地址：" << &p3[i] << endl;
//		cout << "p4[i]的地址：" << &p4[i] << endl;
//	}
//	cout << "====================" << endl;
//	int* p5 = new int[N]; //正常的new
//	int* p6 = new(buffer+N*sizeof(int )) int[N]; //定位new
//	cout << "buffer的地址：" << &buffer << endl;
//	for (int i = 0; i < N; i++)
//	{
//		cout << "p5[i]的地址：" << &p5[i] << endl;
//		cout << "p6[i]的地址：" << &p6[i] << endl;
//	}
//	return 0;
//}
#define _CRTDBG_MAP_ALLOC

//class Stack
//{
//public:
//	Stack()
//	{
//		cout << "Stack()" << endl;
//		_a = new int[4];
//		_top = 0;
//		_capacity = 4;
//	}
//
//	~Stack()
//	{
//		cout << "~Stack()" << endl;
//		delete[] _a;
//		_top = _capacity = 0;
//	}
//
//private:
//	int* _a;
//	int _top;
//	int _capacity;
//};
//int main()
//{
//	Stack st;
//	Stack* ptr = new Stack;
//	delete ptr;
//	return 0;
//}
//int main()
//{
//	//int* p = new int[10];
//	//free(p);
//	Stack* p = new Stack;
//	free(p);
//	_CrtDumpMemoryLeaks();
//	return 0;
//}

//
//int main(int argc, char* argv[]) {
//	int* var = new int(5);
//
//	_CrtDumpMemoryLeaks();
//
//	return 0;
//}
//int main()
//{
//	int* p = new int[10];
//	// 将该函数放在main函数之后，每次程序退出的时候就会检测是否存在内存泄漏
//	_CrtDumpMemoryLeaks();
//	return 0;
//}


//void Swap(int& left, int& right)
//{
//	int temp = left;
//	left = right;
//	right = temp;
//}
//void Swap(double& left, double& right)
//{
//	double temp = left;
//	left = right;
//	right = temp;
//}
//void Swap(char& left, char& right)
//{
//	char temp = left;
//	left = right;
//	right = temp;
//}

//template<class T,class  Y>
// T Add(T left, Y right)
//{
//	 return left + right;
//}
// int main()
// {
//	 Add(1, 2);
//	 int a=1, b = 2;
//	 double c = 1.1, d = 2.2;
//	 cout << Add(a, b) << endl;  //隐式实例化
//	 cout<<Add(a, (double)c)<<endl; //隐式实例化
//	 cout << Add<int>(a, d) << endl; //显示实例化
//	return 0;
// }
//template<class T>
//void Swap(T& X, T& Y)
//{
//	T tmp = X;
//	X = Y;
//	Y = tmp;
//}
//int main()
//{
//	int a = 1, b = 2;
//	Swap(a, b);
//	double c = 1.1, d = 2.2;
//	Swap(c, d);
//}

//template<class T>
//class Stack
//{
//public:
//	Stack(int capacity = 4)
//	{
//		_a = new T[capacity];
//	}
//private:
//	T* _a;
//	//..
//};
//int main()
//{
//	Stack<int>st1;
//	Stack<double>st2;
//	return 0;
//}