#define _CRT_SECURE_NO_WARNINGS
#include "head.h"

//二叉搜索树中的插入
//struct TreeNode* insertIntoBST(struct TreeNode* root, int val) {
//    if (root == NULL)
//    {
//        root = (struct TreeNode*)malloc(sizeof(struct TreeNode));
//        root->val = val;
//        root->left = NULL;
//        root->right = NULL;
//    }
//    if (root->val < val)
//    {
//        root->right = insertIntoBST(root->right, val);
//    }
//    else if (root->val > val)
//    {
//        root->left = insertIntoBST(root->left, val);
//    }
//    return root;
//}

//二叉搜索树中的搜索
//struct TreeNode* searchBST(struct TreeNode* root, int val) {
//    if (root == NULL)
//    {
//        return NULL;
//    }
//    else
//    {
//        if (root->val < val)
//        {
//            return searchBST(root->right, val);
//        }
//        else if (root->val > val)
//        {
//            return searchBST(root->left, val);
//        }
//        else
//        {
//            return root;
//        }
//    }
//}

typedef int type;
typedef  struct Stack
{
	type *a; //表示这一块连续地址的起始位置的指针
	int top; //表示栈顶元素的下标,如果有三个元素，那下标就是3,刚好数组中元素个数也是top
	int capacity; //容量

}ST;

//初始化
void Init(ST* q)
{
	q->a = NULL;
	q->capacity = 0;
	q->top = 0;
}
//增
void PushStack(ST* q,type x)
{
	assert(q);
	if (q->capacity == q->top   || q->capacity==0)
	{
       int size = q->capacity == 0 ? 4 : q->capacity * 2;
		ST*newnode =(ST*) realloc(q->a,sizeof(type)*size);
		if (newnode == NULL)
		{
			perror("77");
			exit(-1);
		}
       q->capacity = size;
	   q->a = newnode;
		
	}
	   q->a[q->top++] = x;
		
}
bool EmptyStack(ST* q)
{
	return q->top == 0;
}
//删
void PopStack(ST* q)
{
	assert(q);
	assert(!EmptyStack(q));
	q->top--;
}
//查,返回下标
int  SearchStack(ST* q,type val)
{
	assert(q);
	for (int i = 0; i < q->top; i++)
	{
		if (q->a[i] == val)
			return i;
	}
	return -1;
}
void PrintfStack(ST* q)
{
	assert(q);
	for (int i = q->top-1; i >=0; i--)  //因为最后top一定是落到最后一个下标的后面 ，因为每次Push都top++
	{
		printf("%d ", q->a[i]);
	}
	printf("\n");
}
//改,下标为a的元素改成val
void  ColectStack(ST* q,int a,type val)
{
	assert(q);
	q->a[a] = val;
}
//显示栈顶元素
type TopStack(ST* q)
{
	assert(q);
	return q->a[q->top-1];
}
//int main()
//{
//	ST st;
//	Init(&st);
//	PushStack(&st, 1);
//	PushStack(&st, 2);
//	PushStack(&st, 3);
//	PushStack(&st, 4);
//	PushStack(&st, 5);
//	PushStack(&st, 6);
//	PushStack(&st, 7);
//	PushStack(&st, 8);
//	PushStack(&st, 9);
//	PushStack(&st, 10);
//	PopStack(&st);
//	/*int ret=SearchStack(&st, 5);
//	printf("%d\n", ret);
//	ColectStack(&st, 0, 11);*/
//	int ret1=TopStack(&st);
//	printf("%d\n", ret1);
//	PrintfStack(&st);
//	
//}

//复习一下昨天写的排序代码
//快排的Hoare
//void Insertsort(int* a, int len)
//{
//	for (int i = 1; i < len; i++)
//	{
//    int end = i-1;
//	int tmp = a[end+1];
//	while(end>=0)
//	{
//		if (a[end] > tmp) //说明还需要后移
//		{
//			a[end+1] = a[end];
//			end--;
//		}
//		else
//		{
//			break;
//		}
//	}
//    a[end+1] = tmp;
//	}
//	
//	
//}
//void Swap(int* a, int *b)
//{
//	int tmp = *a;
//	*a = *b;
//	*b = tmp;
//}
//int Getmid(int* a, int begin, int end)
//{
//	int mid = (begin + end) / 2;
//	if (a[begin] < a[mid])
//	{
//		if (a[mid] < a[end])
//		{
//			return mid;
//		}
//		else
//		{
//			if (a[begin] < a[end])
//				return end;
//			else
//			{
//				return begin;
//			}
//		}
//	}
//	else
//	{
//		if (a[mid] > a[end])
//			return mid;
//		else
//		{
//			if (a[begin] > a[end])
//				return end;
//			else
//				return begin;
//		}
//	}
//}
//void QuickSortHoare(int* a,int begin,int end)
//{
//	if (begin >= end)
//		return;
//	//if (end - begin < 3) //因为这里的测试数据比较小，大数据可以改变3
//	//{
//	//	Insertsort(a,end-begin+1); //换成别的排序
//	//}
//	else
//	{
//		int left = begin,right=end,mid=Getmid(a,begin,end),key=left;
//		Swap(&a[mid], &a[begin]); //避免第一个元素是最值
//		while (left < right)
//		{
//			while (a[right] > a[key] && left < right)
//			{
//				right--;
//			}
//			while (a[left] < a[key] && left < right)
//			{
//				left--;
//			}
//			Swap(&a[right], &a[left]);
//		}
//		Swap(&a[left], &a[key]);
//		key = left;
//		QuickSortHoare(a, begin, key - 1);
//		QuickSortHoare(a, key, end);
//	}
//}
//void Print(int* a, int len)
//{
//	for (int i = 0; i < len; i++)
//	{
//		printf("%d ", a[i]);
//	}
//	printf("\n");
//}
//int main()
//{
//	int a[] = { 7,4,8,2,3,6 };
//	int len = sizeof(a) / sizeof(int);
//	//Insertsort(a, len);
//	QuickSortHoare(a, 0, len - 1);
//	Print(a, len);
//}