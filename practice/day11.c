#define _CRT_SECURE_NO_WARNINGS
#include "head.h"
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */


 /**
  * Note: The returned array must be malloced, assume caller calls free().
  */
  /*void preorder(struct TreeNode* root,int* res,int*resSize){
  if(root==NULL){
      return ;
  }
  res[(*resSize)++]=root->val;
  preorder(root->left,res,resSize);
  preorder(root->right,res,resSize);
  }
  int* preorderTraversal(struct TreeNode* root, int* returnSize)
  {
      int* res=(int*)malloc(sizeof(int)*200);
      *returnSize=0;
      preorder(root,res,returnSize);
      return res;
  }*/
//void preorder(struct TreeNode* root, int* res, int* resSize) {
//    if (root == NULL) {
//        return;
//    }
//    res[(*resSize)++] = root->val;
//    preorder(root->left, res, resSize);
//    preorder(root->right, res, resSize);
//}
//int* preorderTraversal(struct TreeNode* root, int* returnSize)
//{
//    int* res = (int*)malloc(sizeof(int) * 101);
//    *returnSize = 0;
//    preorder(root, res, returnSize);
//    return res;
//}
//
//void inorder(struct TreeNode* root, int* res, int* resSize) {
//    if (root == NULL) {
//        return;
//    }
//    inorder(root->left, res, resSize);
//    res[(*resSize)++] = root->val;
//    inorder(root->right, res, resSize);
//}
//int* inorderTraversal(struct TreeNode* root, int* returnSize) {
//    int* res = (int*)malloc(sizeof(int) * 101);
//    *returnSize = 0;
//    inorder(root, res, returnSize);
//    return res;
//}
//
//void postorder(struct TreeNode* root, int* res, int* resSize) {
//    if (root == NULL) {
//        return;
//    }
//    postorder(root->left, res, resSize);
//    postorder(root->right, res, resSize);
//    res[(*resSize)++] = root->val;
//}
//int* postorderTraversal(struct TreeNode* root, int* returnSize) {
//    int* res = (int*)malloc(sizeof(int) * 101);
//    *returnSize = 0;
//    postorder(root, res, returnSize);
//    return res;
//}

