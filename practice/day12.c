#define _CRT_SECURE_NO_WARNINGS
#include "head.h"
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */


 /**
  * Return an array of arrays of size *returnSize.
  * The sizes of the arrays are returned as *returnColumnSizes array.
  * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
  */
//typedef  struct TreeNode* Tree;
//typedef struct Queue
//{
//    Tree data;
//    struct Queue* next;
//}Link;
//
//typedef struct LinkNode1 {
//    Link* front, * rear;
//    int size;
//}LinkNode;
//
////初始化
//void Init(LinkNode* q)
//{
//    q->size = 0;
//    q->front = NULL;
//    q->rear = NULL;
//}
////判断是否为空
//bool Empty(LinkNode* q)
//{
//    return q->front == NULL && q->rear == NULL;
//}
////队列销毁
//void Destory(LinkNode* q)
//{
//    q->size = 0;
//    Link* cur = q->front;
//    while (cur)
//    {
//        Link* next = cur->next;
//        free(cur);
//        cur = cur->next;
//    }
//
//}
////入队
//void Push(LinkNode* q, struct TreeNode* x)
//{
//    Link* tmp = (Link*)malloc(sizeof(Link));
//    tmp->next = NULL;
//    tmp->data = x;
//    if (q->rear == NULL && q->front == NULL)
//    {
//        q->front = q->rear = tmp;
//    }
//    else
//    {
//        q->rear->next = tmp;
//        q->rear = q->rear->next;
//    }
//    q->size++;
//}
//
////出队
//void Pop(LinkNode* q)
//{
//    if (q->front->next == NULL)
//    {
//        free(q->front);
//        q->front = q->rear = NULL;
//    }
//    else {
//        Link* head = q->front;
//        Link* next = head->next;
//        free(head);
//        head = next;
//        q->front = next;
//    }
//    q->size--;
//}
////取队头
//struct TreeNode* Front(LinkNode* q)
//{
//    return q->front->data;
//}
//
//
//
//
//int** levelOrder(struct TreeNode* root, int* returnSize, int** returnColumnSizes) {
//    *returnSize = 0;
//    if (root == NULL)
//        return NULL;
//    struct TreeNode* queue[10000];
//    int** ans = (int**)malloc(sizeof(int*) * 10000), head = 0, tail = 0;
//    *returnColumnSizes = (int*)malloc(sizeof(int) * 10000);
//    queue[tail++] = root;
//    while (head != tail)//这个是大循环
//    {
//        int temp = tail, k = 0;
//        ans[*returnSize] = (int*)malloc(sizeof(int) * (tail - head));
//        while (head < temp)//为什么要用temp是因为tail的值是会在这个循环里变的,tail指向队尾节点,根据上面说的,
//        {//每次遍历完一个节点的左右两个子节点后都要把他们加入队列,则tail需要增大
//            struct TreeNode* p = queue[head++];
//            ans[*returnSize][k++] = p->val;
//            if (p->left)
//            {
//                queue[tail++] = p->left;
//            }
//            if (p->right)
//            {
//                queue[tail++] = p->right;
//            }
//        }
//        (*returnColumnSizes)[*returnSize] = k;
//        (*returnSize)++;
//    }
//    return ans;
//}
//
//int maxDepth(struct TreeNode* root) {
//    if (root == NULL)
//    {
//        return 0;
//    }
//
//    int left = maxDepth(root->left);
//    int right = maxDepth(root->right);
//    return 1 + fmax(left, right);
//}
//
///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     struct TreeNode *left;
// *     struct TreeNode *right;
// * };
// */
//
//bool _isSymmetric(struct TreeNode* left, struct TreeNode* right) {
//    if (left == NULL && right == NULL)
//    {
//        return true;
//    }
//    if (left == NULL || right == NULL)
//    {
//        return false;
//    }
//    if (left->val != right->val)
//    {
//        return false;
//    }
//
//    return _isSymmetric(left->left, right->right) && _isSymmetric(left->right, right->left);
//}
//
//bool isSymmetric(struct TreeNode* root) {
//    if (root == NULL)
//    {
//        return true;
//    }
//    return  _isSymmetric(root->left, root->right);
//}

//手撕队列
typedef int type;
typedef struct QueueNode
{
	type data;
	struct QueueNode* next;
}QNode;
typedef struct Queue
{
	QNode* head;
	QNode* tail;
	int size;
}Q;
//初始化
void InitQueue(Q* q)
{
	q->head = q->tail = NULL;
	q->size = 0;
}
//判断是否为空
bool Empty(Q* q)
{
	return q->head = NULL && q->tail == NULL;
}
//尾插
void QPush(Q* q, type x)
{
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("-1");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;

	if (q->tail == NULL)
	{
		q->head=q->tail = newnode;
	}
	else
	{
		q->tail->next = newnode;
		q->tail = q->tail->next;
	}
	q->size++;
}
//头删
void Pop(Q* q)
{
	assert(q);
	if (q->head->next == NULL)
	{
		q->head = q->tail = NULL;
	}
	else
	{
    QNode* cur = q->head;
	q->head = q->head->next;
	free(cur);
	cur = NULL;
	}
	q->size--;
}
//销毁
void Destory(Q* q)
{
	free(q->head);
	free(q->tail);
	q->tail=q->head = NULL;
	q->size = 0;
}
//取队头
type Front(Q* q)
{
	assert(q);
	return q->head->data;
}
//int main()
//{
//	Q q;
//	InitQueue(&q);
//	QPush(&q, 1);
//	QPush(&q, 2);
//	QPush(&q, 3);
//	QPush(&q, 4);
//	QPush(&q, 5);
//}