#define _CRT_SECURE_NO_WARNINGS
#include "head.h"

//有效的括号
//typedef int type;
//
//typedef struct Stack
//{
//	type* a;
//	int top;// 初始化成0 表示栈顶位置下一个位置的下标
//	int capacity;
//}ST;
//bool Empty(ST* p)//判断栈是不是空
//{
//	assert(p);
//	return p->top == 0;
//}
//void InitST(ST* p)
//{
//	type* tmp = (type*)malloc(sizeof(type) * 4);
//	if (tmp == NULL)
//	{
//		perror("InitST");
//		exit(-1);
//	}
//	p->a = tmp;
//	p->capacity = 4;
//	p->top = 0;
//}
//
//void PushST(ST* p, type x)//在栈顶压数据
//{
//	if (p->capacity == p->top)//表示需要扩容
//	{
//		type* tmp = (type*)realloc(p->a, p->capacity * 2 * sizeof(type));
//		if (tmp == NULL)
//		{
//			perror("realloc");
//			exit(-1);
//		}
//		p->a = tmp;
//		p->capacity *= 2;
//	}
//	p->a[p->top] = x;
//	p->top++; //因为top是栈顶元素下一个位置的下标
//}
//
//void PopST(ST* p)//从栈顶删除数据
//{
//	assert(p);
//	assert(!Empty(p));
//	p->top--;
//}
//
//void DestoryST(ST* p)//销毁栈
//{
//	assert(p);
//	free(p->a);
//	p->a = NULL;
//	p->capacity = p->top = 0;
//}
//
//type StackTop(ST* p)//显示栈顶的数据
//{
//	assert(p);
//	assert(!Empty(p));
//	return p->a[p->top - 1];
//}
//bool isValid(char* s) {
//    ST st;
//    InitST(&st);
//    while (*s)
//    {
//        if (*s == '(' || *s == '[' || *s == '{')
//        {
//            PushST(&st, *s);
//            ++s;
//        }
//        else
//        {
//            if (Empty(&st))
//            {
//                DestoryST(&st);
//                return false;
//
//            }
//            char top = StackTop(&st);
//            PopST(&st);
//            if ((*s == ')' && top != '(') || (*s == ']' && top != '[') || (*s == '}' && top != '{'))
//            {
//                DestoryST(&st);
//                return false;
//            }
//            else {
//                s++;
//            }
//
//        }
//
//    }
//    bool ret = Empty(&st);
//    DestoryST(&st);
//    return ret;
//}


//typedef struct {
//    ST push;
//    ST pop;
//} MyQueue;
//bool myQueueEmpty(MyQueue* obj) {
//    return Empty(&obj->push) && Empty(&obj->pop);
//}
//
//
//
//MyQueue* myQueueCreate() {
//    MyQueue* q = (MyQueue*)malloc(sizeof(MyQueue));
//    InitST(&q->push);
//    InitST(&q->pop);
//    return q;
//}
//
//void myQueuePush(MyQueue* obj, int x) {
//    assert(obj);
//    PushST(&obj->push, x);
//}
//
//int myQueuePop(MyQueue* obj) {
//    assert(obj);
//    assert(!myQueueEmpty(obj));
//    int peek = myQueuePeek(obj);
//    PopST(&obj->pop);
//    return peek;
//}
//
//int myQueuePeek(MyQueue* obj) {
//    assert(obj);
//
//    assert(!myQueueEmpty(obj));
//
//    if (Empty(&obj->pop))
//    {
//        while (!Empty(&obj->push))
//        {
//            PushST(&obj->pop, StackTop(&obj->push));
//            PopST(&obj->push);
//        }
//    }
//    return StackTop(&obj->pop);
//}
//
//
//void myQueueFree(MyQueue* obj) {
//    DestoryST(&obj->pop);
//    DestoryST(&obj->push);
//    free(obj);
//}

//动态数字和
//int* runningSum(int* nums, int numsSize, int* returnSize) {
//    int count = 0;
//    int* tmp = (int*)malloc(sizeof(int) * numsSize);
//
//    *returnSize = numsSize;
//    for (int i = 0; i < numsSize; i++)
//    {
//        count += nums[i];
//        tmp[i] = count;
//    }
//    return tmp;
//}

//int searchInsert(int* nums, int numsSize, int target) {
//    int left = 0;
//    int right = numsSize - 1;
//    int ans = numsSize;
//    while (left <= right)
//    {
//        int mid = ((right - left) >> 1) + left;
//        if (target <= nums[mid]) //在左区间
//        {
//            ans = mid;
//            right = mid - 1;
//        }
//        else//说明在右区间
//        {
//            left = mid + 1;
//        }
//
//    }
//    return ans;
//}
