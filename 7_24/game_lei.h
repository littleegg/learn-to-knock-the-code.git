#define _CRT_SECURE_NO_WARNINGS
#define ROW 9
#define COL 9
#define ROWS  ROW +2
#define COLS COL+2
#define EAST_COUNT 10
#include <stdio.h>
#include<time.h>
#include<string.h>
//#include<ctime>
//#include<cstdlib>
//#include<iostream>
void Initarr(char arr[ROWS][COLS], int rows, int cols,char set);
void Printf(char arr[ROWS][COLS],int row, int col);
void Setlei(char arr1[ROWS][COLS], int row, int col);
void Play(char arr1[ROWS][COLS], char arr2[ROWS][COLS],int row, int col);
int Count(char arr1[ROWS][COLS], int x, int y);
void Power(char arr1[ROWS][COLS], char arr2[ROWS][COLS], int x, int y, int row, int col);


