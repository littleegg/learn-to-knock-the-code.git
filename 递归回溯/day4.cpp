//#define _CRT_SECURE_NO_WARNINGS
//
////单词搜索
//class Solution {
//public:
//    bool vis[7][7];
//    int m;
//    int n;
//    bool exist(vector<vector<char>>& board, string word) {
//        m = board.size(), n = board[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (board[i][j] == word[0])
//                {
//                    vis[i][j] = true;
//                    if (dfs(board, word, i, j, 1)) return true;
//                    vis[i][j] = false;
//                }
//            }
//        }
//        return false;
//    }
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    bool dfs(vector<vector<char>>& board, string word, int i, int j, int pos)
//    {
//        if (pos == word.size()) return true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && board[x][y] == word[pos])
//            {
//                vis[x][y] = true;
//                if (dfs(board, word, x, y, pos + 1)) return true;
//                vis[x][y] = false;
//
//            }
//        }
//        return false;
//    }
//};
//
////黄金矿工
//class Solution {
//public:
//    bool vis[15][15];
//    int ans;
//    int m, n;
//    int path;
//    int getMaximumGold(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] != 0)
//                {
//                    vis[i][j] = true;
//                    path += grid[i][j];
//                    dfs(grid, i, j);
//                    path -= grid[i][j];
//                    vis[i][j] = false;
//                }
//            }
//        }
//        return ans;
//    }
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    void dfs(vector<vector<int>>& grid, int i, int j)
//    {
//        ans = max(ans, path);
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y])
//            {
//                vis[x][y] = true;
//                path += grid[x][y];
//                dfs(grid, x, y);
//                path -= grid[x][y];
//                vis[x][y] = false;
//            }
//        }
//    }
//};
//
//
////不同路径 III
//class Solution {
//public:
//    bool vis[21][21];
//    int m, n;
//    int ans, step;
//    int uniquePathsIII(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        int inx, iny;
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == 0) step++;
//                else if (grid[i][j] == 1)
//                {
//                    inx = i, iny = j;
//                }
//            }
//        }
//        step += 2;
//        vis[inx][iny] = true;
//        dfs(grid, inx, iny, 1);
//        return ans;
//    }
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    void dfs(vector<vector<int>>& grid, int i, int j, int count)
//    {
//        if (grid[i][j] == 2)
//        {
//            if (count == step)
//                ans++;
//            return;
//        }
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y] != -1)
//            {
//                vis[x][y] = true;
//                dfs(grid, x, y, count + 1);
//                vis[x][y] = false;
//            }
//        }
//    }
//};
//
////图像渲染
//class Solution {
//public:
//    //最好的方法就是直接在原来的数组上修改
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int m, n;
//    int cen;
//
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        if (image[sr][sc] == color) return image;
//        m = image.size(), n = image[0].size();
//        cen = image[sr][sc];
//        dfs(image, sr, sc, color);
//        return image;
//    }
//    void dfs(vector<vector<int>>& image, int sr, int sc, int color)
//    {
//        image[sr][sc] = color;
//        for (int k = 0; k < 4; k++)
//        {
//            //先找中心位置的四个方向
//            int x = sr + dx[k], y = sc + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == cen)
//            {
//                dfs(image, x, y, color);
//            }
//        }
//    }
//};
//
////岛屿数量
//class Solution {
//public:
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int m, n;
//    int ans;
//    bool vis[300][300];
//    int numIslands(vector<vector<char>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == '1' && !vis[i][j])
//                {
//                    ans++;
//                    dfs(grid, i, j);
//                }
//            }
//        }
//        return ans;
//    }
//    void dfs(vector<vector<char>>& grid, int i, int j)
//    {
//        vis[i][j] = true;
//
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y] == '1')
//            {
//                dfs(grid, x, y);
//            }
//        }
//    }
//};
//
////最大岛屿面积
//class Solution {
//public:
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int m, n;
//    int ans;
//    int count = 0;
//    bool vis[51][51];
//    int maxAreaOfIsland(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == 1 && !vis[i][j])
//                {
//                    vis[i][j] = true;
//                    count = 0;
//                    dfs(grid, i, j);
//                    ans = max(ans, count);
//                }
//            }
//        }
//        return ans;
//    }
//    void dfs(vector<vector<int>>& grid, int i, int j)
//    {
//        count++;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y] == 1)
//            {
//                vis[x][y] = true;
//                dfs(grid, x, y);
//
//            }
//        }
//    }
//};
//
////被围绕的面积
//class Solution {
//public:
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int m, n;
//    void solve(vector<vector<char>>& board) {
//        m = board.size(), n = board[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            if (board[i][0] == 'O') dfs(board, i, 0);
//            if (board[i][n - 1] == 'O') dfs(board, i, n - 1);
//        }
//        for (int j = 0; j < n; j++)
//        {
//            if (board[0][j] == 'O')
//                dfs(board, 0, j);
//            if (board[m - 1][j] == 'O')
//                dfs(board, m - 1, j);
//        }
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//            {
//                if (board[i][j] == '.') board[i][j] = 'O';
//                else if (board[i][j] == 'O') board[i][j] = 'X';
//            }
//    }
//    void dfs(vector<vector<char>>& board, int i, int j)
//    {
//        board[i][j] = '.';
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'O')
//                dfs(board, x, y);
//        }
//    }
//};
//
//

//
//#include<iostream>
//#include <list>
//#include <vector>
//using namespace std;
//
// struct ListNode {
//int val;
//struct ListNode *next;	
//ListNode(int x) : val(x), next(nullptr) {}
//};
// 
//ListNode* mergeKLists(vector<ListNode*>& lists) {
//    // write code here
//    ListNode* head = new ListNode(0);
//    ListNode* cur = head;
//    ListNode* l1 = lists[0];
//    ListNode* l2 = lists[1];
//    while (l1!= nullptr && l2 != nullptr)
//    {
//        if (l1->val < l2->val) {
//            cur->next = l1;
//            l1 = l1->next;
//        }
//        else {
//            cur->next= l2;
//            l2 = l2->next;
//        }
//        //cur->next= nullptr;
//        cur = cur->next;
//    }
//    while (l1) {
//        cur->next = l1;
//        l1 = l1->next;
//    }
//    while (l2) {
//        cur->next = l2;
//        l2 = l2->next;
//    }
//   
//    return head->next;
//}
//
//
//int main()
//{
//   /* list<int> l1(1);
//    l1.push_back(2);
//    l1.push_back(3);
//    list<int> l2(2);
//    l2.push_back(4);
//    l2.push_back(5);*/
//    ListNode* l1 = new ListNode(1);
//    ListNode* l2 = new ListNode(2);
//    ListNode* l3 = new ListNode(3);
//    
//    ListNode* l5 = new ListNode(5);
//    ListNode* l4 = new ListNode(4);
//    l1->next = l2;
//    l2->next = l3;
//    l4->next = l5;
//    vector<ListNode* > v = { l1 , l4 };
//    mergeKLists(v);
//}