////#define _CRT_SECURE_NO_WARNINGS
////
//////汉诺塔
////class Solution {
////public:
////    void hanota(vector<int>& A, vector<int>& B, vector<int>& C) {
////        dfs(A, B, C, A.size());
////    }
////    void dfs(vector<int>& A, vector<int>& B, vector<int>& C, int n)
////    {
////        if (n == 1)
////        {
////            C.push_back(A.back());
////            A.pop_back();
////            return;
////        }
////        dfs(A, C, B, n - 1);
////        C.push_back(A.back());
////        A.pop_back();
////        dfs(B, A, C, n - 1);
////    }
////};
////
//////合并有序链表
/////**
//// * Definition for singly-linked list.
//// * struct ListNode {
//// *     int val;
//// *     ListNode *next;
//// *     ListNode() : val(0), next(nullptr) {}
//// *     ListNode(int x) : val(x), next(nullptr) {}
//// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
//// * };
//// */
////class Solution {
////public:
////    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
////        if (!list1) return list2;
////        if (!list2) return list1;
////        if (list1->val > list2->val)
////        {
////            list2->next = mergeTwoLists(list1, list2->next);
////            return list2;
////        }
////        else
////        {
////            list1->next = mergeTwoLists(list1->next, list2);
////            return list1;
////        }
////
////    }
////};
////
////
//////翻转链表
/////**
//// * Definition for singly-linked list.
//// * struct ListNode {
//// *     int val;
//// *     ListNode *next;
//// *     ListNode() : val(0), next(nullptr) {}
//// *     ListNode(int x) : val(x), next(nullptr) {}
//// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
//// * };
//// */
////class Solution {
////public:
////    ListNode* reverseList(ListNode* head) {
////        if (!head || head->next == nullptr) return head;
////        ListNode* newhead = reverseList(head->next);
////        head->next->next = head;
////        head->next = nullptr;
////        return newhead;
////    }
////};
////
//////两两反转链表
/////**
//// * Definition for singly-linked list.
//// * struct ListNode {
//// *     int val;
//// *     ListNode *next;
//// *     ListNode() : val(0), next(nullptr) {}
//// *     ListNode(int x) : val(x), next(nullptr) {}
//// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
//// * };
//// */
////class Solution {
////public:
////    ListNode* swapPairs(ListNode* head) {
////        if (!head || !head->next) return head;
////        ListNode* newhead = swapPairs(head->next->next);
////        ListNode* tmp = head->next;
////        tmp->next = head;
////        head->next = newhead;
////        return tmp;
////    }
////};
////
//////快速幂     在32位和64位机器中，int占32位，取值范围为-2147483648～2147483647（-2^{31} ， 2^{31}-1）。
////lass Solution{
////public:
////    double myPow(double x, int n) {
////       return n < 1 ? 1.0 / pow(x,-(long long)n) : pow(x,(long long)n);
////    }
////    double pow(double x, long long n)
////    {
////      if (n == 0) return 1.0;
////      double tmp = pow(x,n / 2);
////      return n % 2 == 0 ? tmp * tmp : tmp * tmp * x;
////    }
////};
////
//////计算布尔二叉树的值 
/////**
//// * Definition for a binary tree node.
//// * struct TreeNode {
//// *     int val;
//// *     TreeNode *left;
//// *     TreeNode *right;
//// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
//// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
//// * };
//// */
////class Solution {
////public:
////    bool evaluateTree(TreeNode* root) {
////        if (!root->left) return root->val;
////        bool left = evaluateTree(root->left);
////        bool right = evaluateTree(root->right);
////        return root->val == 2 ? left | right : left & right;
////    }
////};
////
////
////
//////求根节点到叶节点之和
/////**
//// * Definition for a binary tree node.
//// * struct TreeNode {
//// *     int val;
//// *     TreeNode *left;
//// *     TreeNode *right;
//// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
//// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
//// * };
//// */
////class Solution {
////public:
////    int sumNumbers(TreeNode* root) {
////        return dfs(root, 0);
////    }
////    int dfs(TreeNode* root, int presum)
////    {
////        presum = presum * 10 + root->val;
////        if (!root->left && !root->right)
////            return presum;
////        int tmp = 0;
////        if (root->left)
////            tmp += dfs(root->left, presum);
////        if (root->right)
////            tmp += dfs(root->right, presum);
////        return tmp;
////    }
////};
////
////
//////二叉树剪枝
/////**
//// * Definition for a binary tree node.
//// * struct TreeNode {
//// *     int val;
//// *     TreeNode *left;
//// *     TreeNode *right;
//// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
//// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
//// * };
//// */
////class Solution {
////public:
////    TreeNode* pruneTree(TreeNode* root) {
////        if (!root) return nullptr;
////        root->left = pruneTree(root->left);
////        root->right = pruneTree(root->right);
////        if (root->left == nullptr && root->right == nullptr && root->val == 0)
////        {
////            delete root;
////            return nullptr;
////        }
////        return root;
////    }
////};
////
//////二叉搜索树的第k小的数字
//////非递归
////class Solution {
////public:
////    int max = INT_MAX;
////    int kthSmallest(TreeNode* root, int k) {
////        stack<TreeNode*> s;
////        int ans = 0;
////        TreeNode* cur = root;
////        while (!s.empty() || cur)
////        {
////            if (cur)
////            {
////                s.push(cur);
////                cur = cur->left;
////            }
////            else
////            {
////                cur = s.top();
////                s.pop();
////                ans++;
////                if (ans == k) return cur->val;
////                cur = cur->right;
////            }
////        }
////        return 0;
////    }
////
////};
//////递归
/////**
//// * Definition for a binary tree node.
//// * struct TreeNode {
//// *     int val;
//// *     TreeNode *left;
//// *     TreeNode *right;
//// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
//// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
//// * };
//// */
////class Solution {
////public:
////    int count;
////    int ans;
////    int kthSmallest(TreeNode* root, int k) {
////        count = k;
////        dfs(root);
////        return ans;
////    }
////    void dfs(TreeNode* root)
////    {
////        if (root == nullptr || count == 0) return;
////        dfs(root->left);
////        count--;
////        if (count == 0)
////            ans = root->val;
////        dfs(root->right);
////    }
////};
//
//
////二叉树的所有路径
///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
// //前序+全局变量+恢复现场
//class Solution {
//public:
//    vector<string> ret;
//    vector<string> binaryTreePaths(TreeNode* root) {
//        string path = "";
//        if (!root) return ret;
//        dfs(root, path);
//
//        return ret;
//    }
//    void dfs(TreeNode* root, string path)
//    {
//        path += to_string(root->val);
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            //叶子
//            ret.push_back(path);
//            return;
//        }
//        path += "->";
//        if (root->left) dfs(root->left, path);
//        if (root->right) dfs(root->right, path);
//    }
//};
//
////全排列
//class Solution {
//public:
//    vector<vector<int>> vv;
//    vector<int> path;
//    bool check[7];
//    vector<vector<int>> permute(vector<int>& nums) {
//        dfs(nums);
//        return vv;
//    }
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == nums.size())
//        {
//            vv.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (!check[i])
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                dfs(nums);
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};
//
//
////子集
////解法一
//lass Solution{
//public:
//vector<vector<int>> vv;
//vector<int> path;
//    vector<vector<int>> subsets(vector<int>& nums) {
//        dfs(nums,0);
//        return vv;
//    }
//    void dfs(vector<int>& nums,int pos)
//    {
//        vv.push_back(path);
//        for (int i = pos; i < nums.size(); i++)
//        {
//            path.push_back(nums[i]);
//            dfs(nums,i + 1);
//            path.pop_back();
//        }
//    }
//};
//
////解法二
//class Solution {
//public:
//    vector<vector<int>> vv;
//    vector<int> path;
//    vector<vector<int>> subsets(vector<int>& nums) {
//        dfs(nums, 0);
//        return vv;
//    }
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos == nums.size())
//        {
//            vv.push_back(path);
//            return;
//        }
//        //选
//        path.push_back(nums[pos]);
//        dfs(nums, pos + 1);
//        path.pop_back();
//
//        //buxuan 
//        dfs(nums, pos + 1);
//
//    }
//};
//
////找出所有子集的异或和再求和
//class Solution {
//public:
//    int path;
//    int sum;
//    int subsetXORSum(vector<int>& nums) {
//        dfs(nums, 0);
//        return sum;
//    }
//    void dfs(vector<int>& nums, int pos)
//    {
//        sum += path;
//        for (int i = pos; i < nums.size(); i++)
//        {
//            path ^= nums[i];
//            dfs(nums, i + 1);
//            path ^= nums[i];
//        }
//    }
//};
//
////全排列Ⅱ
//class Solution {
//public:
//    vector<vector<int>> vv;
//    vector<int> path;
//    bool check[9];
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        dfs(nums, 0);
//        return vv;
//    }
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (path.size() == nums.size())
//        {
//            vv.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false && (i == 0 || (nums[i] != nums[i - 1]) || check[i - 1] == true))
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                dfs(nums, i + 1);
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};
//
//
// 
// 
// 
////不定参的参数包类型
//#include <iostream>
//
//void xprintf()
//{
//	std::cout << std::endl;
//}
//
//template<class T,class ...Args>
//void xprintf(const T& v, Args && ... args)
//{
//	std::cout << v<<" ";
//	if (sizeof ...(args) > 0)
//	{
//		xprintf(std::forward<Args>(args)...);
//	}
//	else
//	{
//		xprintf();
//	}
//}
//
//int main()
//{
//	xprintf("nihao", 666);
//	return 0;
//}