//#define _CRT_SECURE_NO_WARNINGS
//
////电话号码的组合
//class Solution {
//public:
//    string path;
//    string a[10] = { "","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz" };
//    vector<string> v;
//    vector<string> letterCombinations(string digits) {
//        if (digits.size() == 0) return v;
//        dfs(digits, 0);
//        return v;
//    }
//    void dfs(string digits, int pos)
//    {
//        if (pos == digits.size()) //pos代表选到第几个数字了 全部数字选完就直接返回
//        {
//            v.push_back(path);
//            return;
//        }
//        for (auto& e : a[digits[pos] - '0']) //首先定位到数字对应 的字符串 
//        {
//            path.push_back(e);//先选一个数字
//            dfs(digits, pos + 1);//在下一个数字中选取
//            path.pop_back();
//        }
//    }
//
//};
//
////括号生成
//class Solution {
//public:
//    string path;
//    vector<string> v;
//    vector<string> generateParenthesis(int n) {
//        dfs(0, 0, n);
//        return v;
//    }
//    void dfs(int left, int right, int n)
//    {
//        if (left < n)
//        {
//            path.push_back('(');
//            dfs(left + 1, right, n);
//            path.pop_back();
//        }
//        if (right < left)
//        {
//            path.push_back(')');
//            dfs(left, right + 1, n);
//            path.pop_back();
//        }
//        if (left == n && right == n)
//        {
//            v.push_back(path);
//            return;
//        }
//    }
//};
////或者使用更多的全局变量
//class Solution {
//public:
//    string path;
//    vector<string> v;
//    int left;
//    int right;
//    int n;
//    vector<string> generateParenthesis(int n1) {
//        n = n1;
//        dfs();
//        return v;
//    }
//    void dfs()
//    {
//        if (left < n)
//        {
//            path.push_back('('); left++;
//            dfs();
//            path.pop_back(); left--;
//        }
//        if (right < left)
//        {
//            path.push_back(')'); right++;
//            dfs();
//            path.pop_back(); right--;
//        }
//        if (left == n && right == n)
//        {
//            v.push_back(path);
//            return;
//        }
//    }
//};
//
//
////组合
//class Solution {
//public:
//    vector<int> path;
//    vector<vector<int>> vv;
//    int n;
//    int k;
//    vector<vector<int>> combine(int n1, int k1) {
//        n = n1; k = k1;
//        dfs(1);
//        return vv;
//    }
//    void dfs(int pos)
//    {
//        if (path.size() == k)
//        {
//            vv.push_back(path);
//            return;
//        }
//        for (int i = pos; i <= n; i++)
//        {
//            path.push_back(i);
//            dfs(i + 1);
//            path.pop_back();
//        }
//    }
//};
////第二中方法 两个pos的含义不一样
//class Solution {
//public:
//    vector<int> path;
//    vector<vector<int>> vv;
//    int n;
//    int k;
//    vector<vector<int>> combine(int n1, int k1) {
//        n = n1; k = k1;
//        dfs(1);
//        return vv;
//    }
//    void dfs(int pos)
//    {
//        if (path.size() == k)
//        {
//            vv.push_back(path);
//            return;
//        }
//        for (int i = pos; i <= n; i++)
//        {
//            path.push_back(i);
//            dfs(i + 1);
//            path.pop_back();
//        }
//    }
//};
//
//
////目标和
//class Solution {
//public:
//    int ans;
//    int target;
//    int findTargetSumWays(vector<int>& nums, int target1) {
//        target = target1;
//        dfs(nums, 0, 0);
//        return ans;
//    }
//    void dfs(vector<int>& nums, int pos, int path)
//    {
//        if (pos == nums.size())
//        {
//            if (path == target)
//                ans++;
//            return;
//        }
//        dfs(nums, pos + 1, path + nums[pos]);
//        dfs(nums, pos + 1, path - nums[pos]);
//    }
//};
//
//
////组合总和
////pos代表的是你取出来的是下标位置
//class Solution {
//public:
//    vector<vector<int>> vv;
//    vector<int> path;
//    int target;
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target1) {
//        target = target1;
//        dfs(candidates, 0, 0);
//        return vv;
//    }
//    void dfs(vector<int>& candidates, int pos, int sum)
//    {
//        if (sum == target)
//        {
//            vv.push_back(path);
//            return;
//        }
//        if (pos == candidates.size() || sum > target) return;
//        for (int i = pos; i < candidates.size(); i++)
//        {
//            path.push_back(candidates[i]);
//            dfs(candidates, i, sum + candidates[i]);
//            path.pop_back();
//        }
//    }
//};
//
////根据添加每个元素的个数来选
//class Solution {
//public:
//    int target;
//    vector<vector<int>> vv;
//    vector<int> path;
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target1) {
//        target = target1;
//        dfs(candidates, 0, 0);
//        return vv;
//    }
//    void dfs(vector<int>& candidates, int pos, int sum)
//    {
//        if (sum == target)
//        {
//            vv.push_back(path);
//            return;
//        }
//        if (sum > target || pos == candidates.size()) return;
//        for (int i = 0; i * candidates[pos] + sum <= target; i++)
//        {
//            if (i) path.push_back(candidates[pos]);
//            dfs(candidates, pos + 1, sum + i * candidates[pos]);
//        }
//        for (int i = 1; i * candidates[pos] + sum <= target; i++)
//        {
//            path.pop_back();
//        }
//    }
//};
//
////字母大小写全排列
//class Solution {
//public:
//    string path;
//    vector<string> v;
//    vector<string> letterCasePermutation(string s) {
//        if (s.empty()) return v;
//        dfs(s, 0);
//        return v;
//    }
//    void dfs(const string& s, int pos)
//    {
//        if (pos == s.size())
//        {
//            v.push_back(path);
//            return;
//        }
//        char a = s[pos];
//        path.push_back(a);
//        dfs(s, pos + 1);
//        path.pop_back();
//        if (a < '0' || a > '9')
//        {
//            if (islower(a))
//                a = toupper(a);
//            else a = tolower(a);
//            path.push_back(a);
//            dfs(s, pos + 1);
//            path.pop_back();
//        }
//    }
//};
//
////优美的排列
//class Solution {
//public:
//    int ans;
//    bool check[16]; //为了检测是否重复使用
//    int countArrangement(int n) {
//        dfs(n, 1);
//        return ans;
//    }
//    void dfs(int n, int pos)
//    {
//        if (pos == n + 1)
//        {
//            ans++;
//            return;
//        }
//        for (int i = 1; i <= n; i++)
//        {
//            if (check[i] == false && ((pos % i == 0) || (i % pos == 0)))
//            {
//                check[i] = true;
//                dfs(n, pos + 1);
//                check[i] = false;
//            }
//        }
//    }
//};
//
//
////n皇后
//class Solution {
//public:
//    vector<vector<string>> vv;
//    vector<string> path;
//    //为了检查 列 斜 反斜三个位置上是不是被放过——和判断数字是不是被用过一样
//    //行不需要 在对每一行操作的时候已经满足 一行只放一个
//    bool checkdig1[20];
//    bool checkcol[10];
//    bool checkdig2[20];
//
//    int n;
//    vector<vector<string>> solveNQueens(int n1) {
//        n = n1;
//        path.resize(n);
//        for (int i = 0; i < n; i++)
//        {
//            path[i].append(n, '.');
//        }
//        dfs(0);
//        return vv;
//    }
//    void dfs(int row)
//    {
//        if (row == n)
//        {
//            vv.push_back(path);
//            return;
//        }
//        for (int i = 0; i < n; i++)
//        {
//            if (checkcol[i] == false && checkdig1[row - i + n] == false && checkdig2[i + row] == false)
//            {
//                path[row][i] = 'Q';
//                checkcol[i] = checkdig1[row - i + n] = checkdig2[i + row] = true;
//
//                dfs(row + 1);
//                path[row][i] = '.';
//                checkcol[i] = checkdig1[row - i + n] = checkdig2[i + row] = false;
//
//            }
//        }
//    }
//};
//
////有效的数独
//class Solution {
//public:
//    bool row[9][10];
//    bool col[9][10];
//    bool sboard[3][3][10];
//    bool isValidSudoku(vector<vector<char>>& board) {
//        for (int i = 0; i < 9; i++)
//        {
//            for (int j = 0; j < 9; j++)
//            {
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    if (row[i][num] || col[j][num] || sboard[i / 3][j / 3][num])
//                        return false;
//                    row[i][num] = col[j][num] = sboard[i / 3][j / 3][num] = true;
//                }
//
//            }
//        }
//        return true;
//    }
//};
//
////解数独
//class Solution {
//public:
//    bool row[9][10];
//    bool col[9][10];
//    bool sboard[3][3][10];
//    void solveSudoku(vector<vector<char>>& board) {
//        for (int i = 0; i < 9; i++)
//        {
//            for (int j = 0; j < 9; j++)
//            {
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    row[i][num] = col[j][num] = sboard[i / 3][j / 3][num] = true;
//                }
//
//            }
//        }
//        dfs(board);
//    }
//    bool dfs(vector<vector<char>>& board)
//    {
//        for (int i = 0; i < 9; i++)
//        {
//            for (int j = 0; j < 9; j++)
//            {
//                if (board[i][j] == '.')
//                {
//                    for (int num = 1; num <= 9; num++)
//                    {
//                        if (!row[i][num] && !col[j][num] && !sboard[i / 3][j / 3][num])
//                        {
//                            board[i][j] = num + '0';
//                            row[i][num] = col[j][num] = sboard[i / 3][j / 3][num] = true;
//                            if (dfs(board)) return true;
//                            board[i][j] = '.';
//                            row[i][num] = col[j][num] = sboard[i / 3][j / 3][num] = false;
//                        }
//
//                    }
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
//};
//

//C++11
//#include <iostream>
//#include <memory>
//using namespace std;
//
////水果工厂
//class Fruit
//{
//public:
//	virtual void name() = 0;//纯虚函数
//};
//class Apple:public Fruit 
//{
//public:
//	void name() override
//	{
//		cout << "苹果" << endl;
//	}
//};
//class Banana:public Fruit
//{
//public:
//	void name() override
//	{
//		cout << "香蕉" << endl;
//	}
//};
//
//class FruitFactory
//{
//public:
//	static shared_ptr<Fruit> create(const string& name)
//	{
//		if (name == "苹果")
//		{
//			return make_shared<Apple>();
//		}
//		else
//		{
//			return make_shared<Banana>();
//		}
//	}
//};
//int main()
//{
//	shared_ptr<Fruit> fruit = FruitFactory::create("苹果");
//	fruit->name();
//
//	return 0;
//}
//
