//#define _CRT_SECURE_NO_WARNINGS
//
//// 太平洋和大西洋水流问题
//class Solution {
//public:
//    int m, n;
//    bool vis1[200][200];
//    bool vis2[200][200];
//    vector<vector<int>> ans;
//    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights) {
//        m = heights.size(), n = heights[0].size();
//        //太平洋边界的点能去哪些位置
//        for (int i = 0; i < m; i++)
//        {
//            dfs1(heights, i, 0);
//            dfs2(heights, i, n - 1);
//        }
//        for (int j = 0; j < n; j++)
//        {
//            dfs1(heights, 0, j);
//            dfs2(heights, m - 1, j);
//        }
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (vis1[i][j] && vis2[i][j])
//                    ans.push_back({ i,j });
//            }
//        }
//        return ans;
//    }
//    int dx[4] = { 1,-1,0,0 };
//    int dy[4] = { 0,0,1,-1 };
//    void dfs1(vector<vector<int>>& heights, int i, int j)
//    {
//        vis1[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis1[x][y] && heights[x][y] >= heights[i][j])
//                dfs1(heights, x, y);
//        }
//    }
//    void dfs2(vector<vector<int>>& heights, int i, int j)
//    {
//        vis2[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis2[x][y] && heights[x][y] >= heights[i][j])
//                dfs2(heights, x, y);
//        }
//
//    }
//};
//
//
////扫雷
//class Solution {
//public:
//    int dx[8] = { -1,1,0,0,-1,1,-1,1 };
//    int dy[8] = { 0,0,1,-1,1,1,-1,-1 };
//    int m, n;
//    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
//        m = board.size(); n = board[0].size();
//        int x = click[0], y = click[1];
//        if (board[x][y] == 'M')
//        {
//            board[x][y] = 'X';
//            return board;
//        }
//        dfs(board, x, y);
//        return board;
//    }
//    void dfs(vector<vector<char>>& board, int i, int j)
//    {
//        int count = 0;
//        for (int k = 0; k < 8; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'M')
//            {
//                count++;
//            }
//        }
//        if (count)
//        {
//            board[i][j] = count + '0';
//            return;
//        }
//        else
//        {
//            board[i][j] = 'B';
//            for (int k = 0; k < 8; k++)
//            {
//                int x = i + dx[k], y = j + dy[k];
//                if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'E')
//                    dfs(board, x, y);
//            }
//        }
//    }
//};
//
////机器人的运动范围
//class Solution {
//public:
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    //每个数位之和
//
//    int ans, m, n, k;
//    bool vis[100][100];
//    int movingCount(int m1, int n1, int k1) {
//        m = m1, n = n1, k = k1;
//        dfs(0, 0);
//        return ans;
//    }
//    void dfs(int i, int j)
//    {
//        vis[i][j] = true;
//        ans++;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && sum(x, y))
//                dfs(x, y);
//        }
//    }
//    bool sum(int i, int j)
//    {
//        int ans = 0;
//        while (i)
//        {
//            ans += i % 10;
//            i /= 10;
//        }
//        while (j)
//        {
//            ans += j % 10;
//            j /= 10;
//        }
//        return ans <= k;
//    }
//};
//
//
////记忆化搜索解决斐波那契
//class Solution {
//public:
//    int memo[31];
//    int fib(int n) {
//        memset(memo, -1, sizeof(memo));
//        return dfs(n);
//    }
//    int dfs(int n)
//    {
//        if (memo[n] != -1)
//        {
//            return memo[n];
//        }
//        if (n == 0 || n == 1)
//        {
//            memo[n] = n;
//            return memo[n];
//        }
//        memo[n] = dfs(n - 1) + dfs(n - 2);
//        return memo[n];
//    }
//};
//
//
////不同路径（机器人
//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        vector<vector<int>> memo(m + 1, vector<int>(n + 1));
//        return dfs(m, n, memo);//表示到达mn位置有多少不同的路径
//    }
//    int dfs(int i, int j, vector<vector<int>>& memo)
//    {
//        if (memo[i][j] != 0)
//            return memo[i][j];
//        if (i == 0 || j == 0) return 0;
//        if (i == 1 && j == 1)
//        {
//            memo[i][j] = 1;
//            return 1;
//        }
//        memo[i][j] = dfs(i - 1, j, memo) + dfs(i, j - 1, memo);
//        return memo[i][j];
//
//    }
//};
//
////最长递增子序列
//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        //dfs是以i为起点的最长
//        int ans = 0;
//        vector<int> memo(nums.size());
//        for (int i = 0; i < nums.size(); i++)
//        {
//            ans = max(ans, dfs(i, nums, memo));
//        }
//        return ans;
//    }
//    int dfs(int pos, vector<int>& nums, vector<int>& memo)
//    {
//        if (memo[pos] != 0) return memo[pos];
//
//        int ans = 1;
//        for (int i = pos + 1; i < nums.size(); i++)
//        {
//            if (nums[i] > nums[pos])
//                ans = max(ans, dfs(i, nums, memo) + 1);
//        }
//        memo[pos] = ans;
//        return ans;
//    }
//};
//
//
////猜数字的大小
//class Solution {
//public:
//    int memo[201][201];
//    int getMoneyAmount(int n) {
//
//        return dfs(1, n);
//    }
//    int dfs(int l, int r)
//    {
//        if (l >= r) return 0;
//        if (memo[l][r] != 0) return memo[l][r];
//
//        int ans = INT_MAX;
//        for (int head = l; head <= r; head++)
//        {
//            int x = dfs(l, head - 1);
//            int y = dfs(head + 1, r);
//            ans = min(ans, head + max(x, y));
//        }
//        memo[l][r] = ans;
//        return ans;
//    }
//};

