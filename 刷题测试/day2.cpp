#define _CRT_SECURE_NO_WARNINGS

//#include <algorithm>
//class Solution {
//public:
//    /**
//     *
//     * @param head1 ListNode类
//     * @param head2 ListNode类
//     * @return ListNode类
//     */
//    ListNode* reverse(ListNode* head) //反转，之前做过
//    {
//        if (!head) return nullptr;
//        ListNode* pre = nullptr, * cur = head, * tmp;
//        while (cur)
//        {
//            tmp = cur->next;
//            cur->next = pre;
//            pre = cur;
//            cur = tmp;
//        }
//        return pre;
//
//
//    }
//    ListNode* addInList(ListNode* head1, ListNode* head2) {
//        // write code here
//        ListNode* p1 = reverse(head1); //记录反转之后的头结点
//        ListNode* p2 = reverse(head2);//记录反转之后的头结点
//        ListNode* benwei, * jinwei; //本位是指 例如 21 的1，进位是2
//        int add1 = 0, add2 = 0;  //这两个存储每个节点的val，因为可能节点为空，所以用一个值记录一下，空就是0
//
//        benwei = new ListNode(0);  //首先new一个本位节点，他要和add1，add2加在一起算结果
//        while (p1 || p2)  //两个都为空遍历结束
//        {
//            add1 = p1 != nullptr ? p1->val : 0; //保存第一个链表的每个节点值
//            add2 = p2 != nullptr ? p2->val : 0; //保存第二个链表的每个节点值
//            int result = add1 + add2 + benwei->val; //总结果加和，上面例子对应21
//            benwei->val = result % 10; //本位是21的1（取模）
//            if (result >= 10) //如果总结果比10大于等于，需要进位
//                jinwei = new ListNode(1); //进位肯定进1，因为每个节点的值<10，加在一起<20
//            else
//                jinwei = new ListNode(0); //不需要进位直接设为0
//            jinwei->next = benwei; //进位的下一个节点是本位，2->1
//            benwei = jinwei; //本位变成链表头，即2，第一次相加后本位2->1
//            if (p1) p1 = p1->next; //如果p1不是空，那么后移
//            if (p2) p2 = p2->next; //如果p2不是空，那么后移
//            ;
//        }
//        return benwei->val!=0 ? benwei : benwei->next; //本位是链表头，如果链表头是0，从下一位开始
//    }
//};

//
//ListNode* sortInList(ListNode* head) {
//    // write code here
//    if (!head || !head->next) return head;
//    ListNode* p = head;
//    vector<int> v;
//    while (p)
//    {
//        v.push_back(p->val);
//        p = p->next;
//
//    }
//    sort(v.begin(), v.end());
//    p = head;
//    for (auto it : v)
//    {
//        p->val = it;
//        p = p->next;
//    }
//    return head;
//
//}
//};


//#include <utility>
//#include <vector>
//class Solution {
//public:
//    /**
//     *
//     * @param head ListNode类 the head
//     * @return bool布尔型
//     */
//    bool isPail(ListNode* head) {
//        // 还是先vector保存数据
//        if (!head || !head->next) return true;
//        vector<int> v;
//        ListNode* p = head;
//        while (p)
//        {
//            v.push_back(p->val);
//            p = p->next;
//        }
//        vector<int>::iterator a = v.begin(); //一个头迭代器
//        vector<int>::reverse_iterator r = v.rbegin(); //一个反向迭代器
//        for (; a != v.end() && r != v.rend(); a++, r++) //当两个迭代器都没走到末尾
//        {
//            if (*a != *r) //有不等的就不是回文，回文对称位置一定相等
//                return false;
//        }
//        return true;
//    }
//};
//#include<iostream>
//#include <vector>
//using namespace std;
//struct ListNode {
//    ListNode* next;
//    int val;
//};
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* oddEvenList(ListNode* head) {
//        // write code here
//        if (!head || !head->next) return head;
//        vector<int> v;
//        ListNode* p = head;
//        while (p)
//        {
//            v.push_back(p->val);
//            p = p->next;
//
//        }
//        p = head;
//        vector<int>::iterator it = v.begin();
//        for (; it != v.end(); it += 2)
//        {
//            if (p)
//            {
//                p->val = *it;
//                p =->next;
//            }
//
//        }
//        it = v.begin() + 1;
//        for (; it != v.end(); it += 2)
//        {
//            if (p)
//            {
//                p->val = *it;
//                p = p->next;
//            }
//
//        }
//        return head;
//
//    }
//     
//};
//
//
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* oddEvenList(ListNode* head) {
//        // write code here
//        if (!head || !head->next) return head;
//        ListNode* h1 = head->next;
//        ListNode* even = head, * odd = head->next;
//        while (even->next && odd->next)
//        {
//            even->next = even->next->next;
//            even = even->next;
//            odd->next = odd->next->next;
//            odd = odd->next;
//        }
//        even->next = h1;
//        return head;
//    }
//};

//ListNode* deleteDuplicates(ListNode* head) {
//    // write code here
//    if (!head || !head->next) return head;
//    ListNode* p = head;
//    while (p)
//    {
//        if (p->next && p->val == p->next->val)
//        {
//            ListNode* tmp = p->next;
//
//            p->next = p->next->next;
//            delete tmp;
//        }
//        else
//            p = p->next;
//
//    }
//    return head;
//}
//};