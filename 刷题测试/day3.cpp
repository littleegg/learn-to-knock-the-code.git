#define _CRT_SECURE_NO_WARNINGS
//ListNode* deleteDuplicates(ListNode* head) {
//    // write code here
//    if (!head || !head->next) return head; //空链表或者只有一个节点直接返回
//    ListNode* p = new ListNode(-1); //哨兵位
//    p->next = head; //哨兵位和链表链接
//    ListNode* cur = p; //遍历的指针cur
//    while (cur->next && cur->next->next) //保证不访问空
//    {
//        if (cur->next->val == cur->next->next->val) //下一个和下下个是重复项
//        {
//            int tmp = cur->next->val; //记录一下重复的val，保证删干净所有val
//            while (cur->next && cur->next->val == tmp) //从cur->next开始，值是val
//                cur->next = cur->next->next; //cur往后链接
//        }
//        else {
//            cur = cur->next; //没有重复就正常遍历
//        }
//
//    }
//    return p->next;
//
//}

//class Solution {
//public:
//    int Fibonacci(int n) {
//        int F[n + 1]; //用数组保存每个斐波那契数，节约成本，避免不必要的多次计算
//        F[0] = 1; //初始化
//        F[1] = 1; //初始化
//        for (int i = 2; i < n; i++)
//        {
//            F[i] = F[i - 1] + F[i - 2]; //状态方程的建立
//        }
//        return F[n - 1]; //返回末状态
//    }
//};


//int search(vector<int>& nums, int target) {
//    // write code here
//    int begin = 0, end = nums.size() - 1;
//    while (begin <= end)
//    {
//        int mid = (begin + end) / 2;
//        if (target == nums[mid])
//            return mid;
//        else if (target > nums[mid])
//            begin = mid + 1;
//        else if (target < nums[mid])
//            end = mid - 1;
//        else
//            return -1;
//    }
//    return -1;
//}
//};

//class Solution {
//public:
//    bool Find(int target, vector<vector<int> > array) {
//        if (array[0].size() < 1) return false; //很奇怪，这个样例{{}}是size()=1，要特殊处理
//        for (int i = 0; i < array.size(); i++) //array.size()是行数
//        {
//            if (target > array[i][0]) //需要进去和这一行的每个元素比较
//            {
//                for (int j = 0; j < array[i].size(); j++) // array[i].size()是第i行的元素个数
//                {
//                    if (target == array[i][j]) //找到了
//                        return true;
//                }
//            }
//            else if (target < array[i][0])  //这一行没戏，第一个数是这一行最小的，但是都比target大
//            {
//                break;
//            }
//            else return true; //找到了
//        }
//        return false;
//    }
//};



//
//int findPeakElement(vector<int>& nums) {
//    // write code here
//    for (int i = 0; i < nums.size(); i++)  //遍历
//    {
//        if (i == 0) //可能第一项就是峰值
//        {
//            if (nums[i] > nums[i + 1]) //判断是不是峰值
//                return i;
//        }
//        else if (i == nums.size() - 1) //可能最后一项是峰值
//        {
//            if (nums[i] > nums[i - 1]) //判断是不是
//                return i;
//        }
//        else {
//            if (nums[i] > nums[i - 1] && nums[i] > nums[i + 1])//普通位置的判断 
//                return i;
//        }
//    }
//    return -1;
//}
//};

//
//int findPeakElement(vector<int>& nums) {
//    // write code here
//    int begin = 0, end = nums.size() - 1; //两侧
//    while (begin < end) //不能写等号，因为相等的时候都是0，mid也是0，但是没有nums[0+1](越界）
//    {
//        int mid = (begin + end) / 2;
//        if (nums[mid] < nums[mid + 1]) //说明峰值在右区间
//            begin = mid + 1;
//        else
//            end = mid;
//    }
//    return begin; //随便=返回begin/end
//}
//};

//class Solution {
//public:
//    const int mod = 1000000007; 
//    int merges(int l, int r, vector<int>& data) //归并，边排序边找逆序
//    {
//        if (l >= r) return 0; //区间不成立返回0
//        int mid = (l + r) / 2; //中间值
//        int res = merges(l, mid, data) + merges(mid + 1, r, data); //两个区间的逆序数之和
//        int j = mid + 1, i = l; 
//        vector<int> v; //数组储存排序之后的数列
//
//        while (i <= mid && j <= r) //满足是区间
//        {
//            if (data[i] <= data[j]) //左区间直接进
//                v.push_back(data[i++]);
//            else
//            {
//                v.push_back(data[j++]); //右区间进
//                res += mid - i + 1; //逆序数+左区间.size()-1 ,[1,mid]有mid-i+1个元素
//                if (res >= mod) res %= mod;
//            }
//        }
//        while (i <= mid)
//            v.push_back(data[i++]);
//        while (j <= r)
//            v.push_back(data[j++]);
//        i = l;
//        for (auto x : v) data[i++] = x;
//        return res;
//    }
//    int InversePairs(vector<int> data) {
//        return merges(0, data.size() - 1, data);
//    }
//};