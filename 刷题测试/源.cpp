//#define _CRT_SECURE_NO_WARNINGS
//#include <iostream>
//#include <string>
//using namespace std;
////int main()
////{
////    // 请在此输入您的代码
////    string s;
////    cin >> s;
////    int a[28] = { 0 };
////    for (int i = 0; i < s.size(); i++)
////    {
////        a[s[i] - 'a']++;
////    }
////    int count = 0;
////    int j = 0;
////    for (int i = 0; i < 28; i++)
////    {
////        if (a[i] > count)
////        {
////            count = a[i];
////            j = i;
////        }
////    }
////    printf("%c\n%d\n", 'a' + j, count);
////    return 0;
////}
//
//
//using namespace std;
//int main()
//{
//    // 请在此输入您的代码
//    int num = 0;
//    cin >> num;
//    int a[7] = { 0 };
//    int c = 0;
//    int nice = 0, n = 0;
//    int go = 0, g = 0;
//    for (int i = 0; i < num; i++)
//    {
//        scanf("%d", &c);
//        if (c >= 60) { go += c; g++; }
//        if (c >= 85)
//        {
//            n++; nice += c;
//        }
//    }
//    printf("%d\n%d\n", go / g, nice / n);
//    return 0;
//}
//
//#include <cstddef>
//class Solution {
//public:
//    ListNode* ReverseList(ListNode* pHead) {
//        ListNode* cur = pHead; //当前指针
//        ListNode* tail = nullptr;  //尾指针
//        ListNode* tmp; //保存指向，防止找不到
//        while (cur) //一直遍历到链表尾
//        {
//            tmp = cur->next;  //首先保存下一个位置的指向
//            cur->next = tail; //反转
//            tail = cur; //tail往后走
//            cur = tmp; //cur向后走，不能先写tail=cur要不然cur就丢了
//
//        }
//        return tail;
//    }
//};
//
//class Solution {
//public:
//    ListNode* ReverseList(ListNode* pHead) {
//        if (!pHead || !pHead->next) return pHead;  //如果节点为空或者next为空 此时的返回值是最后一个节点
//        ListNode* tail = ReverseList(pHead->next);//tail接收最后一个节点，也就是结果链表的第一个节点
//        //pHead->next->next=pHead; //pHead->next是尾节点，把当前节点pHead挂到尾节点的后面
//        //pHead->next=nullptr; //当前节点指向置空
//        //return tail; //返回反转链表的第一个节点
//        pHead->next->next = pHead;
//        pHead->next = nullptr;
//        return tail;
//    };
//};
//
//
//ListNode* reverseBetween(ListNode* head, int m, int n) {
//    ListNode* res = new ListNode(-1);  //设置哨兵位，防止pre越界
//    res->next = head;  //哨兵位和链表连上
//    ListNode* pre = res;
//    ListNode* cur = head;
//    for (int i = 1; i < m; i++)
//    {
//        pre = cur;
//        cur = cur->next;
//    }                    //cur在m个节点上，pre是他前一个节点
//    for (int i = m; i < n; i++)  //头插迭代，看文字说明
//    {
//        ListNode* tmp = cur->next;
//        cur->next = tmp->next;
//        tmp->next = pre->next;
//        pre->next = tmp;
//    }
//    return res->next;
//}
//};
//
//class Solution {
//public:
//    /**
//     *
//     * @param head ListNode类
//     * @param k int整型
//     * @return ListNode类
//     */
//    ListNode* reserve(ListNode* head, int m, int n)  //m-n区间反转
//    {
//        ListNode* res = new ListNode(-1);
//        res->next = head;
//        ListNode* cur = head;
//        ListNode* pre = res;
//        for (int i = 1; i < m; i++)
//        {
//            pre = cur;
//            cur = cur->next;
//        }
//        for (int i = m; i < n; i++)
//        {
//            ListNode* tmp = cur->next;
//            cur->next = tmp->next;
//            tmp->next = pre->next;
//            pre->next = tmp;
//        }
//        return res->next;
//    }
//    ListNode* reverseKGroup(ListNode* head, int k) {
//        // write code here
//        if (!head) return head; //空直接返回
//
//        int m = 1, n = k;
//        ListNode* cur = head;
//        int size = 0;  //计算节点个数
//        while (cur)
//        {
//            cur = cur->next;
//            size++;
//        }
//        if (k > size) return head;  //k比节点总数多直接不反转
//        head = reserve(head, m, n); //肯定反转一次
//
//        m += k;
//        n += k;
//        while (n <= size) //迭代
//        {
//            head = reserve(head, m, n);
//            m += k; n += k;
//        }
//        return head;
//    }
//};
//
//class Solution {
//public:
//    /**
//     *
//     * @param head ListNode类
//     * @param k int整型
//     * @return ListNode类
//     */
//    ListNode* reserve(ListNode* head, int m, int n)  //m-n区间反转
//    {
//        ListNode* res = new ListNode(-1);
//        res->next = head;
//        ListNode* cur = head;
//        ListNode* pre = res;
//        for (int i = 1; i < m; i++)
//        {
//            pre = cur;
//            cur = cur->next;
//        }
//        for (int i = m; i < n; i++)
//        {
//            ListNode* tmp = cur->next;
//            cur->next = tmp->next;
//            tmp->next = pre->next;
//            pre->next = tmp;
//        }
//        return res->next;
//    }
//    ListNode* reverseKGroup(ListNode* head, int k) {
//        // write code here
//        if (!head) return head; //空直接返回
//
//        int m = 1, n = k;
//        ListNode* cur = head;
//        int size = 0;  //计算节点个数
//        while (cur)
//        {
//            cur = cur->next;
//            size++;
//        }
//        if (k > size) return head;  //k比节点总数多直接不反转
//        head = reserve(head, m, n); //肯定反转一次
//
//        m += k;
//        n += k;
//        while (n <= size) //迭代
//        {
//            head = reserve(head, m, n);
//            m += k; n += k;
//        }
//        return head;
//    }
//};
//
//
//class Solution {
//public:
//    ListNode* merge(ListNode* head1, ListNode* head2)
//    {
//        ListNode* newhead = new ListNode(-1);
//        ListNode* cur = newhead;
//        ListNode* cur1 = head1;
//        ListNode* cur2 = head2;
//        while (cur1 && cur2)
//        {
//            ListNode* tmp1 = cur1;
//            ListNode* tmp2 = cur2;
//            if (cur1->val < cur2->val)
//            {
//                cur->next = cur1;
//                cur1 = cur1->next;
//            }
//            else
//            {
//                cur->next = cur2;
//                cur2 = cur2->next;
//            }
//            cur = cur->next;
//        }
//        while (cur1)
//        {
//            cur->next = cur1;
//            cur1 = cur1->next;
//            cur = cur->next;
//        }
//        while (cur2)
//        {
//            cur->next = cur2;
//            cur2 = cur2->next;
//            cur = cur->next;
//        }
//        return newhead->next;
//    }
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        if (lists.size() == 0)return nullptr;
//        ListNode* newhead = lists[0];
//        for (int i = 1; i < lists.size(); i++)
//        {
//            newhead = merge(newhead, lists[i]);
//        }
//        return newhead;
//    }
//};

