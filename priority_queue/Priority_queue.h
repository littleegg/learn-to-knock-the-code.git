#pragma once
#include <iostream>
#include <vector>
using namespace std;

#include <functional>
namespace wrt
{
	template<class T>
	struct  less
	{
		bool operator()(const T& x, const T& y)
		{
			return x < y;
		}
	};
	template<class T>
	struct greater
	{
		bool operator()(const T& x, const T& y)
		{
			return x > y;
		}
	};
	template<class T,class Container=vector<T>,class Compare= less<T>>
	class priority_queue
	{
	public:
		priority_queue()
		{}
		template <class InputIterator>
		priority_queue(InputIterator first, InputIterator last)
			:_con(first,last)
		{
			for (size_t i= (_con.size() - 1 - 1 )/ 2; i >=0; i--)//一个节点-1  /2  是用来算父亲节点的 
			{
				//向下调整
				adjust_down(i);
			}
		}
		void adjust_up(size_t child)  //违背祖宗...
		{
			Compare com;
			size_t  parent = (child - 1) / 2;
			while (child >0)
			{
				if(com(_con[parent],_con[child]))
				//if (_con[child] > _con[parent])
				{
					swap(_con[child], _con[parent]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					break;
				}
			}
		}
		void adjust_down(size_t parent)
		{
			Compare com;
			size_t  child = 2 * parent + 1;//假设是左孩子，并且左孩子是最大的孩子
			while (child < _con.size())
			{
			//	if (child + 1 < _con.size() && _con[child + 1] > _con[child])
			if (child + 1 < _con.size() && com( _con[child],_con[child + 1]))
				{
					child++;
				}
				//此时的child已经是最大的孩子
				//if (_con[parent] < _con[child])
				if (com(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					parent = child;
					child = 2 * parent + 1;
				}
				else
				{
					break;
				}
			}
		}
		void push(const T& x)//插入数据就是向上调整
		{
			_con.push_back(x);
			adjust_up(_con.size() - 1);
		}
		void pop()
		{
			swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			adjust_down(0);
		}
		const T& top() const
		{
			return _con[0];
		}
		bool empty() const 
		{
			return _con.empty();
		}
		size_t size()
		{
			return _con.size();
		}
	private:
		Container _con;
	};
	void test()
	{
		priority_queue<int> pq;
		priority_queue<int,vector<int>,greater<int>> pq1;

		pq.push(1);
		pq.push(4);
		pq.push(6);
		pq.push(7);
	
		while (!pq.empty())
		{
			cout << pq.top() << " ";
			pq.pop();
		}
		cout << endl;
		pq1.push(1);
		pq1.push(4);
		pq1.push(6);
		pq1.push(7);

		while (!pq1.empty())
		{
			cout << pq1.top() << " ";
			pq1.pop();
		}
		cout << endl;
	}
}