#include<stdio.h>
//首先写一个求和函数
//int Add(int x, int y)
//{
//	int z = x + y;
//	return z;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf_s("%d%d", &a, &b);
//	int sum = Add(a,b);
//	printf("sum=%d", sum);
//	return 0;
//}
//#include <string.h>
//int main()
//{
//	char arr1[] = "dandan";
//	char arr2[30] ;
//	char arr3[40] ;
//	strcpy_s(arr2,30, arr1);
//	strcpy_s(arr3,40, "zhihuiguan");
//	printf("arr2=%s\narr3=%s\narr1=%s\n", arr2,arr3,arr1);
//	return 0;
//}
//#include <string.h>
//int main()
//{
//	char arr[] = "dan dan";
//	memset(arr, 'b', 1);
//	printf(" % s\n", arr);
//	return 0;
//}

//写一个函数找出两个整数中的最大值
//int get_max(int x, int y)
//{
//	if (x > y)
//		return x;
//	else
//		return y;
//}
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	scanf_s("%d%d", &num1, &num2);
//	int M =get_max(num1, num2);
//	printf("max=%d", M);
//	return 0;
//}
//int get_max(int x, int y)
//{
//	if (x > y)
//		return x;
//    else
//		return y;
//}
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	int max = get_max(100, 200);
//	printf("max=%d", max);
//	return 0;
//}
// 
//写一个函数可以交换两个整型变量的内容
//1.我自己写的
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	int mid = 0;
//	scanf_s("%d%d", &num1, &num2);
//	mid = num1;
//	num1 = num2;
//	num2 = mid;
//	printf("num1=%d\nnum2=%d\n", num1, num2);
//	return 0;
//}
//2.老师的
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int tmp = 0;
//	printf("a=%d\nb=%d\n", a, b);
//	tmp = a;
//	a = b;
//	b = tmp;
//	printf("a=%d\nb=%d\n", a, b);
//	return 0;
//}
//但我写的不是自定义函数，修改如下
//void Swap(int* x, int* y)
//{
//	int mid = 0;
//	mid = *x;
//	*x = *y;
//	*y = mid;
//}
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	scanf_s("%d%d", &num1, &num2);
//	printf("num1=%d\nnum2=%d\n", num1, num2);
//	Swap(&num1, &num2);
//	printf("num1=%d\nnum2=%d\n", num1, num2);
//	return 0;
//}
//首先需要回顾指针变量的知识
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	*pa = 20;
//	printf("%d", a);
//	return 0;
//}
//#include<string.h>
//int main()
//{
//	char arr1[] = "abc";
//	char arr2[] = { 'a','b','c' };
//	printf("%d\n", strlen(arr1));
//	printf("%d\n", strlen(arr2));
//	printf("%d\n", sizeof(arr1));
//	printf("%d\n", sizeof(arr2));
//	return 0;
//}
//int main()
//{
//	char arr[] = { 'a', 'b', 'c' };
//	printf("%d", sizeof(arr));
//	return 0;
//}
//int main()
//{
//    int a ,b ;
//    scanf_s("%d %d", &a, &b);
//    printf("%d %d", a/b, a%b);
//    return 0;
//}
//int main()
//{
//    int n;
//    scanf_s("%d", &n);
//    for (int i = 0; i < 4; i++)
//    {
//        printf("%d", n % 10);
//        n = n / 10;
//    }
//    return 0;
//}
//int main()
//{
//    int a, b, c, d, e;
//    scanf_s("%d%d%d%d%d", &a, &b, &c, &d, &e);
//    int z = a + b + c + d + e;
//    printf("%.1lf", z);
//        return 0;
//}
//int main()
//{
//    int a, b;
//    char ch;
//    while (scanf_s("%d %d", &a, &b) != EOF)
//    {
//        
//        if (a > b)
//            ch = '>';
//        else if (a < b)
//            ch = '<';
//        else
//            ch = '=';
//        printf("%d%c%d\n", a, ch, b);
//
//    }
//    return 0;
//}
//int main()
//{
//    int age = 0;
//    scanf_s("%d", &age);
//    printf("%d", age * 31560000);
//    return 0;
//}
//int main()
//{
//	float a, b, c;
//	scanf_s("%f%f%f", &a, &b, &c);
//	float z = a + b + c;
//	printf("%.2lf %.2lf", z, z / 3);
//	return 0;
//}
//int main()
//{
//    int w, t;
//    scanf_s("%d%d", &w, &t);
//    float  a = t/100.0;
//    float BMI= w/(a*a);
//    printf("%.2lf", BMI);
//    return 0;
//}
//写一个函数判断是不是函数，首先回顾一下打印100-200的素数
//#include <math.h>
//int main()
//{
//	int a = 0;
//	for (a = 100; a <= 200; a++)
//	{
//		int j = 0;
//		for (j = 2; j <= sqrt(a); j++)
//		{
//			if (a % j == 0)
//				break;
//		}
//		if (j > sqrt(a))
//			printf("%d ",a); 
//	}
//	return 0;
//}
//现在正式写函数来判断
//#include <math.h>
//int is_prime(int x) //是素数返回1，不是返回0
//{
//	int j = 0;
//	for (j = 2; j <= sqrt(x); j++)
//	{
//		if (x % j == 0)
//			return 0;
//	}
//		return 1;
//}
//int main()
//{
//	int  a = 0;
//	for (a = 100; a <= 200; a++)
//	{
//		if (is_prime(a) == 1)
//			printf("%d ", a);
//	}
//	return 0;
//}
//现在写一个函数判断是不是不闰年
//int is_d(int x)
//{
//	if (x%4==0 && x%100 !=0 || x % 400 == 0)
//		return 1;
//	else
//		return 0;
//}
//int main()
//{
//	int year = 0;
//	scanf_s("%d", &year);
//		if (is_d(year) == 1)
//			printf("YES");	   
//		else 
//			printf("NO");
//	return 0;
//}
//这个是老师的写法
//int is_leap_year(int x)
//{
//	if (x % 4 == 0 && x % 100 != 0 || x % 400 == 0)
//			return 1;
//		else
//			return 0;
//}
//
//int main()
//{
//	int year;
//	for (year = 1000; year <= 2000; year++)
//	{
//		if (1 == is_leap_year(year))
//			printf("%d ", year);
//	}
//	return 0;
//}
//二分查找，在一个有序数组里查找具体的某个数
//int binary_search(int arr[], int x,int sz)
//{
//	int begin = 0;
//	int last = sz - 1;
//	while (begin <= last)
//	{
//		int mid = (begin + last) / 2;
//		if (x < arr[mid])
//			last = mid - 1;
//		else if (x > arr[mid])
//			begin = mid + 1;
//		else
//			return mid;
//	}
//	if (begin > last)
//		return 0;
//}
//int main()
//{
//	int k = 0;
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	scanf_s("%d", &k);
//	int ret = binary_search(arr, k,sz);
//	if (ret == 0)
//		printf("NO %d",-1);
//	else
//		printf("YES %d",ret );
//	return 0;
//}
//每调用一次num+1
//void S(int* x)
//{
//	(*x)++;
//}
//int main()
//{
//	int num = 0;
//	S(&num);
//	printf("%d",num);
//	S(&num);
//	printf("%d", num);
//	S(&num);
//	printf("%d", num);
//	return 0;
//}
//int main()
//{
//	int len = 0;
//	printf("%d\n", strlen("abc"));
//	return 0;
//}
//int main()
//{
//	printf("%d", printf("%d", printf("%d", 43)));		
//	return 0;
//}
//#include"add.h"
//int main()
//{
//	int a,b;
//	scanf_s("%d%d", &a, &b);
//	int ret = Add(a, b);
//	printf("%d ", ret);
//	return 0;
//}
//int main()
//{
//	printf("dandan\n");
//	main();
//	return 0;
//}
//void print(int x) //1234
//{
//	if (x > 9)  //两位数及以上就拆下来以为，然后单独打印剩下的
//		print(x / 10);  //123
//	printf("%d ",x % 10); //4
//}
//int main()
//{
//	unsigned int num = 0;
//	scanf_s("%d", &num);
//	print(num);
//
//	return 0;
//}
//int my_strlen(char* x) //循环判断的方法
//{
//	int count=0;
//	while (*x != '\0')
//	{
//		count++;
//		x++;
//	}
//	return count;
//}
//int my_strlen(char* x)
//{
//	if(*x != '\0')
//		return 1 + my_strlen(x+1);
//	else
//		return 0;
//}
//int main()
//{
//	char arr[] = "dandan";
//	int len = my_strlen(arr);
//	printf("%d\n", len); //arr在传参的时候是传首元素的地址
//	return 0;
//}
//求n的阶乘
//int main()
//{
//	int n,j;
//	int ret = 1;
//	scanf_s("%d", &n);
//	for (j = 1; j <= n; j++)
//	{
//		ret *= j;
//	}
//	printf("%d", ret);
//	return 0;
//}
//int Fac(int x) //4
//{
//	if (x <= 1)
//		return 1;
//	else
//		return x * Fac(x - 1);
//}
//int main()
//{
//	int a, ret;
//	scanf_s("%d", &a);
//	ret = Fac(a);
//	printf("%d", ret);
//	return 0;
//}
//int count;
//int Fib(int x)
//{
//	if (x == 3)
//	{
//		count++;
//	}
//	if (x <= 2)
//		return 1;
//	else
//		return Fib(x - 2) + Fib(x - 1);
//}
//int main()
//{
//	
//	int n = 0;
//	scanf_s("%d", &n);
//	int ret = Fib(n);
//	printf("ret=%d", ret);
//	printf("count=%d", count);
//	return 0;
//}
//int count;
//int Fib(int x)
//{
//	int a = 0; int b = 0; int c = 1;
//	for (a = 1, b = 1; x > 2; x--)
//	{
//       c = a + b;
//	   a = b;
//	   b = c;
//	   
//	}
//	return c;
//}
//int main()
//{
//	int n = 0;
//	int ret = 0;
//	scanf_s("%d", &n);
//	ret = Fib(n);
//	printf("ret=%d\n", ret);
//	printf("count=%d\n", count);
//	return 0;
//}
//void print(int x)
//{
//    if (x > 9)
//        print(x / 10);
//    printf("%d ", x % 10);
//}
//int main()
//{
//    unsigned  int a = 0;
//    scanf_s("%d", &a);
//    print(a);
//    return 0;
//}
//int my_strlen(char* x)
//{
//    if (*x != '\0')
//        return 1 + my_strlen(x + 1);
//    else
//        return 0;
//}
//int main()
//{
//    char arr[] = "dada";
//    int ret = my_strlen(arr);
//    printf("%d", ret);
//    return 0;
//}
//int count;
//int Fib(int x)
//{
//    if (x == 2)
//        count++;
//    if (x > 2)
//        return Fib(x - 2) + Fib(x - 1);
//    else
//        return 1;
//}
//int main()
//{
//    int n = 0;
//    scanf_s("%d", &n);
//    int ret = Fib(n);
//    printf("ret=%d", ret);
//    printf("count=%d", count);
//    return 0;
//}
//int count;
//int Fib(int x)
//{
//	int a, b = 0;
//	int c = 1;
//	for (a = 1, b = 1; x > 2; count++, x--)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//	}
//	return c;
//}
//
//int main()
//{
//		int n = 0;
//		scanf_s("%d", &n);
//		int ret = Fib(n);
//		printf("ret=%d\n", ret);
//		printf("count=%d", count);
//		return 0;
//}
int Fac(int x)
{
    if (x > 1)
        return x * Fac(x - 1);
    else
        return 1;
}
int main()
{
    int n = 0;
    scanf("%d", &n);
    int ret = Fac(n);
    printf("%d", ret);

    return 0;
}