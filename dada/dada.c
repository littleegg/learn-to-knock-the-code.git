#define _CRT_SECURE_NO_WARNINGS
#include "game.h"
void menu()
{
	printf("****************\n");
	printf("*****1.play*****\n");
	printf("*****0.exit*****\n");
	printf("****************\n");
}
void game()
{
	char ret = 0;
	char board[ROW][COL] = { 0 };
	InitBoard(board, ROW, COL);
	PrintBoard(board, ROW, COL);
	while (1)
	{
		PlayerMove(board, ROW, COL);
		PrintBoard(board, ROW, COL);
	    ret = Jugue(board, ROW, COL);
		if (ret == '*')
		{
			printf("玩家赢\n");
			break;
		}
		else if (ret == 'Q')
		{
          printf("平局\n");
		  break;
		}
		ComputerMove(board, ROW, COL);
		PrintBoard(board, ROW, COL);
		ret = Jugue(board, ROW, COL);
		if (ret == '#')
		{
			printf("电脑赢\n");
			break;
		}
	}
}
void test()
{
	int input =0;
	srand((unsigned int)time(NULL));
	do 
	{
		menu();
	   printf("请输入\n");
	   scanf_s("%d", &input);
		switch (input)
		{
		case 1:
		{
			printf("游戏开始\n");
			game();
			break;
		}
		case 0:
		{
			printf("退出游戏\n");
			break;
		}
		default:
		{
			printf("请重新输入\n");
			break;
		}
		}
	} while (input);
}
int main()
{
	test();
	return 0;
}