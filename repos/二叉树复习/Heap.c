#include "Heap.h"

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}
void AdjustUp(HeapDataType* a, int child)
{
	int parent = (child - 1) / 2;
	while(child>0) //如果孩子更大
	{
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}
void HeapPrint(Hp*hp)
{
	assert(hp);
	for(int i=0;i<=hp->size;i++)
	{
		printf("%d ", hp->a[i]);
	}
	printf("\n");
}
void HeapDestory(Hp* hp)
{
	assert(hp);
	free(hp->a);
	hp->a = NULL;
	hp->capacity = hp->capacity = 0;
}
void HeapInit(Hp *hp)
{
	assert(hp);
	hp->a = NULL;
	hp->capacity = hp->size = 0;
}
void Heappush(Hp* hp,HeapDataType x)
{
	assert(hp);
	if (hp->capacity == hp->size)
	{
		int newcapacity = hp->capacity == 0 ? 4 : hp->capacity * 2;
		HeapDataType* tmp = (HeapDataType*)realloc(hp->a, sizeof(HeapDataType) * newcapacity);
		if (tmp == NULL)
		{
			perror("realloc fail");
			exit(-1);
		}
		hp->a = tmp;
		hp->capacity = newcapacity;
	}
	hp->a[hp->size] = x;
	hp->size++;
	//默认这里是大堆结构，父亲都应该>=孩子
	
	//向上调整
		AdjustUp(hp->a,hp->size);
	
}
void AdjustDown(HeapDataType* a, int n,int parent)
{
	assert(a);
	int child = parent * 2 + 1;
	while (child<n)
	{
		if (a[child + 1] > a[child])
		{
			child = child + 1;
		}
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child= parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}
void HeapPop(Hp* hp)
{
	assert(hp);
	assert(hp->size>0);
	Swap(&hp->a[0], &hp->a[hp->size - 1]);
	hp->size--;
	AdjustDown(hp->a, hp->size,0);
}
HeapDataType HeapTop(Hp* hp)
{
	assert(hp);
	return hp->a[0];
}