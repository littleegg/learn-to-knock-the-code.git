#include "Heap.h"
void test1()
{
	Hp hp;
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 }; 
	HeapInit(&hp);
	for (int i = 0; i <= sizeof(arr)/sizeof(int);i++)
	{
      Heappush(&hp,arr[i]);
	}
	int k = 2;
	while (k--)
	{
		HeapPop(&hp);
		HeapDataType x=HeapTop(&hp);
		printf("%d ", x);
	}
	HeapPrint(&hp);
	HeapDestory(&hp);
}
int main()
{
	test1();
}