#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

typedef int HeapDataType;
typedef struct Heap
{
	HeapDataType* a;
	int size;
	int capacity;
}Hp;

void Swap(int* a, int* b);

void AdjustUp(HeapDataType* a, int child);

void HeapPrint(Hp* hp);

void HeapDestory(Hp* hp);

void HeapInit(Hp* hp);

void Heappush(Hp* hp, HeapDataType x);
void AdjustDown(HeapDataType* a, int n, int parent);
void HeapPop(Hp* hp);
HeapDataType HeapTop(Hp* hp);

