#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>
using namespace std;
int main()
{
	int n = 0;
	int s = 0;
	int d = 0;
	int a = 0;
	cin >> n;
	while( n--)
	{
		cin >> a;
		if (a % 2) s++;
		else d++;
	}
	if (d > s) cout << "YES" << endl;
	else cout << "NO"<<endl;
		return 0;
}