#define _CRT_SECURE_NO_WARNINGS
#include "head.h"
//char* addStrings(char* num1, char* num2) {
//    int k1 = strlen(num1) - 1, k2 = strlen(num2) - 1; //记录最后一个位置
//    int len = k1 > k2 ? k1 + 1 : k2 + 1; //len代表ans的最后一个元素下标
//    int k = 0, c = 0; //k记录的是两个数组对应元素之和，c判断是不是需要进位
//    char* ans = (char*)malloc(sizeof(char) * (len + 2));
//    ans[len + 1] = '\0'; //最后一个位置要加上终止符
//    while (k1 >= 0 && k2 >= 0)
//    {
//        k = ((num1[k1--] - '0') + (num2[k2--] - '0')) + c;
//        ans[len--] = k % 10 + '0';
//        c = k >= 10 ? 1 : 0;
//    }
//    while (k1 >= 0)
//    {
//        k = (num1[k1--] - '0') + c;
//        ans[len--] = k % 10 + '0';
//        c = k >= 10 ? 1 : 0;
//    }
//    while (k2 >= 0)
//    {
//        k = (num2[k2--] - '0') + c;
//        ans[len--] = k % 10 + '0';
//        c = k >= 10 ? 1 : 0;
//    }
//    ans[len] = c + '0';
//    return ans[0] == '1' ? ans : ans + 1; //是字符1不是数字1
//}
//
//bool isPalindrome(char* s) {
//    //首先把奇怪字符都去掉
//    int k = 0, i = 0;
//    while (s[k])
//    {
//        if (s[k] >= 'A' && s[k] <= 'Z')
//        {
//            s[i++] = s[k++] + 32;
//        }
//        else if (s[k] >= 'a' && s[k] <= 'z')
//        {
//            s[i++] = s[k++];
//        }
//        else if (s[k] >= 48 && s[k] <= 57)
//        {
//            s[i++] = s[k++];
//        }
//        else
//        {
//            k++;
//        }
//    }
//    int left = 0, right = i - 1;
//    while (left < right) {
//        if (s[left] != s[right])
//            return false;
//        left++;
//        right--;
//    }
//    return true;
//}
//
//
//int longestPalindrome(char* s) {
//    int c[128] = { 0 };
//    for (int i = 0; i < strlen(s); i++)
//    {
//        c[s[i]]++;
//    }
//    int ret = 0;
//    for (int i = 0; i < 128; i++)
//    {
//        ret += c[i] - c[i] % 2;
//    }
//    return ret + (ret != strlen(s));
//}