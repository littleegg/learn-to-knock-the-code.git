#define _CRT_SECURE_NO_WARNINGS
#include "head.h"

//重排链表
//struct ListNode* middleNode(struct ListNode* head) {
//    struct ListNode* slow = head;
//    struct ListNode* fast = head;
//    while (fast->next != NULL && fast->next->next != NULL) {
//        slow = slow->next;
//        fast = fast->next->next;
//    }
//    return slow;
//}
//
//struct ListNode* reverseList(struct ListNode* head) {
//    struct ListNode* prev = NULL;
//    struct ListNode* curr = head;
//    while (curr != NULL) {
//        struct ListNode* nextTemp = curr->next;
//        curr->next = prev;
//        prev = curr;
//        curr = nextTemp;
//    }
//    return prev;
//}
//
//void mergeList(struct ListNode* l1, struct ListNode* l2) {
//    struct ListNode* l1_tmp;
//    struct ListNode* l2_tmp;
//    while (l1 != NULL && l2 != NULL) {
//        l1_tmp = l1->next;
//        l2_tmp = l2->next;
//
//        l1->next = l2;
//        l1 = l1_tmp;
//
//        l2->next = l1;
//        l2 = l2_tmp;
//    }
//}
//
//void reorderList(struct ListNode* head) {
//    if (head == NULL) {
//        return;
//    }
//    struct ListNode* mid = middleNode(head);
//    struct ListNode* l1 = head;
//    struct ListNode* l2 = mid->next;
//    mid->next = NULL;
//    l2 = reverseList(l2);
//    mergeList(l1, l2);
//}
//
//
////k个一组翻转链表
//struct ListNode* reverrse(struct ListNode* start, struct ListNode* finish) {
//    if (start == NULL) return start;
//    struct ListNode* cur = start, * temp = NULL, * prev = NULL;
//    while (cur != finish) {
//        temp = cur->next;
//        cur->next = prev;
//        prev = cur;
//        cur = temp;
//    }
//    return prev;
//}
//struct ListNode* reverseKGroup(struct ListNode* head, int k) {
//    if (head == NULL) return NULL;
//    struct ListNode* a = head, * b = head;
//    for (int i = 0; i < k; i++) {
//        if (b == NULL) return head;
//        b = b->next;
//    }
//    struct ListNode* newhead = reverrse(a, b);
//    a->next = reverseKGroup(b, k);
//    return newhead;
//}
//
////设计链表
//#define MAX(a, b) ((a) > (b) ? (a) : (b))
//
//typedef struct {
//    struct ListNode* head;
//    int size;
//} MyLinkedList;
//
//struct ListNode* ListNodeCreat(int val) {
//    struct ListNode* node = (struct ListNode*)malloc(sizeof(struct ListNode));
//    node->val = val;
//    node->next = NULL;
//    return node;
//}
//
//MyLinkedList* myLinkedListCreate() {
//    MyLinkedList* obj = (MyLinkedList*)malloc(sizeof(MyLinkedList));
//    obj->head = ListNodeCreat(0);
//    obj->size = 0;
//    return obj;
//}
//
//int myLinkedListGet(MyLinkedList* obj, int index) {
//    if (index < 0 || index >= obj->size) {
//        return -1;
//    }
//    struct ListNode* cur = obj->head;
//    for (int i = 0; i <= index; i++) {
//        cur = cur->next;
//    }
//    return cur->val;
//}
//
//void myLinkedListAddAtIndex(MyLinkedList* obj, int index, int val) {
//    if (index > obj->size) {
//        return;
//    }
//    index = MAX(0, index);
//    obj->size++;
//    struct ListNode* pred = obj->head;
//    for (int i = 0; i < index; i++) {
//        pred = pred->next;
//    }
//    struct ListNode* toAdd = ListNodeCreat(val);
//    toAdd->next = pred->next;
//    pred->next = toAdd;
//}
//
//void myLinkedListAddAtHead(MyLinkedList* obj, int val) {
//    myLinkedListAddAtIndex(obj, 0, val);
//}
//
//void myLinkedListAddAtTail(MyLinkedList* obj, int val) {
//    myLinkedListAddAtIndex(obj, obj->size, val);
//}
//
//void myLinkedListDeleteAtIndex(MyLinkedList* obj, int index) {
//    if (index < 0 || index >= obj->size) {
//        return;
//    }
//    obj->size--;
//    struct ListNode* pred = obj->head;
//    for (int i = 0; i < index; i++) {
//        pred = pred->next;
//    }
//    struct ListNode* p = pred->next;
//    pred->next = pred->next->next;
//    free(p);
//}
//
//void myLinkedListFree(MyLinkedList* obj) {
//    struct ListNode* cur = NULL, * tmp = NULL;
//    for (cur = obj->head; cur;) {
//        tmp = cur;
//        cur = cur->next;
//        free(tmp);
//    }
//    free(obj);
//}
//
////两两交换链表节点
//struct ListNode* swapPairs(struct ListNode* head) {
//    struct ListNode H;
//    H.next = head;
//
//    struct ListNode* cur = &H;
//    while (cur->next && cur->next->next)
//    {
//        struct ListNode* node1 = cur->next;
//        struct ListNode* node2 = cur->next->next;
//        cur->next = node2;
//        node1->next = node2->next;
//        node2->next = node1;
//        cur = node1;
//    }
//
//    return H.next;
//}
//
////删除链表中重复元素
//struct ListNode* swapPairs(struct ListNode* head) {
//    struct ListNode H;
//    H.next = head;
//
//    struct ListNode* cur = &H;
//    while (cur->next && cur->next->next)
//    {
//        struct ListNode* node1 = cur->next;
//        struct ListNode* node2 = cur->next->next;
//        cur->next = node2;
//        node1->next = node2->next;
//        node2->next = node1;
//        cur = node1;
//    }
//
//    return H.next;
//}
//
////两数相加
//struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
//    struct ListNode* head = NULL, * tail = NULL;
//    int count = 0;
//    while (l1 || l2)
//    {
//        int n1 = l1 ? l1->val : 0;
//        int n2 = l2 ? l2->val : 0;
//        int sum = n1 + n2 + count;
//        if (!head)
//        {
//            head = tail = malloc(sizeof(struct ListNode));
//            head->val = sum % 10;
//            tail->val = sum % 10;
//            tail->next = NULL;
//        }
//        else {
//            tail->next = malloc(sizeof(struct ListNode));
//            tail->next->val = sum % 10;
//            tail = tail->next;
//            tail->next = NULL;
//        }
//        count = sum / 10;
//        if (l1)
//        {
//            l1 = l1->next;
//        }
//        if (l2)
//        {
//            l2 = l2->next;
//        }
//    }
//    if (count > 0)
//    {
//        tail->next = malloc(sizeof(struct ListNode));
//        tail->next->val = count;
//        tail = tail->next;
//        tail->next = NULL;
//    }
//    return head;
//}
