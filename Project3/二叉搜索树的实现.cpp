#define _CRT_SECURE_NO_WARNINGS
#include "标头.h"
//template <class T>
//struct BSNode {
//	BSNode(const T&data=T())
//		:_left(nullptr),_right(nullptr),_val(data)
//	{}
//	BSNode<T>* _left;
//	BSNode<T>* _right;
//	T _val;
//};
//template <class T>
//class BSTree {
//public:
//	typedef BSNode<T> node;
//	BSTree()
//		:_root(nullptr)
//	{}
//	~BSTree() {
//		//二叉树的析构
//		Destory(_root);
//	}
//
//	//拷贝构造
//	BSTree(const BSTree<T>& bs) {
//		_root = Copy(bs._root);
//	}
//	bool find(const T& val) {
//		if (_root == nullptr) return false;
//		node* cur = _root;
//		while (cur) {
//			if (cur->_val < val) {
//				cur = cur->_right;
//			}
//			else if (cur->_val > val) {
//				cur = cur->_left;
//			}
//			else return true;
//		}
//		return false;
//	}
//	bool Insert(const T& val) {
//		//首先是查找在不在树里
//		if (_root == nullptr) {
//			node* newnode = new node(val);
//			_root = newnode;
//		}
//		else {
//			if (find(val) == true) return false;
//			node* pre = nullptr;
//			node* cur = _root;
//			while (cur) {
//				if (cur->_val > val) {
//					pre = cur;
//					cur = cur->_left;
//				}
//				else if (cur->_val < val) {
//					pre = cur;
//					cur = cur->_right;
//				}
//			}
//			cur = new node(val);
//			if (pre->_val > val) {
//				//zuo
//				pre->_left = cur;
//			}
//			else {
//				//you
//				pre->_right = cur;
//			}
//			return true;
//		}
//		
//	}
//	bool Erase(const T& val) {
//		if (_root == nullptr) return false;
//		if (find(val)==false) return false;
//		node* cur = _root;
//		node* pre = nullptr;
//		while (cur) {
//			if (cur->_val > val) {
//				pre = cur;
//				cur = cur->_left;
//			}
//			else if (cur->_val < val) {
//				pre = cur;
//				cur = cur->_right;
//			}
//			else {
//				if (!cur->_left && !cur->_right) {
//					if (pre->_val > val) {
//						delete(cur);
//						pre->_left = nullptr;
//					}
//					else {
//						delete(cur);
//						pre->_right = nullptr;
//					}
//				}
//				else if (!cur->_left)
//				{
//					//删cur右
//					delete(cur->_right);
//					cur->_right = nullptr;
//				}
//				else if (!cur->_right) {
//					delete(cur->_left);
//					cur->_left = nullptr;
//				}
//				else {
//					//找到cur左树最大节点 或者是右树最小节点
//					//这里找右树最小
//					node* pminRight = cur;
//					node* minRight = cur->_right;
//					while (minRight->_left)
//					{
//						pminRight = minRight;
//						minRight = minRight->_left;
//					}
//
//					cur->_val = minRight->_val;//替换
//
//					//找到最小之后如果是minRight连在左边 证明minRight左边没有比他还小的节点 也就是只有minRight右孩子不为空
//					if (pminRight->_left == minRight)
//					{
//						//连在右孩子上
//						pminRight->_left = minRight->_right;
//					}
//					//minRight在pmin右侧  说明pmin左侧没有节点  右侧要连min的右 因为min是最小的所以不可能有左孩子
//					else
//					{
//						pminRight->_right = minRight->_right;
//					}
//
//					delete minRight;
//				}
//
//				return true;
//				}
//			}
//		return false;
//		}
//	void InOrder() {
//		_InOrder(_root);
//	}
//
//protected:
//	node* Copy( node* bs) {
//		if (bs == nullptr) return nullptr;
//		node* newnode = new node(bs->_val);
//		newnode->_left = Copy(bs->_left);
//		newnode->_right = Copy(bs->_right);
//		return newnode;
//	}
//	void _InOrder( node* root) {
//		if (root == nullptr)
//			return;
//
//		_InOrder(root->_left);
//		cout << root->_val << " ";
//		_InOrder(root->_right);
//	}
//	void Destory(node*& root) {
//		if (root == nullptr) return;
//		Destory(root->_left);
//		Destory(root->_right);
//
//		delete root;
//		root = nullptr;
//	}
//private:
//	node* _root;
//};
//void test()
//{
//	BSTree<int> bs;
//	bs.Insert(10);
//	bs.Insert(17);
//	bs.Insert(16);
//	bs.Insert(20);
//	bs.Insert(23);
//	BSTree<int> a(bs);
//	bs.InOrder();
//	cout << endl;
//	a.InOrder();
//	cout << endl;
//	//bs.Erase(20);
//	//bs.InOrder();
//	//cout << endl;
//}


template <class K,class V>
struct BSNode {
	BSNode(const K& key = K(),const V& val=V())
		:_left(nullptr), _right(nullptr), _val(val),_key(key)
	{}
	BSNode<K,V>* _left;
	BSNode<K,V>* _right;
	K _key;
	V _val;
};
template <class K, class V>
class BSTree {
public:
	typedef BSNode<K,V> node;
	BSTree()
		:_root(nullptr)
	{}
	~BSTree() {
		//二叉树的析构
		Destory(_root);
	}

	//拷贝构造
	BSTree(const BSTree<K,V>& bs) {
		_root = Copy(bs._root);
	}
	bool find(const K& key) {
		if (_root == nullptr) return false;
		node* cur = _root;
		while (cur) {
			if (cur->_key < key) {
				cur = cur->_right;
			}
			else if (cur->_key > key) {
				cur = cur->_left;
			}
			else return true;
		}
		return false;
	}
	bool Insert(const K& key,const V& val) {
		//首先是查找在不在树里
		if (_root == nullptr) {
			node* newnode = new node(key,val);
			_root = newnode;
		}
		else {
			if (find(key) == true) return false;
			node* pre = nullptr;
			node* cur = _root;
			while (cur) {
				if (cur->_key > key) {
					pre = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key) {
					pre = cur;
					cur = cur->_right;
				}
			}
			cur = new node(key,val);
			if (pre->_key > key) {
				//zuo
				pre->_left = cur;
			}
			else {
				//you
				pre->_right = cur;
			}
			return true;
		}
	}
	bool Erase(const K& key,const V& val) {
		if (_root == nullptr) return false;
		if (find(key) == false) return false;
		node* cur = _root;
		node* pre = nullptr;
		while (cur) {
			if (cur->_key > key) {
				pre = cur;
				cur = cur->_left;
			}
			else if (cur->_key < key) {
				pre = cur;
				cur = cur->_right;
			}
			else {
				if (!cur->_left && !cur->_right) {
					if (pre->_key > key) {
						delete(cur);
						pre->_left = nullptr;
					}
					else {
						delete(cur);
						pre->_right = nullptr;
					}
				}
				else if (!cur->_left)
				{
					//删cur右
					delete(cur->_right);
					cur->_right = nullptr;
				}
				else if (!cur->_right) {
					delete(cur->_left);
					cur->_left = nullptr;
				}
				else {
					//找到cur左树最大节点 或者是右树最小节点
					//这里找右树最小
					node* pminRight = cur;
					node* minRight = cur->_right;
					while (minRight->_left)
					{
						pminRight = minRight;
						minRight = minRight->_left;
					}

					cur->_key = minRight->_key;//替换

					//找到最小之后如果是minRight连在左边 证明minRight左边没有比他还小的节点 也就是只有minRight右孩子不为空
					if (pminRight->_left == minRight)
					{
						//连在右孩子上
						pminRight->_left = minRight->_right;
					}
					//minRight在pmin右侧  说明pmin左侧没有节点  右侧要连min的右 因为min是最小的所以不可能有左孩子
					else
					{
						pminRight->_right = minRight->_right;
					}

					delete minRight;
				}

				return true;
			}
		}
		return false;
	}
	void InOrder() {
		_InOrder(_root);
	}

protected:
	node* Copy(node* bs) {
		if (bs == nullptr) return nullptr;
		node* newnode = new node(bs->_key,bs->_val);
		newnode->_left = Copy(bs->_left);
		newnode->_right = Copy(bs->_right);
		return newnode;
	}
	void _InOrder(node* root) {
		if (root == nullptr)
			return;

		_InOrder(root->_left);
		cout << root->_key << ":"<<root->_val<<endl;
		_InOrder(root->_right);
	}
	void Destory(node*& root) {
		if (root == nullptr) return;
		Destory(root->_left);
		Destory(root->_right);

		delete root;
		root = nullptr;
	}
private:
	node* _root;
};


//int main()
//{
//	BSTree<string, string> bs;
//	bs.Insert("hello", "你好");
//	bs.Insert("best", "最");
//	bs.InOrder();
//	cout << endl;
//	cout << bs.find("best") << endl;
//	cout << bs.find("jj") << endl;
//}