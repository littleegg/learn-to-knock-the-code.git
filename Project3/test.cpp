#define _CRT_SECURE_NO_WARNINGS

#include "标头.h"

//class Person {
//public:
//	virtual ~Person() { cout << "~Person()" << endl; }
//};
//class Student : public Person {
//public:
//	virtual ~Student() { cout << "~Student()" << endl; }
//};
////// 只有派生类Student的析构函数重写了Person的析构函数，下面的delete对象调用析构函
////数，才能构成多态，才能保证p1和p2指向的对象正确的调用析构函数。
//int main()
//{
//	Person* p1 = new Person;
//	Person* p2 = new Student;
//	
//	delete p1;
//	delete p2;
//	return 0;
//}

//class Person {
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//};
//class Student : public Person {
//public:
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//	/*注意：在重写基类虚函数时，派生类的虚函数在不加virtual关键字时，虽然也可以构成重写(因
//	为继承后基类的虚函数被继承下来了在派生类依旧保持虚函数属性),但是该种写法不是很规范，不建议
//	这样使用*/
//	/*void BuyTicket() { cout << "买票-半价" << endl; }*/
//};
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
//int main()
//{
//	Person ps;
//	Student st;
//	Func(ps);
//	Func(st);
//	return 0;
//}


//
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//int main()
//{
//	Base b;
//	cout << sizeof(b) << endl;
//}

//class Base{
//public:
// virtual void func1() { cout << "Base::func1" << endl; }
// virtual void func2() { cout << "Base::func2" << endl; }
//private:
// int a;
//};
//class Derive :public Base {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//	virtual void func4() { cout << "Derive::func4" << endl; }
//private:
//	int b;
//};
//
//typedef void(*VFPTR) ();
//void PrintVTable(VFPTR vTable[])
//{
//	// 依次取虚表中的虚函数指针打印并调用。调用就可以看出存的是哪个函数
//	cout << " 虚表地址>" << vTable << endl;
//	for (int i = 0; vTable[i] != nullptr; ++i)
//	{
//		printf(" 第%d个虚函数地址 :0X%x,->", i, vTable[i]);
//		VFPTR f = vTable[i];
//		f();
//	}
//	cout << endl;
//}
//int main()
//{
//	Base b;
//	Derive d;
//	// 思路：取出b、d对象的头4bytes，就是虚表的指针，前面我们说了虚函数表本质是一个存虚函数
//	//指针的指针数组，这个数组最后面放了一个nullptr
//		// 1.先取b的地址，强转成一个int*的指针
//		// 2.再解引用取值，就取到了b对象头4bytes的值，这个值就是指向虚表的指针
//		// 3.再强转成VFPTR*，因为虚表就是一个存VFPTR类型(虚函数指针类型)的数组。
//		// 4.虚表指针传递给PrintVTable进行打印虚表
//		// 5.需要说明的是这个打印虚表的代码经常会崩溃，因为编译器有时对虚表的处理不干净，虚表最
//	/*	后面没有放nullptr，导致越界，这是编译器的问题。我们只需要点目录栏的 - 生成 - 清理解决方案，再
//		编译就好了。*/
//	VFPTR * vTableb = (VFPTR*)(*(int*)&b);  //把指向虚表的指针传递给VFPTR
//	PrintVTable(vTableb);
//	VFPTR* vTabled = (VFPTR*)(*(int*)&d);
//	PrintVTable(vTabled);
//	return 0;
//}



//class  Base
//{
//public:
//	virtual void func1() { cout << "Base::func1" << endl; }
//	virtual void func2() { cout << "Base::func2" << endl; }
//};
//class Derive :public Base {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//	virtual void func4() { cout << "Derive::func4" << endl; }
//};

//class  Base1
//{
//public:
//	virtual void func1() { cout << "Base1::func1" << endl; }
//	virtual void func2() { cout << "Base1::func2" << endl; }
//private:
//	int _a;
//};
//class Base2
//{
//public:
//	virtual void func3() { cout << "Base2::func3" << endl; }
//	virtual void func4() { cout << "Base2::func4" << endl; }
//private:
//	int _b;
//};
//class Derive :public Base1 ,public Base2 {
//	virtual void func1() { cout << "Derive::func1" << endl; } //
//	 
//	virtual void func3() { cout << "Derive::func3" << endl; } //
//
//	virtual void func5() { cout << "Derive::func5" << endl; } //
//	virtual void func6() { cout << "Derive::func6" << endl; } //
//private:
//	int _d;
//};
//
//typedef void(*VFPTR)();
//void Printf(VFPTR v[]) {
//	cout << "虚函数指针的地址是" << v << endl;
//	for (int i = 0; v[i] != nullptr; i++) {
//		printf("第%d个虚函数的地址是：0x%x", i, v[i]);
//		v[i]();
//	}
//	cout << endl;
//}
//int main()
//{
//	Derive d;
//
//	VFPTR* son1 = (VFPTR*)(*(int*)&d);
//	VFPTR* son2 = (VFPTR*)(*(int*)((char*)&d+sizeof(Base1)));
//
//
//	Printf(son1);
//	Printf(son2);
//	return 0;
//}


//#include <iostream>
//#include <cmath>
//#include <vector>
//using namespace std;
//bool is(int a) {
//    for (int i = 2; i <= sqrt(a); i++) {
//        if (a % i == 0) return false;
//    }
//    return true;
//}
//void f(int& n, vector<int>& v) {
//    if (n == 1) return;
//    for (int i = 2; i < n; i++) {
//        if (n % i == 0 && is(i)) {
//            v.push_back(i);
//            f(n / i, v);
//        }
//    }
//
//}
//int main() {
//    int n;
//    while (cin >> n) {
//        vector<int> v;
//        f(n, v);
//        cout << n << "=";
//        for (int i = 0; i < v.size() - 1; i++) {
//            cout << v[i] << "*";
//        }
//        cout << v[v.size() - 1] << endl;
//    }
//}


int main()
{
	cout << min(-1,-6) << endl;
}