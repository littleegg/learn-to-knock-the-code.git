#define _CRT_SECURE_NO_WARNINGS

#include <iterator>
class Solution {
public:
    bool hasCycle(ListNode* head) {
        //快慢指针
        ListNode* fast = head;
        ListNode* slow = head;
        if (!fast) return false;

        while (fast->next && fast->next->next)
        {
            slow = slow->next;
            fast = fast->next->next;
            if (slow == fast) return true;
        }
        return false;
    }
};

class Solution {
public:
    ListNode* EntryNodeOfLoop(ListNode* pHead) {
        ListNode* fast = pHead;
        ListNode* slow = pHead;
        if (!fast) return NULL;
        while (fast->next && fast->next->next)
        {
            fast = fast->next->next;
            slow = slow->next;
            if (fast == slow)
                break;
        }
        if (!fast->next || !fast->next->next) return NULL;  //在前面那个题目这里直接返回false，这里应该是返回NULL
        slow = pHead;
        while (fast != slow)
        {
            fast = fast->next;
            slow = slow->next;
        }
        return fast;
    }
};


ListNode* FindKthToTail(ListNode* pHead, int k) {
    // write code here
    if (!pHead) return NULL;
    ListNode* cur = pHead;
    int n = 0;  //记录节点总数
    while (cur)
    {
        cur = cur->next;
        n++;
    }
    if (k > n) return NULL; //根据题意直接返回空
    if (k == n) return pHead;  //直接是整个链表
    k %= n; //保证k合法
    int tmp = n - k; //前n-k的节点个数
    cur = pHead;
    while (--tmp) //走到要返回节点之前
    {
        cur = cur->next;
    }
    return cur->next;
}
};


ListNode* FindKthToTail(ListNode* pHead, int k) {
    // write code here
    if (!pHead || k <= 0) return NULL;  //空链表或者k不合法直接返回
    ListNode* fast = pHead;
    ListNode* slow = pHead;

    while (fast && k) //fast不能越界，一直走到k=0
    {
        fast = fast->next;
        k--;
    }
    if (k > 0) return NULL;  //如果fast==NULL，但是k！=0，即k>n,直接返回
    while (fast)  //现在特殊情况都排除，直接按照思路写
    {
        slow = slow->next;
        fast = fast->next;
    }
    return slow;
}
};

ListNode* removeNthFromEnd(ListNode* head, int n) {
    // write code here
    if (!head || n <= 0) return head;//删除头结点相当于没删
    ListNode* fast = head;
    ListNode* slow = head;
    while (fast && n--)
    {
        fast = fast->next;
    }
    if (n > 0) return head; //删除头结点相当于没删
    ListNode* pre = NULL; //保存要删除之前的节点
    while (fast)
    {
        pre = slow;
        fast = fast->next;
        slow = slow->next;
    }
    //pre slow
    if (pre)
    {
        pre->next = slow->next;
    }
    else
    {
        head = head->next;
    }

    return head;

}
};


include <unistd.h>
class Solution {
public:
    int finlen(ListNode* head)
    {
        int n = 0;
        while (head)
        {
            head = head->next;
            n++;
        }
        return n;
    }
    ListNode* FindFirstCommonNode(ListNode* pHead1, ListNode* pHead2) {
        int len1 = finlen(pHead1);
        int len2 = finlen(pHead2);

        if (len1 > len2)
        {
            int gap = len1 - len2;
            while (gap--)
            {
                pHead1 = pHead1->next;
            }
        }
        else
        {
            int gap = len2 - len1;
            while (gap--)
            {
                pHead2 = pHead2->next;
            }
        }
        while (pHead1 != pHead2)
        {
            pHead1 = pHead1->next;
            pHead2 = pHead2->next;
        }
        return pHead2;
    }
};
