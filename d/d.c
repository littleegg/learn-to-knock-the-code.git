#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
void reverse(char* arr)
{
	int len = strlen(arr);
	char* left = arr;
	char* right = arr + len - 1;
	char tmp = *left;
	*left = *right;
	*right = '\0';
	reverse(arr + 1);
	*right = tmp;
}
int main()
{
	char arr[]="abcdef";
	reverse(arr);
	printf("%s", arr);
}