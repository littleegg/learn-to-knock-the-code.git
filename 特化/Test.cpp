#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include <array>
#include <string>
using namespace std;

// #define N 1000

// 静态数组
// 非类型模板参数 -- 常量
template<class T, size_t N>
class Array
{
private:
	T _a[N];
};

//int main()
//{
//	Array<int, 10> a1;        // 10
//	Array<double, 1000> a2;     // 1000
//
//	return 0;
//}

//int main()
//{
//	int a1[10];
//	array<int, 10> a2;
//	array<int, 100> a3;
//
//	// 对于越界的检查
//	// 越界读不检查
//	// 越界写  抽查
//	cout << a1[10] << endl;
//	cout << a1[11] << endl;
//
//	//a1[10] = 0;
//	a1[15] = 0;
//
//	// C++11  array
//	cout << a2[10] << endl;
//	cout << a2[11] << endl;
//
//	//a2[10] = 0;
//	//a2[15] = 0;
//
//	return 0;
//}

//template<class T, double N>
//void Func(const T& x)
//{
//	cout << N << endl;
//}

//template<class T, size_t N>
//void Func(const T& x)
//{
//	cout << N << endl;
//}
//
//int main()
//{
//	int N = 100;
//	Func<int, N>(1);
//
//	return 0;
//}

class Date
{
public:
	Date(int year = 1900, int month = 1, int day = 1)
		: _year(year)
		, _month(month)
		, _day(day)
	{}

	bool operator<(const Date& d)const
	{
		return (_year < d._year) ||
			(_year == d._year && _month < d._month) ||
			(_year == d._year && _month == d._month && _day < d._day);
	}

	bool operator>(const Date& d)const
	{
		return (_year > d._year) ||
			(_year == d._year && _month > d._month) ||
			(_year == d._year && _month == d._month && _day > d._day);
	}

	friend ostream& operator<<(ostream& _cout, const Date& d)
	{
		_cout << d._year << "-" << d._month << "-" << d._day;
		return _cout;
	}

private:
	int _year;
	int _month;
	int _day;
};

// 函数模板 -- 参数匹配
template<class T>
bool Less(T left, T right)
{
	return left < right;
}

// 针对某些类型进行特殊处理 -- Date*
//template<>
//bool Less<Date*>(Date* left, Date* right)
//{
//	return *left < *right;
//}

bool Less(Date* left, Date* right)
{
	return *left < *right;
}

//int main()
//{
//	cout << Less(1, 2) << endl;   // 可以比较，结果正确
//
//	Date d1(2022, 7, 7);
//	Date d2(2022, 7, 8);
//	cout << Less(d1, d2) << endl;  // 可以比较，结果正确
//
//	Date* p1 = &d1;
//	Date* p2 = &d2;
//	cout << Less(p1, p2) << endl;  // 可以比较，结果错误
//
//	return 0;
//}

template<class T1, class T2>
class Data
{
public:
	Data() { cout << "Data<T1, T2>" << endl; }
private:
	T1 _d1;
	T2 _d2;
};

// 全特化
template<>
class Data<double, char>
{
public:
	Data() { cout << "Data<double, char>" << endl; }
};

// 半特化、偏特化
template<class T1>
class Data<T1, char>
{
public:
	Data() { cout << "Data<T1, char>" << endl; }
};

// 参数类型进一步限制
template<class T1, class T2>
class Data<T1*, T2*>
{
public:
	Data() { cout << "Data<T1*, T2*>" << endl; }
};

template<class T1, class T2>
class Data<T1&, T2&>
{
public:
	Data() { cout << "Data<T1&, T2&>" << endl; }
};

//int main()
//{
//	Data<int, int> d1;
//	Data<double, double> d2;
//	Data<double, char> d3;
//	Data<char, char> d4;
//
//	Data<char*, char*> d5;
//	Data<char, int*> d6;
//	Data<double*, int*> d7;
//
//	Data<double&, int&> d8;
//
//	return 0;
//}