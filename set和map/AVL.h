#pragma once
template<class K,class V>
struct AVLNode
{
    pair<K, V> _kv;
    AVLNode <K,V>* left;
    AVLNode <K, V>* right;
    AVLNode  <K, V>* parent;
    int bf; //平衡因子
    AVLNode(const pair<K,V>& kv)
        :_kv(kv)
        ,left(nullptr)
        ,right(nullptr)
        ,parent(nullptr)
        ,bf(0)
    {}
};

template<class K,class V>
struct AVLtree
{
    typedef AVLNode<K, V> Node;
    bool insert(const pair<K, V>& kv)
    {
        if (!_root)
        {
            _root = new Node(kv);
            return true;
        }
        Node* parent = nullptr;
        Node* cur = _root;
        while (cur)
        {
            if (cur->_kv.first < kv.first)
            {
                parent = cur;
                cur = cur->right;
            }
            else if (cur->_kv.first > kv.first)
            {
                parent = cur;
                cur = cur->left;
            }
            else
                return false; //在AVL中有这个节点存在，那么直接false
        }
        //现在走到了该插入的位置（cur）
        cur = new Node(kv);
        if (parent->_kv.first < kv.first)
        {
            parent->right = cur;
            cur->parent = parent;
        }
        else
        {
            parent->left = cur;
            cur->parent = parent;
        }
        //现在插入结束，但是不一定符合平衡树,现在要根据插入的位置调整二叉树各个节点的平衡因子
        while (parent)
        {
            if (parent->right == cur)
                parent->bf++;
            else if (parent->left == cur)
                parent->bf--;
            if (parent->bf == 0)
                return true; //说明插入之后两侧高度正好相等，很好不许要调整
            else if (abs(parent->bf )== 1 )
            {
                //说明插入之前的parent->_kv==0，说明以双亲为根的二叉树的高度增加了一层，因此需要继续向上调整
                cur = parent;
                parent = parent->parent;
            }
            else if (abs(parent->bf) == 2)
            {
               //旋转
                if (parent->bf == 2 && cur->bf == 1) //说明插在较高右子树的右侧
                {
                    RotateL(parent); //右右=左单旋
                }
                else if (parent->bf == -2 && cur->bf == -1) //插在较高左子树的左侧
                {
                    RotateR(parent); //左左=右单旋
                }
                else if (parent->bf == -2 && cur->bf == 1) //插在较高左子树的右侧
                {
                    RotateLR(parent); //左右=左右旋
                }
                else if (parent->bf == 2 && cur->bf == -1) //插在较高右子树的左侧
                {
                    RotateRL(parent); //右左=右左旋
                }
                else
                {
                    assert(false);
                }

                break;
            }
            else //如果高度差大于等于3，那么说明无药可救了
            {
                assert(false); 
            }
        }
        return true;
    }
    void RotateL(Node* parent)
    {
        Node* subR = parent->right; //首先定义出节点，代表右子树
        Node* subRL = subR->left; //右子树的左孩子
        parent->right = subRL;
        if (!subRL) //如果右子树的左边是空，那么直接把父节点插入到空的地方
            subR->left = parent;
        Node* pparent = parent->parent; //祖父节点
        subR->left = parent;//左树不是空
        parent->parent = subR; //把原来的父节点的父改成subR
        if (!pparent) //如果祖父节点是空
        {
            _root = subR; //subR变成根
            subR->parent = pparent;
        }
        else //看subR到底应该是祖父节点的哪个孩子
        {
            if (pparent->left == parent)
                parent->left = subR;
            else
                parent->right = subR;
            subR->parent = pparent;
        }
        parent->bf = subR->bf = 0; //根据画图，我们知道最后两个的bf都是0
    }
    void RotateR(Node* parent)
    {
        Node* subL = parent->left;
        Node* subLR = subL->right;
        parent->left = subLR;
        if (!subLR)
            subL->right = parent;
        Node* pparent = parent->parent;
        subL->right = parent;
        parent->parent = subL;
        if (!pparent)
        {
            _root = subL;
            _root->parent = nullptr;
        }
        else
        {
          if (pparent->left == parent)
            pparent->left = subL;
        else
            pparent->right = subL;
        subL->parent = pparent;
        }
     
        parent->bf = subL->bf = 0;
    }
    void RotateRL(Node* parent)
    {
        Node* subR = parent->right;
        Node* subRL = subR->left;
        int bf = subRL->bf;

        RotateR(subR);
        RotateL(parent);

        if (bf == 1)
        {
            subR->bf = 0;
            subRL->bf = 0;
            parent->bf = -1;
        }
        else if (bf == -1)
        {
            subRL->bf = 0;
            subR->bf = 1;
            parent->bf = 0;
        }
        else if (bf == 0)
        {
            subR->bf = 0;
            subRL->bf = 0;
            parent->bf = 0;
        }
        else
            assert(false);
    }
    void RotateLR(Node* parent)
    {
        Node* subL= parent->right;
        Node* subLR = subL->right;
        int bf = subLR->bf;

        RotateL(subL);
        RotateR(parent);

        if (1 == bf)
        {
            parent->bf = 0;
            subL->bf = -1;
            subLR->bf = 0;
        }
        else if (-1 == bf)
        {
            subLR->bf = 0;
            subL->bf = 0;
            parent->bf = 1;
        }
        else if (bf == 0)
        {
            subL->bf = 0;
            subLR->bf = 0;
            parent->bf = 0;
        }
        else
            assert(false);
    }
    void Inorder()
    {
        _Inorder(_root);
    }

    void _Inorder(Node* root)
    {
        if (!root)
            return;

        _Inorder(root->left);
        cout << root->_kv.first << ":" << root->_kv.second << endl;
        _Inorder(root->right);
    }

    int Height(Node* root)
    {
        if (root == nullptr)
            return 0;

        int lh = Height(root->left);
        int rh = Height(root->right);
        return lh > rh ? lh + 1 : rh + 1; //返回左右子树中较大的+1（加上根）
    }
    bool IsBalance()
    {
        return IsBalance(_root);
    }

    bool IsBalance(Node* root)
    {
        if (root == nullptr)
        {
            return true;
        }

        int leftHeight = Height(root->left);
        int rightHeight = Height(root->right);

        if (rightHeight - leftHeight != root->bf)
        {
            cout << root->_kv.first << "平衡因子异常" << endl;
            return false;
        }

        return abs(rightHeight - leftHeight) < 2
            && IsBalance(root->left)
            && IsBalance(root->right);
    }
private:
    Node* _root=nullptr;
};