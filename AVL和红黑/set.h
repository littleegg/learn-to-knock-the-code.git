#pragma once
#include "RBTree.h"
namespace wrt {
	template<class K>
	class set {
		struct SetKeyofT {
			const K& operator()(const K& key) {
				return key;
			}
		};
	public:
		//编译器无法识别RBTree<K, K, SetKeyofT>是一个静态变量还是一个类型
		//类模板里面取RBTree<K, K, SetKeyofT> 一定要加上typename 
		//目的是告诉编译器这是一个类型 等类模板实例化之后再去取这个类型


		//由于set不能修改K也就是value 普通迭代器也是用const迭代器实现的
		typedef typename  RBTree<K, K, SetKeyofT>::const_iterator  iterator;
		typedef typename  RBTree<K, K, SetKeyofT>::const_iterator  const_iterator;


		iterator begin()
		{
			//但是这里有一个编译错误
			return _t.begin();
		}
		iterator end() {
			return _t.end();
		}
		pair<iterator, bool> insert(const K& key)
		{
			return _t.Insert(key);
		}
	private:
		RBTree<K,K,SetKeyofT> _t;
	};
	void test_set() {
		set<int> s;
		int a[] = {4,1,7,88,5,2,6,4,7,8,2,3,0};
		
		for (auto e : a) {
			s.insert(e);
		}

		set<int>::iterator it= s.begin();
		while (it != s.end()) {
			cout << *it << " ";
			//*it=1;
			++it;
		}
		cout << endl;
	}
}
