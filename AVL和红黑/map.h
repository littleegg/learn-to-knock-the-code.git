﻿#pragma once
#include "RBTree.h"
namespace wrt {
	template<class K,class V>
	class map{
	public:
		struct MapKeyofT {
			const K& operator()(const pair<const K,V>& kv) {
				return kv.first;
			}
		};
		
		typedef typename RBTree<K, pair<const K, V>, MapKeyofT>::iterator iterator;
		typedef typename RBTree<K, pair<const K, V>, MapKeyofT>::const_iterator const_iterator;

		iterator begin()
		{
			return _t.begin();
		}

		iterator end()
		{
			return _t.end();
		}
		V& operator[](const  K& key)
		{
			pair<iterator,bool> ret=_t.Insert(make_pair(key, V()));
			return ret.first->second;
		}
		pair<iterator, bool> insert(const pair<const K, V>& kv)
		{
			return _t.Insert(kv);
		}
	private:
		RBTree<K, pair<const K, V>,MapKeyofT> _t;

	};
	void test_map1()
	{
		map<string, string> dict;
		dict.insert(make_pair("sort", "排序"));
		dict.insert(make_pair("string", "字符串"));
		dict.insert(make_pair("count", "计数"));
		dict.insert(make_pair("string", "字符"));//插入失败

		map<string, string>::iterator it = dict.begin();
		while (it != dict.end())
		{
			cout << it->first << ":" << it->second << endl;
			it->first = "sdh";
			it->second = "222";
			++it;
		}
		cout << endl;

		for (auto& kv : dict)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
		cout << endl;
	}
	void test_map2()
	{
		string arr[] = { "hello","hello","hi"};
		map<string, int> countMap;
		for (auto& e : arr)
		{
			countMap[e]++;
		}

		for (auto& kv : countMap)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
	}

}