#pragma once
#include "标头.h"

template<class K,class V>
struct AVLNode {
	AVLNode<K,V>* _left;
	AVLNode<K,V>* _right;
	AVLNode<K, V>* _parent;
	pair<K, V> _kv;
	int _bf; //默认是右-左
	AVLNode(const pair<K,V>& kv)
		:_left(nullptr),_right(nullptr),_parent(nullptr),_kv(kv),_bf(0)
	{}
};
template<class K,class V>
class AVLTree {
public:
	typedef AVLNode<K, V> node;

	void RotateL(node* pre){
		//subR  subRL  pp
		//pre_bf = 2 && cur_bf  = 1
		//要注意的是subRL可能是空  pre的pre可能是空（此时检索到根节了）
		node* subR = pre->_right;
		node* subRL = subR->_left;
		pre->_right = subRL;
		if (subRL) subRL->_parent = pre;
		node* pp = pre->_parent;

		subR->_left = pre;
		pre->_parent = subR;

		if (pp == nullptr) {
			_root = subR;
			_root->_parent = nullptr;
		}
		else {
			if (pp->_left == pre) {
				pp->_left = subR;
			}
			else {
				pp->_right = subR;
			}
			subR->_parent = pp;
		}
		pre->_bf = 0; subR->_bf = 0;
	}
	void RotateR(node* pre) {
		//subL  subLR pp
		//pre_bf=-2  cur_bf=-1
		node* subL = pre->_left;
		node* subLR = subL->_right;
		pre->_left = subLR;
		if (subLR) subLR->_parent = pre;
		node* pp = pre->_parent;

		subL->_right = pre;
		pre->_parent = subL;

		if (pre ==_root) {
			_root = subL;
			_root->_parent = nullptr;
		}
		else {
			if (pp->_left == pre) pp->_left = subL;
			else pp->_right = subL;
			subL->_parent = pp;
		}
		subL->_bf = pre->_bf = 0;
	}
	void RotateLR(node* pre) {
		//subL  subLR  subLR的bf是多少
		node* subL = pre->_left;
		node* subLR = subL->_right;
		int bf = subLR->_bf;
		RotateL(subL);
		RotateR(pre);
		//插入之前是不平衡的
		if (bf == 1) {
			//肯定是插在右侧 原来的subLR->_right可能是空
			pre->_bf = 0;
			subL->_bf = -1;
			subLR->_bf = 0;
		}
		else if (bf = -1) {
			//插在左侧 原来的subLR->_right可能是空
			pre->_bf = 1;
			subL->_bf = 0;
			subLR->_bf = 0;
		}
		//插入之后就是平衡的
		else if (bf == 0)
		{
			pre->_bf = 0;
			subL->_bf = 0;
			subLR->_bf = 0;
		}
		else {
			assert(false);
		}
	}
	void RotateRL(node* pre) {
		node* subR = pre->_right;
		node* subRL = subR->_left;
		int bf = subRL->_bf;
		RotateR(subR);
		RotateL(pre);
		if (bf == 1)
		{
			//插在右侧
			pre->_bf = -1;
			subR->_bf = 0;
			subRL->_bf = 0;
		}
		else if (bf == -1)
		{
			//插在左侧
			pre->_bf = 0;
			subR->_bf = 1;
			subRL->_bf = 0;
		}
		else if (bf == 0)
		{
			//插入之后平衡的
			pre->_bf = 0;
			subR->_bf = 0;
			subRL->_bf = 0;
		}
		else assert(false);
	}


	bool insert(const pair<K, V>& kv)
	{
		//首先插入
		if (_root == nullptr) {
			_root = new node(kv);
			return true;
		}
		node* cur = _root;
		node* pre = nullptr;
		while (cur) {
			if (cur->_kv.first < kv.first) {
				pre = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first) {
				pre = cur;
				cur = cur->_left;
			}
			else return false;
		}
		cur = new node(kv);
		if (pre->_kv.first > kv.first) {
			pre->_left = cur;
		}
		else if (pre->_kv.first < kv.first) {
			pre->_right = cur;
		}
		cur->_parent = pre;
		//检查并且更新平衡因子
		while (pre) {
			//更新
			if (pre->_left == cur) {
				pre->_bf--;
			}
			else pre->_bf++;
			//检查
			if (pre->_bf == 1 || pre->_bf == -1) {
				//继续向上检查更新'
				pre = pre->_parent;
				cur = cur->_parent;
			}
			else if (pre->_bf == 0) break;
			else if(pre->_bf==2 || pre->_bf==-2){
				//旋转更新
				if (pre->_bf == 2 && cur->_bf == 1) //说明插在较高右子树的右侧
				{
					RotateL(pre); //右右=左单旋
				}
				else if (pre->_bf == -2 && cur->_bf == -1) //插在较高左子树的左侧
				{
					RotateR(pre); //左左=右单旋
				}
				else if (pre->_bf == -2 && cur->_bf == 1) //插在较高左子树的右侧
				{
					RotateLR(pre); //左右=左右旋
				}
				else if (pre->_bf == 2 && cur->_bf == -1) //插在较高右子树的左侧
				{
					RotateRL(pre); //右左=右左旋
				}
				else assert(false);
			}
			else {
				// 说明之前结构不对 直接报错
				assert(false);
			}
		}
	}
	int Heigh(node* head) {
		if (head == nullptr) return 0;
		int l = Heigh(head->_left);
		int r = Heigh(head->_right);
		return l > ? (l+ 1 ): (r + 1);
	}
	//判断一个树是不是平衡的  首先平衡因子要正确 并且左右子树都是AVL
	bool IsBalance(node* head)
	{
		if (head == nullptr) return true;
		int lh = Heigh(head->_left);
		int rh = Heigh(head->_right);
		if (rh - lh != head->_bf) return false;
		return abs(rh - lh) < 2 && IsBalance(head->_left) && IsBalance(head->_right);
	}
private:
	node* _root = nullptr;
};
