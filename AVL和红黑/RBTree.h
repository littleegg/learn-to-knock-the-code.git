#pragma once
#include "标头.h"
enum Colour {
	RED,
	BLACK,
};
template<class T>
struct RBNode {
	RBNode<T>* _left;
	RBNode<T>* _right;
	RBNode<T>* _parent;
	T _data;
	Colour _col;
	RBNode(const T& data)
		:_left(nullptr), _right(nullptr)
		, _parent(nullptr),_col(RED)
		,_data(data)
	{}
};
template <class T,class Ref,class Ptr>
struct __RBTreeIterator {
	typedef RBNode<T> node;
	typedef __RBTreeIterator<T, Ref, Ptr> Self;
	node*  _node;
	__RBTreeIterator(node* node) 
		:_node(node)
	{}
	__RBTreeIterator(const __RBTreeIterator<T,T&,T*>& it) //注意这个不是拷贝构造 相当于传入普通迭代器最后返回
		:_node(it._node)
	{}
	Ref  operator*()
	{
		return _node->_data;
	}
	Ptr operator->() {
		return &_node->_data;
	}
	bool operator!=(const Self& s) {
		return _node != s._node;
	}

	Self& operator++() {
		if (_node->_right) {
			//1.右不是空 下一个就是右子树的最左节点
			node* subL = _node->_right;
			while (subL->_left) {
				subL = subL->_left;
			}
			_node = subL;
		}
		else {
			//2.右为空  当前子树完了 沿着到根的路径 找孩子是父亲左的那个祖先
			node* cur = _node;
			node* parent = cur->_parent;
			while (parent && cur == parent->_right) {
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}
	Self& operator--() {
		if (_node->_left) {
			//1.左不是空 下一个就是左子树的最右节点
			node* subR = _node->_left;
			while (subR->_right) {
				subR = subR->_right;
			}
			_node = subR;
		}
		else {
			//2.左为空  找孩子是父亲的右的那个祖先
			node* cur = _node;
			node* parent = cur->_parent;
			while (parent && cur == parent->_left) {
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}
};

template<class K, class T,class KeyofT>
class RBTree
{
public:
	typedef RBNode<T> node;
	~RBTree()
	{
		_Destroy(_root);
		_root = nullptr;
	}
public:
	typedef __RBTreeIterator<T, T&, T*> iterator;
	typedef __RBTreeIterator<T, const T&,const  T*> const_iterator;
	iterator begin() {
		node* cur = _root;
		while (cur && cur->_left) {
			cur = cur->_left;
		}
		return iterator(cur);
	}
	iterator end() {
		return iterator(nullptr);
	}
	const iterator begin() const {
		node* cur = _root;
		while (cur && cur->_left) {
			cur = cur->_left;
		}
		return iterator(cur);
	}
	const iterator end() const {
		return iterator(nullptr);
	}
	node* Find(const K& key){
		node* cur = _root;
		KeyofT kot;
		while (cur) {
			if (kot(cur->_data) < key) {
				cur = cur->_right;
			}
			else if (kot(cur->_data) > key) {
				cur = cur->_left;
			}
			else return cur;
		}
		return nullptr;
	}
	void RotateL(node* pre) {
		//subR  subRL  pp
		//pre_bf = 2 && cur_bf  = 1
		//要注意的是subRL可能是空  pre的pre可能是空（此时检索到根节了）
		node* subR = pre->_right;
		node* subRL = subR->_left;
		pre->_right = subRL;
		if (subRL) subRL->_parent = pre;
		node* pp = pre->_parent;

		subR->_left = pre;
		pre->_parent = subR;

		if (pp == nullptr) {
			_root = subR;
			_root->_parent = nullptr;
		}
		else {
			if (pp->_left == pre) {
				pp->_left = subR;
			}
			else {
				pp->_right = subR;
			}
			subR->_parent = pp;
		}
	}
	void RotateR(node* pre) {
		//subL  subLR pp
		//pre_bf=-2  cur_bf=-1
		node* subL = pre->_left;
		node* subLR = subL->_right;
		pre->_left = subLR;
		if (subLR) subLR->_parent = pre;
		node* pp = pre->_parent;

		subL->_right = pre;
		pre->_parent = subL;

		if (pre == _root) {
			_root = subL;
			_root->_parent = nullptr;
		}
		else {
			if (pp->_left == pre) pp->_left = subL;
			else pp->_right = subL;
			subL->_parent = pp;
		}
	}
	bool IsBalance() {
		if (_root && _root->_col == RED) {
			cout << "根节点颜色是红色" << endl;
			return false;
		}
		int benchmark = 0;
		node* cur = _root;
		while (cur) {
			if (cur->_col == BLACK) ++benchmark;
			cur = cur->_left;
		}
		return _Check(_root, 0, benchmark);
	}
	int Height()
	{
		return _Height(_root);
	}

	pair<iterator, bool> Insert(const T& data)
	{
		if (_root == nullptr)
		{
			_root = new node(data);
			_root->_col = BLACK;

			return make_pair(iterator(_root), true);
		}

		KeyofT kot;
		node* parent = nullptr;
		node* cur = _root;
		while (cur)
		{
			if (kot(cur->_data) < kot(data))
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kot(cur->_data) > kot(data))
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return make_pair(iterator(cur), false);
			}
		}

		cur = new node(data);
		node* newnode = cur;
		if (kot(parent->_data) > kot(data))
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		cur->_parent = parent;

		while (parent && parent->_col == RED)
		{
			node* grandfather = parent->_parent;
			if (grandfather->_left == parent)
			{
				node* uncle = grandfather->_right;
				// 情况1：u存在且为红，变色处理，并继续往上处理
				if (uncle && uncle->_col == RED)
				{
					parent->_col = BLACK;
					uncle->_col = BLACK;
					grandfather->_col = RED;

					// 继续往上调整
					cur = grandfather;
					parent = cur->_parent;
				}
				else // 情况2+3：u不存在/u存在且为黑，旋转+变色
				{
					//     g
					//   p   u
					// c 
					if (cur == parent->_left)
					{
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						//     g
						//   p   u
						//     c
						RotateL(parent);
						RotateR(grandfather);
						cur->_col = BLACK;
						//parent->_col = RED;
						grandfather->_col = RED;
					}

					break;
				}
			}
			else // (grandfather->_right == parent)
			{
				//    g
				//  u   p
				//        c
				node* uncle = grandfather->_left;
				// 情况1：u存在且为红，变色处理，并继续往上处理
				if (uncle && uncle->_col == RED)
				{
					parent->_col = BLACK;
					uncle->_col = BLACK;
					grandfather->_col = RED;

					// 继续往上调整
					cur = grandfather;
					parent = cur->_parent;
				}
				else // 情况2+3：u不存在/u存在且为黑，旋转+变色
				{
					//    g
					//  u   p
					//        c
					if (cur == parent->_right)
					{
						RotateL(grandfather);
						grandfather->_col = RED;
						parent->_col = BLACK;
					}
					else
					{
						//    g
						//  u   p
						//    c
						RotateR(parent);
						RotateL(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}

					break;
				}
			}
		}

		_root->_col = BLACK;

		return make_pair(iterator(newnode), true);;
	}
private:
	void _Destroy(node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		_Destroy(root->_left);
		_Destroy(root->_right);
		delete root;
	}
	int _Height(node* root)
	{
		if (root == NULL)
			return 0;

		int leftH = _Height(root->_left);
		int rightH = _Height(root->_right);

		return leftH > rightH ? leftH + 1 : rightH + 1;
	}
	bool _Check(node* root, int blacknum, int benchmark)
	{
		if (root == nullptr) {
			if (benchmark != blacknum) {
				cout << "某条路上黑节点数量不等" << endl;
				return false;
			}
			return true;
		}
		if (root->_col == BLACK) {
			++blacknum;
		}
		if (root->_col == RED && root->_parent && root->_parent->_col==RED) {
			cout << "存在连续红色" << endl;
			return false;
		}
		return _Check(root->_left, blacknum, benchmark) && _Check(root->_right, blacknum, benchmark);
	}
	
private:
	node* _root=nullptr;
};

