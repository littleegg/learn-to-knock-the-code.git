#pragma once
#include "标头.h"
enum Colour {
	RED,
	BLACK
};
template<class K, class V>
struct RBNode {
	RBNode<K, V>* _left;
	RBNode<K, V>* _right;
	RBNode<K, V>* _parent;
	pair<K, V> _kv;
	Colour _col;
	RBNode(const pair<K, V>& kv)
		:_left(nullptr), _right(nullptr), _parent(nullptr), _kv(kv),_col(RED)
	{}
};
template<class K, class V>
class RBTree
{
public:
	typedef RBNode<K, V> node;
	~RBTree()
	{
		_Destroy(_root);
		_root = nullptr;
	}
	node* Find(const K& key){
		node* cur = _root;
		while (cur) {
			if (cur->_kv.first < key) {
				cur = cur->_right;
			}
			else if (cur->_kv.first > key) {
				cur = cur->_left;
			}
			else return cur;
		}
		return nullptr;
	}
	void RotateL(node* pre) {
		//subR  subRL  pp
		//pre_bf = 2 && cur_bf  = 1
		//要注意的是subRL可能是空  pre的pre可能是空（此时检索到根节了）
		node* subR = pre->_right;
		node* subRL = subR->_left;
		pre->_right = subRL;
		if (subRL) subRL->_parent = pre;
		node* pp = pre->_parent;

		subR->_left = pre;
		pre->_parent = subR;

		if (pp == nullptr) {
			_root = subR;
			_root->_parent = nullptr;
		}
		else {
			if (pp->_left == pre) {
				pp->_left = subR;
			}
			else {
				pp->_right = subR;
			}
			subR->_parent = pp;
		}
	}
	void RotateR(node* pre) {
		//subL  subLR pp
		//pre_bf=-2  cur_bf=-1
		node* subL = pre->_left;
		node* subLR = subL->_right;
		pre->_left = subLR;
		if (subLR) subLR->_parent = pre;
		node* pp = pre->_parent;

		subL->_right = pre;
		pre->_parent = subL;

		if (pre == _root) {
			_root = subL;
			_root->_parent = nullptr;
		}
		else {
			if (pp->_left == pre) pp->_left = subL;
			else pp->_right = subL;
			subL->_parent = pp;
		}
	}
	
	void InOrder() {
		_InOrder(_root);
	}
	bool IsBalance() {
		if (_root && _root->_col == RED) {
			cout << "根节点颜色是红色" << endl;
			return false;
		}
		int benchmark = 0;
		node* cur = _root;
		while (cur) {
			if (cur->_col == BLACK) ++benchmark;
			cur = cur->_left;
		}
		return _Check(_root, 0, benchmark);
	}
	int Height()
	{
		return _Height(_root);
	}

	bool Insert(const pair<K, V>& kv)
	{
		//首先插入
		if (_root == nullptr) {
			_root = new node(kv);
			_root->_col = BLACK;
			return true;
		}
		node* cur = _root;
		node* pre = nullptr;
		while (cur) {
			if (cur->_kv.first < kv.first) {
				pre = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first) {
				pre = cur;
				cur = cur->_left;
			}
			else return false;
		}
		cur = new node(kv);
		//默认插入的颜色就是R
		if (pre->_kv.first > kv.first) {
			pre->_left = cur;
		}
		else {
			pre->_right = cur;
		}
		cur->_parent = pre;
		while (pre && pre->_col == RED) {
			node* pp = pre->_parent;
			//先找叔叔
			if (pp->_left == pre) {
				node* ucl = pp->_right;
				if (ucl && ucl->_col == RED) {
					//叔叔存在且R   pp变R  pre和ucl都是B
					pre->_col = BLACK;
					ucl->_col = BLACK;
					pp->_col = RED;
					//继续向上调整
					cur = pp;
					pre = cur->_parent;
				}
				else {
					//u不存在 或者 u存在且B  旋转+变色
					if (cur == pre->_left) {
						//pre是pp的左孩子 cur也是pre的左孩子 ——左左右旋
						RotateR(pp);
						pre->_col = BLACK;
						pp->_col = RED;
					}
					else {
						//pre是pp左孩子 cur是pre右孩子 —— 左右 左右旋
						RotateL(pre);
						RotateR(pp);
						pp->_col = RED;
						cur->_col = BLACK;
					}
					break;
				}
			}
			else {
			//pre是pp的右孩子
				node* ucl = pp->_left;
				if (ucl && ucl->_col == RED) {
					//叔叔存在且为红 变色 向上调整
					pre->_col = BLACK;
					ucl->_col = BLACK;
					pp->_col = RED;
					cur = pp;
					pre = cur->_parent;
				}
				else {
					//叔叔不存在或者叔叔存在且为黑  旋转+变色
					if (cur == pre->_left) {
						//pre是pp右 cur是pp左——右左 右左旋
						RotateR(pre);
						RotateL(pp);
						pp->_col = RED;
						cur->_col = BLACK;
					}
					else {
						//pre是pp右 cur是pp右——右右左旋
						RotateL(pp);
						pre->_col = BLACK;
						pp->_col = RED;
					}
					break;
				}
			}
		}
		_root->_col = BLACK;
		return true;
	}
private:
	void _Destroy(node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		_Destroy(root->_left);
		_Destroy(root->_right);
		delete root;
	}
	int _Height(node* root)
	{
		if (root == NULL)
			return 0;

		int leftH = _Height(root->_left);
		int rightH = _Height(root->_right);

		return leftH > rightH ? leftH + 1 : rightH + 1;
	}
	bool _Check(node* root, int blacknum, int benchmark)
	{
		if (root == nullptr) {
			if (benchmark != blacknum) {
				cout << "某条路上黑节点数量不等" << endl;
				return false;
			}
			return true;
		}
		if (root->_col == BLACK) {
			++blacknum;
		}
		if (root->_col == RED && root->_parent && root->_parent->_col==RED) {
			cout << "存在连续红色" << endl;
			return false;
		}
		return _Check(root->_left, blacknum, benchmark) && _Check(root->_right, blacknum, benchmark);
	}
	void _InOrder(node* root) {
		if (root == nullptr)
		{
			return;
		}

		_InOrder(root->_left);
		cout << root->_kv.first << " ";
		_InOrder(root->_right);
	}
private:
	node* _root=nullptr;
};

void Test_RBTree1()
{
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14, 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	RBTree<int, int> t1;
	for (auto e : a)
	{
		/*	if (e == 14)
		{
		int x = 0;
		}*/

		t1.Insert(make_pair(e, e));
		//cout << e << "插入：" << t1.IsBalance() << endl;
	}

	t1.InOrder();
}