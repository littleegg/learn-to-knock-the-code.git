#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
//手撕队列
typedef int type;
typedef struct QueueNode
{
	type data;
	struct QueueNode* next;
}QNode;
typedef struct Queue
{
	QNode* head;
	QNode* tail;
	int size;
}Q;
//初始化
void InitQueue(Q* q)
{
	q->head = q->tail = NULL;
	q->size = 0;
}
//判断是否为空
bool Empty(Q* q)
{
	return q->head = NULL && q->tail == NULL;
}
//尾插
void QPush(Q* q, type x)
{
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("-1");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;

	if (q->tail == NULL && q->head==NULL)
	{
		q->head=q->tail = newnode;

	}
	else
	{
		q->tail->next = newnode;
		q->tail = q->tail->next;
	}
	q->size++;
}
//头删
void Pop(Q* q)
{
	assert(q);
	if (q->head->next == NULL)
	{
		q->head = q->tail = NULL;
	}
	else
	{
		QNode* cur = q->head;
		q->head = q->head->next;
		free(cur);
		cur = NULL;
	}
	q->size--;
}
//销毁
void Destory(Q* q)
{
	free(q->head);
	free(q->tail);
	q->tail = q->head = NULL;
	q->size = 0;
}
//取队头
type Front(Q* q)
{
	assert(q);
	return q->head->data;
}
//统计队列的元素个数
int Size(Q* q)
{
	int count = 0;
	QNode* cur = q->head;
	while (cur != q->tail)
	{
		count++;
		cur = cur->next;
	}
	return count;
}
int main()
{
	Q q;
	InitQueue(&q);
	QPush(&q, 1);
	QPush(&q, 2);
	QPush(&q, 3);
	QPush(&q, 4);
	QPush(&q, 5);
	printf("%d ", Size(&q));
	Destory(&q);
}