#include "Heap.h"

//打印
void HeapPrint(Heap* hp)
{
	assert(hp);
	for (int i=0;i<hp->size;i++)
	{
		printf("%d ", hp->a[i]);
	}
	printf("\n");
}
//交换
void Swap(HPDataType* a, HPDataType* b)
{
	HPDataType tmp = *a;
	*a = *b;
	*b = tmp;
}

// 堆的判空
int HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->size == 0;
}

// 堆的构建
void HeapCreate(Heap* hp, HPDataType* a, int n)
{
	assert(hp);
	hp->a = (HPDataType*)malloc(sizeof(HPDataType) * n);
	if (hp->a == NULL)
	{
		perror("HeapCreate fail");
		exit(-1);
	}
	memcpy(hp->a, a, n*sizeof(HPDataType));
	hp->size = hp->capacity=n;
	for (int i = (n - 1 - 1) / 2; i >=0; i--)
	{
		AdjustDown(hp->a, n, i);
	}
}
//初始化
void HeapInit(Heap* hp)
{
	assert(hp);
	hp->a = NULL;
	hp->capacity = hp->size = 0;
}

// 堆的销毁
void HeapDestory(Heap* hp)
{
	assert(hp);
	free(hp->a);
	hp->a = NULL;
	hp->capacity = hp->size = 0;
}

//向上调整,大堆
void AdjustUp(HPDataType* a, int child)
{
	assert(a);
	int parent = (child - 1) / 2;
	while (child>=0)
	{
		if (a[child] < a[parent])
		{
			Swap(&a[child],& a[parent]);
			child = parent;
			parent= (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}
// 堆的插入,尾插
void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	if (hp->capacity == hp->size)
	{
		//需要扩容
		int newcapacity = hp->capacity == 0 ? 4 : hp->capacity * 2;
		HPDataType* newnode = (HPDataType*)realloc(hp->a,sizeof(HPDataType) * newcapacity);
		if (newnode == NULL)
		{
			perror("malloc fail");
			exit(-1);
		}
		hp->a = newnode;
		hp->capacity = newcapacity;
		
	}
        hp->a[hp->size] = x;
		hp->size++;
		//向上调整
		AdjustUp(hp->a, hp->size - 1);
}

//向下调整
AdjustDown(HPDataType* a, int n, int parent)
{
	assert(a);
	int child = 2 * parent + 1;//假设是左孩子，并且左孩子是最大的孩子
	while (child <n)
	{
		if (a[child + 1] < a[child] && child+1<n)
		{
			child = child + 1;
		}
		//此时的child已经是最大的孩子
		if (a[parent] > a[child])
		{
			Swap(&a[parent], &a[child]);
			parent=child;
			child= 2 * parent + 1;
		}
		else
		{
			break;
		}
	}
}

// 堆的删除
void HeapPop(Heap* hp)
{
	assert(hp);
	assert(!HeapEmpty(hp));
	Swap(&hp->a[0], &hp->a[hp->size - 1]);
	hp->size--;
	AdjustDown(hp->a, hp->size, 0);
}
// 取堆顶的数据
HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	return hp->a[0];
}
// 堆的数据个数
int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->size;
}
//找出前k个最大
void PrintTopK(int* a, int n, int k)
{
	assert(a);
	Heap hp;
	while (k--)
	{
		HeapCreate(&hp, a, k);
	}
}

//堆排序
//升序,建大堆
void HeapSortHigh(int* a, int n)
{
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a, n, i);
	}
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		end--;
	}
}

//降序，建小堆
void HeapSortLow(int* a, int n)
{
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a, n, i);
	}
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		end--;
		AdjustUp(a, end);
	}
	
}

int main()
{
	int arr[] = { 1,2,5,7,10 };
	
	//HeapSortLow(arr, sizeof(arr) / sizeof(int));
	HeapSortHigh(arr, sizeof(arr) / sizeof(int));
	for (int i = 0; i < sizeof(arr) / sizeof(int); i++)
		printf("%d ", arr[i]);

	return 0;
}