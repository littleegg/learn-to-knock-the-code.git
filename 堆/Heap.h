#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

typedef int HPDataType;
typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}Heap;

//打印
void HeapPrint(Heap* hp);
//交换
void Swap(HPDataType* a, HPDataType* b);
// 堆的构建
void HeapCreate(Heap* hp, HPDataType* a, int n);
//向上调整
void AdjustUp(HPDataType* a, int chirld);
//向下调整
AdjustDown(HPDataType* a, int n, int parent);
//初始化
void HeapInit(Heap* hp);
// 堆的销毁
void HeapDestory(Heap* hp);
// 堆的插入
void HeapPush(Heap* hp, HPDataType x);
// 堆的删除
void HeapPop(Heap* hp);
// 取堆顶的数据
HPDataType HeapTop(Heap* hp);
// 堆的数据个数
int HeapSize(Heap* hp);
// 堆的判空
int HeapEmpty(Heap* hp);
void PrintTopK(int* a, int n, int k);
