#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
using namespace std;
//int main()
//{
//	string s1("hello world");
//	string::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it << " " ;
//		++it;
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	string s1("hello world");
//	string::reverse_iterator rit = s1.rbegin();
//	while (rit != s1.rend())
//	{
//		cout << *rit;
//		rit++;
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	string s1("hello world");
//	cout << s1.c_str() << endl;
//	cout << (void*)s1.c_str() << endl;
//	return 0;
//}

//int main()
//{
//	string s1("hello world");
//	string::const_iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		//(*it)++;
//		cout << *it ;
//		++it;
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	string s("hello world");
//	try
//	{
//		s.at(100);
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}

//int main()
//{
//	string s("hello world");
//	s.insert(s.size() , "!");
//	s.insert(0, 1, ' ');
//	s.erase(0, 1);
//	s.erase(s.begin());
//	cout << s << endl;
//	return 0;
//}

int main()
{
	string s("hello world");
	string str("Please replace the vowel in");
	size_t found = str.find_first_of("a");
	while (found != string::npos)
	{
		str[found] = '*';
		found = str.find_first_of("a", found + 1);
	}
	cout << str << endl;
	return 0;
}