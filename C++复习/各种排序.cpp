//#define _CRT_SECURE_NO_WARNINGS
//#include "各种头文件.h"
//
////计数排序
////思想：首先找到最大最小的元素 开max-min+1个空间 下标映射：a[i]-min
////  [-2,9] 有9-（-2）+1个数字
//// 很显然这里的时间复杂度是O(N+range) 
//// 空间复杂度O(range)
//// 
////void CountSort(vector<int> v) {
////	int max = v[0], min = v[0];
////	for (auto& e : v) {
////		if (e > max) max = e;
////		if (e < min) min = e;
////	}
////	cout << "min" << min << endl;
////	cout << "max" << max << endl;
////	vector<int> ans(max-min + 1);
////	for (int i = 0; i < v.size(); i++) {
////		ans[ v[i] - min] ++;
////	}
////	for (int i = 0; i < ans.size(); i++) {
////		while (ans[i]--) cout << i + min << " ";
////	}
////	cout << endl;
////}
////int main() {
////	vector<int> v = { -2,7,8,9,4,5,1,3,444 };
////	CountSort(v);
////	return 0;
////}
//
//
////int main()
////{
////	const int& b = 10;
////	double d = 12.34;
////	//int& rd = d; // 该语句编译时会出错，类型不同
////
////	//原因：引用变量类型为int，被引用对象类型为double。
////	//	在进行const int& cj = i; 前，进行了如下操作
////	//	double i = 1.2;
////	//int temp = i;
////	//const int& cj = temp;//所以cj并未真正绑定对象i
////	const int& rd = d;
////	cout << rd << endl;
////	int a = 10;
////	int& const p = a;
////	//p = 10;
////	//cout << "a:" << a << "p:"<<p << endl;
////	a = 10;
////	cout << "a:" << a << "p:" <<p<< endl;
////
////	const int& ci = 3; //正确，整型字面值常量绑定到 const引用
////	//c++编译器  会  分配内存空间  ,c++编译器把ci放在符号表中
////	   //  int  temp  =  3 
////	   //  const  int  &ci  =  temp;
////	return 0;
////}
//
////int& Add(int a, int b)
////{
////	int c = a + b;
////	return c;
////}
////int main()
////{
////	int& ret = Add(1, 2);
////	Add(3, 4);
////	cout << "Add(1, 2) is :" << ret << endl;
//////	
////	return 0;
////}
////
//////int main()
////{
////	int b = 0;
////	int& const a = b;
////	cout << a << b;
////}
//
//int main() {
//	std::string s="aaa";
//	std::cout << sizeof(s) << std::endl;
//	return 0;
//}