#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
using namespace std;
#include <string>
//class A
//{
//
//};
//class B:public A {};
//class Person
//{
//public:
//	//virtual A*  func()
//	//{
//	//	cout << "Person::全价" << endl;
//	//	return (A*)this;
//	//}
//	virtual  ~Person()
//	{
//		cout << "~Person" << endl;
//	}
//};
//class Student :public Person
//{
//public:
//
//	 /* virtual  B*  func()
//{
//	cout << "Student::半价" << endl;
//	return (B*)this;
//}*/
//	  ~Student()
//	  {
//		  cout << "~Student" << endl;
//	  }
//
//};
////class Solder:public Person
////{
////public:
////	void func()
////	{
////		cout << "Solder::免费" << endl;
////	}
////};
////void Fun(Person& p)
////{
////	p.func();
////}
//int main()
//{
//	/*Person p;
//	Student s;
//	Fun(p);
//   Fun(s);*/
//	Person* ptr1 = new Person;
//	Person* ptr2 = new Student;
//	delete ptr1;
//	delete ptr2;
//}


//class A 
//{
//	virtual void Fun() final 
//	{
//		cout << "A" << endl;
//	}
//};
//class B :public A
//{
//	virtual void Fun()
//	{
//		cout << "B" << endl;
//	}
//};

//class A final
//{
//	virtual void Fun() 
//	{
//		cout << "A" << endl;
//	}
//};
//class B :public A
//{
//	virtual void Fun()
//	{
//		cout << "B" << endl;
//	}
//};

//class A 
//{
//	virtual void Fun1()
//	{
//		cout << "A" << endl;
//	}
//};
//class B :public A
//{
//	virtual void Fun2() override 
//	{
//		cout << "B" << endl;
//	}
//};


//class A 
//{
//	virtual void Fun()=0
//	{
//		cout << "A" << endl;
//	}
//};
//class B :public A
//{
//	virtual void Fun() 
//	{
//		cout << "A" << endl;
//	}
//};
//int main()
//{
//	B b;
//}

//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//int main()
//{
//	Base b;
//	b.Func1();
//	cout<<sizeof(b)<<endl;
//}

//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//private:
//	int _b = 1;
//};
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//			cout << "Derive::Func1()" << endl;
//	}
//private:
//	int _d = 2;
//};
//int main()
//{
//	Base b;
//	Derive d;
//	return 0;
//}


//class Person {
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//};
//class Student : public Person {
//public:
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//};
//void Func(Person* p)
//{
//	p->BuyTicket();
//}
//int main()
//{
//	Person mike;
//	Func(&mike);
//	mike.BuyTicket();
//
//	return 0;
//}


//class Person {
//	public:
//		 void BuyTicket() { cout << "买票-全价" << endl; }
//	};
//	class Student : public Person {
//	public:
//		 void BuyTicket() { cout << "买票-半价" << endl; }
//	};
//	void Func(Person* p)
//	{
//		p->BuyTicket();
//	}
//	int main()
//	{
//		Person mike;
//		Func(&mike);
//		mike.BuyTicket();
//	
//		return 0;
//	}

//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//private:
//	int _b = 1;
//};
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//			cout << "Derive::Func1()" << endl;
//	}
//	void Func4()
//	{
//		cout << "Derive::Func4()" << endl;
//	}
//	virtual void Func5()
//	{
//		cout << "Derive::Func5()" << endl;
//	}
//private:
//	int _d = 2;
//};
//typedef void(*VPT)(); //函数指针数组
//void Print(VPT vpt[]) //类似int a[]  ,VPT相当于int，代表数组里面元素类型是函数指针
//{
//	for (int i = 0; i<2; i++) //不要写结束条件是vpi[i]==NULL，不是所有平台虚表末都是空
//	{
//		printf("%d:%p  ", i, vpt[i]); //vpt[i]相当于a[i]，调用数组里每个元素
//		vpt[i](); //vpt[i]是函数指针，加上（）表示调用这个指针所指函数，函数里面正好有打印
//		cout << endl;
//	}
//}
//int main()
//{
//	Base b;
//	Derive d;
//	Print((VPT*)(*(int*)&b)); //打印函数里面应该是i<
//	//Print((VPT*)(*(int*)&d)); //i<3
//
//	return 0;
//}
//int main()
//{
//	int a = 0;
//	cout << "栈:" << &a << endl;
//
//	int* p1 = new int;
//	cout << "堆:" << p1 << endl;
//
//	const char* str = "hello world";
//	cout << "代码段/常量区:" << (void*)str << endl;
//
//	static int b = 0;
//	cout << "静态区/数据段:" << &b << endl;
//
//	Base be;
//	cout << "虚表:" << (void*)*((int*)&be) << endl;
//
//	Base* ptr1 = &be;
//	int* ptr2 = (int*)ptr1;
//	cout << "虚表:" << (void*)*((int*)&be) << endl;
//
//
//	Derive de;
//	cout << "虚表:" << (void*)*((int*)&de) << endl;
//
//	Base b1;
//	Base b2;
//
//
//	return 0;
//}


//class Base1 {
//public:
//	virtual void func1() { cout << "Base1::func1" << endl; }
//	virtual void func2() { cout << "Base1::func2" << endl; }
//private:
//	int b1;
//};
//class Base2 {
//public:
//	virtual void func1() { cout << "Base2::func1" << endl; } //重写
//	virtual void func2() { cout << "Base2::func2" << endl; }
//private:
//		int b2;
//};
//class Derive : public Base2, public Base1 {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//private:
//	int d1;
//};
//typedef void(*VFPTR) ();
//void PrintVTable(VFPTR vTable[])
//{
//	cout << " 虚表地址>" << vTable << endl;
//	for (int i = 0; vTable[i] != nullptr; ++i)
//	{
//		printf(" 第%d个虚函数地址 :0X%x,->", i, vTable[i]);
//		vTable[i]();
//	}
//	cout << endl;
//}
//int main()
//{
//	Derive d;
//	VFPTR* vTableb1 = (VFPTR*)(*(void **)&d);
//	PrintVTable(vTableb1);
//	//VFPTR* vTableb2 = (VFPTR*)(*(int*)((char*)&d + sizeof(Base1)));
//	Base2* ptr=&d;
//	VFPTR* vTableb2 = (VFPTR*)(*(int*)(ptr));
//	PrintVTable(vTableb2);
//	return 0;
//}

//using namespace std;
//class A {
//public:
//	A(const char* s) { cout << s << endl; }
//	~A() {}
//};
//class B :virtual public A
//{
//public:
//	B(const char* s1, const char* s2) :A(s1) { cout << s2 << endl; }
//};
//class C :virtual public A
//{
//public:
//	C(const char* s1, const char* s2) :A(s1) { cout << s2 << endl; }
//};
//class D :public B, public C
//{
//public:
//	D(const char* s1, const char* s2, const char* s3, const char* s4) :B(s1, s2), C(s1, s3), A(s1)
//	{
//		cout << s4 << endl;
//	}
//};
//int main() {
//	D* p = new D("class A", "class B", "class C", "class D");
//	delete p;
//	return 0;
//}


//
//class A
//{
//public:
//	virtual void func(int val = 1) { std::cout << "A->" << val << std::endl; }
//	virtual void test() { func(); }
//};
//class B : public A
//{
//public:
//	void func(int val = 0) { std::cout << "B->" << val << std::endl; }
//};
//int main(int argc, char* argv[])
//{
//	B* p = new B;
//	p->test();
//	return 0;
//}
