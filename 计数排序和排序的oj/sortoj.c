#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void Countsort(int* a, int n)
{
	int index = 0;
	int max = a[n-1];
	for (int i = 0; i < n; i++)
	{
		if (a[i] > max)
		{
			max = a[i];
		}
	}
	//走到这就统计出来最大和最小的元素
	int* result= (int*)malloc(sizeof(int) * (max+1));
	int* tmp = (int*)malloc(sizeof(int) * (max+1));
	if (result == NULL)
	{
		perror("fail");
		exit(-1);
	}
	if (tmp == NULL)
	{
		perror("fail");
		exit(-1);
	}
	for (int i = 0; i <= max; i++) //初始化
	{
		tmp[i] = 0;
		result[i] = 0;
	}
	for (int i = 0; i < n; i++)
	{
		tmp[a[i]]++;
	}
	//此时tmp数组里面放着每一个数字出现的次数
	for (int i = 0; i <= max; i++) 
	{
		while (tmp[i]--) 
		{
			result[index++] = i;
		}
	}
	free(tmp);
	tmp = NULL;
    memcpy(a,result,sizeof(int)*n);
}

int main()
{
	int a[] = { 10,5,1,7,9 };
	Countsort(a, 5);
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", a[i]);
	}
	return 0;
}