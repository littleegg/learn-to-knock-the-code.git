#define _CRT_SECURE_NO_WARNINGS


//二进制位数
#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<int> v;
    int n;
    while (cin >> n)
    {
        while (n)
        {
            v.push_back(n % 2);
            n /= 2;
        }
        for (int i = v.size() - 1; i >= 0; i--) printf("%d", v[i]);
        cout << endl;
        v.clear();
    }
    return 0;

}


//第k小

#include <iostream>
#include <set>
using namespace std;

int main() {
    set<int> s;
    int n, tmp, k;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> tmp;
        s.insert(tmp);
    }
    cin >> k;
    auto e = s.begin();
    for (int i = 0; i < k - 1; i++) e++;
    cout << *e << endl;
}


//矩阵幂
#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n, k;
    while (cin >> n >> k)
    {
        vector<vector<int>> vv(n, vector<int>(n));
        vector<vector<int>> ans(n, vector<int>(n));

        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
            {
                cin >> vv[i][j];
                ans[i][j] = vv[i][j];
            }
        k -= 1;
        while (k--)
        {
            vector<vector<int>> c(n, vector<int>(n));
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    for (int a = 0; a < n; a++)
                        c[i][j] += ans[i][a] * vv[a][j];

            ans = c;
        }
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
                cout << ans[i][j] << " ";
            cout << endl;
        }
    }
    return 0;
}