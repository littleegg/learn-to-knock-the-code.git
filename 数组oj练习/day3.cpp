#define _CRT_SECURE_NO_WARNINGS

class Solution {
public:
    int search(vector<int>& nums, int target) {
        int n = nums.size();
        int left = 0; int right = n - 1;
        if (right == -1) return -1;
        while (left < right)
        {
            int mid = left + (right - left) / 2;
            if (nums[left] < nums[mid])
            {
                if (nums[left] <= target && target <= nums[mid]) right = mid;
                else left = mid + 1;
            }
            else if (nums[left] > nums[mid])
            {
                if (nums[left] <= target || target <= nums[mid]) right = mid;
                else left = mid + 1;
            }
            else {
                if (nums[left] != target) left++;
                else right = left;
            }
        }
        return (nums[left] == target) ? left : -1;

    }
};