#define _CRT_SECURE_NO_WARNINGS

//二分查找
class Solution {
public:
    int search(vector<int>& nums, int target) {
        //左闭右闭
        int l = 0, r = nums.size() - 1;
        while (l <= r)
        {
            int mid = l + (r - l) / 2;  //防溢出
            if (nums[mid] > target) r = mid - 1;
            else if (nums[mid] < target) l = mid + 1;
            else return mid;
        }
        return -1;
    }
};

//快慢指针
class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int s = 0;
        for (int f = 0; f < nums.size(); f++)
        {
            if (nums[f] != val)
            {
                nums[s++] = nums[f];
            }
        }
        return s;
    }
};

//滑动窗口
class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {
        int ans = 0x3f3f3f3f;
        int sum = 0;
        int i = 0;
        int sublen = 0;
        for (int j = 0; j < nums.size(); j++)
        {
            sum += nums[j];

            while (sum >= target)
            {
                sublen = (j - i + 1);
                ans = ans < sublen ? ans : sublen;
                sum -= nums[i++];  //滑动
            }

        }
        return ans == 0x3f3f3f3f ? 0 : ans;
    }
};


//螺旋矩阵
class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
        //左闭右开
        vector<vector<int>> v(n, vector<int>(n));
        int beginx = 0, beginy = 0;
        int loop = n / 2;
        int mid = n / 2;//矩阵中间的位置    
        int count = 1; //赋值
        int offset = 1; //每条边的遍历长度
        int i, j;
        while (loop--)
        {
            i = beginx;
            j = beginy;
            for (; j < n - offset; j++) v[beginx][j] = count++;
            for (; i < n - offset; i++) v[i][j] = count++;
            for (; j > beginy; j--) v[i][j] = count++;
            for (; i > beginx; i--) v[i][j] = count++;
            beginx++; beginy++;
            offset++;
        }
        if (n % 2) v[mid][mid] = count;  //单独给矩阵中间赋值
        return v;

    }
};

//构建链表
class MyLinkedList {
public:
    struct ListNode {
        ListNode* next;
        int val;
        ListNode(int _val) :val(_val), next(nullptr) {}
    };
    MyLinkedList() {
        _dummyhead = new ListNode(0);
        _size = 0;
    }

    int get(int index) {
        if (index > (_size - 1) || index < 0) return -1;
        ListNode* cur = _dummyhead->next;
        while (index--) cur = cur->next;
        return cur->val;
    }

    void addAtHead(int val) {
        ListNode* newhead = new ListNode(val);
        newhead->next = _dummyhead->next;
        _dummyhead->next = newhead;
        _size++;
    }

    void addAtTail(int val) {
        ListNode* cur = _dummyhead;
        while (cur->next)
        {
            cur = cur->next;
        }
        ListNode* newnode = new ListNode(val);
        cur->next = newnode;
        _size++;
    }

    void addAtIndex(int index, int val) {
        if (index > _size) return;
        ListNode* newnode = new ListNode(val);
        ListNode* cur = _dummyhead;
        while (index--) cur = cur->next;
        newnode->next = cur->next;
        cur->next = newnode;
        _size++;
    }

    void deleteAtIndex(int index) {
        if (index >= _size || index < 0) return;
        ListNode* cur = _dummyhead;
        while (index--) cur = cur->next;
        ListNode* tmp = cur->next;
        cur->next = cur->next->next;
        delete tmp;
        _size--;
    }
private:
    ListNode* _dummyhead;
    int _size;

};

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * MyLinkedList* obj = new MyLinkedList();
 * int param_1 = obj->get(index);
 * obj->addAtHead(val);
 * obj->addAtTail(val);
 * obj->addAtIndex(index,val);
 * obj->deleteAtIndex(index);
 */



