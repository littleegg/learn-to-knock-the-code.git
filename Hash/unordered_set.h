#pragma once
#include "hashtable.h"
namespace  wrt{
	template<class K,class Hash=HashFunc<K>>
	class unordered_set
	{
	public:
		struct SetKofT
		{
			const K& operator()(const K& k) {
				return k;
			}
		};
	public:
		typedef typename HashBucket::HashTable<K, K, SetKofT,Hash>::const_iterator iterator;
		typedef typename HashBucket::HashTable<K, K, SetKofT, Hash>::const_iterator  const_iterator;

		iterator begin()
		{
			return _ht.begin();
		}
		iterator end()
		{
			return _ht.end();
		}
		const_iterator begin() const
		{
			return _ht.begin();
		}
		const_iterator end() const
		{
			return _ht.end();
		}

		pair<iterator, bool> insert(const K& k) {
			return _ht.Insert(k);
		}
		iterator find(const K& k) {
			return _ht.Find(k);
		}
		bool erase(const K& k) {
			return _ht.Erase(k);
		}
	private:
		HashBucket::HashTable<K, K, SetKofT,Hash> _ht;
	};
	void print(const unordered_set<int>& s) {
		unordered_set<int>::const_iterator it = s.begin();
		while (it != s.end())
		{
			//*it = 1;
			cout << *it << " ";
			++it;
		}
		cout << endl;
	}
	void test_unordered_set1()
	{
		int a[] = { 3, 33, 2, 13, 5, 12, 1002 };
		unordered_set<int> s;
		for (auto e : a)
		{
			s.insert(e);
		}
		s.insert(54);
		s.insert(107);
		

		for (auto& e : s)
		{
			cout << e << " ";
		}
		cout << endl;

		print(s);
	}
}