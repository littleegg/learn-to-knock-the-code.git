#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <unordered_map>
#include <unordered_set>
using namespace std;

void test_unordered_map1() {
	string arr[] = { "jj","jk","jk","jj"};
	unordered_map<string, int> countMap;
	for (auto& e : arr)
	{
		countMap[e]++;
	}

	for (auto& kv : countMap)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	

}
void test_unordered_set1() {
	unordered_set<int> s;
	s.insert(1);
	s.insert(3);
	s.insert(2);
	s.insert(7);
	s.insert(2);

	unordered_set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;
}